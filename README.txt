bsnes-mcfly: A Qt-based SNES emulator
Emulation: byuu
GUI: hex_usr
License: GPLv3 (except for archive-reader; see archive-reader/LICENSE for details)

bsnes-mcfly seeks to port the Qt GUI of bsnes v073 upwards to modern higan/bsnes versions, so that the long list of features in this GUI are usable with the latest SNES emulation improvements, both with traditional ROM files and with higan v107-style gamepaks (cartridge folders). The goal is to get users to migrate away from bsnes v073 and bsnes-classic, so that its ancient cartridge folder format with XML manifests can finally be put to rest permanently.

What bsnes-mcfly does NOT do is compete with bsnes v107. bsnes is currently active, and it has many of bsnes-mcfly's more important features combined with a nearly bug-free GUI toolkit. While bsnes-mcfly strives to keep GUI bugs to a minimum, it cannot achieve perfection by using Qt as a dependency, thus it should not be heralded as “better” than bsnes v107.

For information on how to use bsnes-mcfly, see Help/Documentation in the menu.

Compiling bsnes-mcfly on Windows is a rather involved procedure, so a beginner should first practice on higan and bsnes before attempting to compile bsnes-mcfly. That said, instructions can be found in the “Windows Compilation.txt” document.

Features ported from bsnes v073 that aren't in bsnes v107:
* Compressed archive support: Zip, GZip, 7z, BZip2
  * 7z support by Igor Pavlov's LZMA SDK, available in the public domain
  * BZip2 support by Rob Landley under the zero-clause BSD license
* Obscure copier extensions: SWC, FIG, UFO, GD3, GD7, DX2, MGD, MGH, 048, 058, 068, 078, BIN, USA, EUR, JPN, AUS
* asciiPad: A gamepad where each turbo switch can be set to Off, Turbo, or Auto.
* Simultaneous up+down and left+right (evil hack, must be enabled in settings-qt.bml)
* UPS soft-patching (applied before removing copier header)
* Movie recording and playback
* Retention of Game Genie cheats without converting them to Pro Action Replay format
* Cheat search (WRAM only)
* Enabling/disabling of individual PPU layers and DSP channels in the Compatibility and Performance profiles (evil hack)
* Software filters
  * 2xSaI, Super 2xSaI, Super Eagle
  * HQ2x, LQ2x, Scale2x
  * Pixellate2x
  * blargg's snes_ntsc (with 15-bit precision instead of 13-bit precision)
  * Phosphor3x (an obscure filter included with bsnes v083-v088)
* Performance profile
  * High-level emulation of the DSP1, DSP2, DSP3, DSP4, ST010, and Cx4 (firmwares still required for validation)
  * SMP and DSP are merged into a single APU class for the purposes of GCC optimizations
  * Mixed opcode/cycle timing for the SMP; slightly faster than cycle timing, still supports streamed music
  * Super Scope and Justifier are the only controllers using cothreads
  * JOY1/JOY2/JOY3/JOY4 parallel registers handled on a controller-by-controller basis

Features shared between bsnes v107 and bsnes-mcfly that weren't in bsnes v073:
* Many, many SNES emulation improvements since December 2010
* Low-level emulation of the Cx4 and ST018 in the Accuracy and Compatibility profiles
* Support for concatenated firmware in ROMs and a firmware/ fallback directory for non-concatenated ROMs
* Database lookup of licensed games. The database is embedded into the application and not external.
* WASAPI and ASIO audio drivers
* Exclusive mode for Direct3D and WASAPI
* BPS soft-patching (applied after removing copier header)
* Plain text cheat code format shared with Snes9x 1.56: AAAAAA=DD and AAAAAA=CC?DD
* Combining OpenGL shaders

An “evil hack” is an external patch to higan's source code that is activated by giving it priority during the compilation stage. This avoids directly modifying higan's source while still applying changes that can't be implemented directly in the GUI such as simultaneous up+down and left+right. All of the evil hacks used by bsnes-mcfly are contained in the bsnes/sfc/ directory. The higan/ directory is never modified directly.

Missing features that are only in bsnes v073:
* Compressed archives: Z (compress), RAR, JMA
* Selecting one of multiple files in a single Zip archive
* snes_ntsc configuration dialog
* Binding the Pause/Break key to an input
* Direct3D shaders (the Sepia shader was converted to OpenGL)
