#include <nall/platform.hpp>
#include <nall/traits.hpp>
#include <nall/stdint.hpp>
#include <nall/primitives.hpp>
using namespace nall;
using uint32 = Natural<32>;

extern "C" {
  void filter_size(unsigned&, unsigned&);
  void filter_render(uint32_t*, unsigned, const uint32_t*, unsigned, unsigned, unsigned);
};

dllexport void filter_size(unsigned& width, unsigned& height) {
  width = width > 256 ? width * 3 / 2 : width * 3;
  height *= 3;
}

dllexport void filter_render(
  uint32_t* output, unsigned outputPitch,
  const uint32_t* input, unsigned inputPitch,
  unsigned width, unsigned height
) {
  enum : unsigned { Mask = (248 << 16) + (248 << 8) + (248 << 0) };
  outputPitch >>= 2, inputPitch >>= 2;

  const bool hires = width > 256;

  #pragma omp parallel for num_threads(2)
  for(unsigned y = 0; y < height; y++) {
    const uint32_t* in = input + y * inputPitch;
    uint32_t* out0 = output + y * outputPitch * 3;
    uint32_t* out1 = output + y * outputPitch * 3 + outputPitch;
    uint32_t* out2 = output + y * outputPitch * 3 + outputPitch + outputPitch;

    for(unsigned x = 0; x < width >> hires; x++) {
      uint32 A, B, C, D;
      A = (x == 0 ? 0 : *(in - 1));
      B = *in;
      C = hires ? *(in + 1) : *in;
      D = x == (width >> hires) - 1 ? 0 : *(in + 1 + hires);

      #define Ar A.byte(2)
      #define Ag A.byte(1)
      #define Ab A.byte(0)
      #define Br B.byte(2)
      #define Bg B.byte(1)
      #define Bb B.byte(0)
      #define Cr C.byte(2)
      #define Cg C.byte(1)
      #define Cb C.byte(0)
      #define Dr D.byte(2)
      #define Dg D.byte(1)
      #define Db D.byte(0)

      if(hires) {
        A = ((Br >> 0) << 16) + ((Bg      >> 1) << 8) + ((Ab >> 1) << 0);
        B = ((Br >> 1) << 16) + ((Bg + Cg >> 1) << 8) + ((Cb >> 1) << 0);
        C = ((Dr >> 1) << 16) + ((Cg      >> 1) << 8) + ((Cb >> 0) << 0);
      } else {
        A = ((Br >> 0) << 16) + ((Bg      >> 1) << 8) + ((Ab >> 1) << 0);
        B = ((Br >> 1) << 16) + ((Bg      >> 0) << 8) + ((Cb >> 1) << 0);
        C = ((Dr >> 1) << 16) + ((Cg      >> 1) << 8) + ((Cb >> 0) << 0);
      }

      #undef Ar
      #undef Ag
      #undef Ab
      #undef Br
      #undef Bg
      #undef Bb
      #undef Cr
      #undef Cg
      #undef Cb
      #undef Dr
      #undef Dg
      #undef Db

      in++; if(hires) in++;

      *out0++ = A;
      *out1++ = A;
      *out2++ = (A & Mask) >> 1;

      *out0++ = B;
      *out1++ = B;
      *out2++ = (B & Mask) >> 1;

      *out0++ = C;
      *out1++ = C;
      *out2++ = (C & Mask) >> 1;
    }
  }
}
