#include "SPC_DSP.h"

struct SMP {
  SMP();

  //io.cpp
  auto portRead(uint8_t port) -> uint8_t;
  auto portWrite(uint8_t port, uint8 data) -> void;

  //smp.cpp
  auto dspMute() const -> bool;

  auto main() -> void;
  auto load() -> bool;
  auto power(bool reset) -> void;

  //disassembler.cpp
//auto disassemble_read(uint16_t addr) -> uint8_t;
//auto relb(int8_t offset, int op_len) -> uint16_t;
//auto disassemble_opcode(char* output, uint16_t addr) -> void;

  //serialization.cpp
  auto serialize(serializer&) -> void;

  //smp.cpp
  auto updateMuteMask() -> void;

  uint frequency;
  int64 clock;

  uint8 iplrom[64];
  uint8 apuram[64 * 1024];

private:
  shared_pointer<Stream> stream;

  SPC_DSP spc_dsp;
  int16_t samplebuffer[8192];

  //memory.cpp
  alwaysinline auto op_io() -> void;
  alwaysinline auto op_io(uint clocks) -> void;
  alwaysinline auto op_read(uint16 addr) -> uint8;
  alwaysinline auto op_write(uint16 addr, uint8 data) -> void;
  alwaysinline auto op_step() -> void;

  //io.cpp
  auto readIO(uint16 addr) -> uint8;
  auto writeIO(uint16 addr, uint8 data) -> void;

  //timing.cpp
  auto step() -> void;
  auto step(uint clocks) -> void;

  //algorithms.cpp
  auto op_adc (uint8  x, uint8  y) -> uint8;
  auto op_addw(uint16 x, uint16 y) -> uint16;
  auto op_and (uint8  x, uint8  y) -> uint8;
  auto op_cmp (uint8  x, uint8  y) -> uint8;
  auto op_cmpw(uint16 x, uint16 y) -> uint16;
  auto op_eor (uint8  x, uint8  y) -> uint8;
  auto op_inc (uint8  x) -> uint8;
  auto op_dec (uint8  x) -> uint8;
  auto op_or  (uint8  x, uint8  y) -> uint8;
  auto op_sbc (uint8  x, uint8  y) -> uint8;
  auto op_subw(uint16 x, uint16 y) -> uint16;
  auto op_asl (uint8  x) -> uint8;
  auto op_lsr (uint8  x) -> uint8;
  auto op_rol (uint8  x) -> uint8;
  auto op_ror (uint8  x) -> uint8;

  struct Flags {
    alwaysinline operator uint() const {
      return (n << 7) | (v << 6) | (p << 5) | (b << 4)
           | (h << 3) | (i << 2) | (z << 1) | (c << 0);
    };

    alwaysinline auto operator=(uint data) -> uint {
      n = data & 0x80; v = data & 0x40; p = data & 0x20; b = data & 0x10;
      h = data & 0x08; i = data & 0x04; z = data & 0x02; c = data & 0x01;
      return data;
    }

    alwaysinline auto operator|=(uint data) -> uint { return operator=(operator uint() | data); }
    alwaysinline auto operator^=(uint data) -> uint { return operator=(operator uint() ^ data); }
    alwaysinline auto operator&=(uint data) -> uint { return operator=(operator uint() & data); }

    bool n, v, p, b, h, i, z, c;
  };

  struct Regs {
    uint16 pc;
    uint8 sp;
    union {
      uint16_t ya;
      struct { uint8_t order_lsb2(a, y); };
    };
    uint8 x;
    Flags p;
  } regs;

  uint16 rd, wr, dp, sp, ya, bit;

  struct IO {
    //external
    union {
      uint8_t apu[4];
      uint16_t apu_w[2];
    };

    //$00f1
    bool iplromEnable;

    //$00f2
    uint8_t dspAddr;

    //$00f8,$00f9
    uint8_t aux4;
    uint8_t aux5;
  } io;

  uint opcode_number;
  uint opcode_cycle;

  template<uint frequency>
  struct Timer {
    auto step(uint clocks) -> void;
    auto synchronizeStage1() -> void;

    bool enable;
    uint8 target;
    uint8 stage1;
    uint8 stage2;
    uint8 stage3;
  };

  Timer<128> timer0;
  Timer<128> timer1;
  Timer< 16> timer2;

  static const uint cycle_count_table[256];
  uint64 cycle_table_cpu[256];
  uint64 cycle_step_cpu;
};

extern SMP smp;
