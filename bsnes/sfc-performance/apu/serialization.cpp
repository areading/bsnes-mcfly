static auto dsp_state_save(unsigned char** out, void* in, size_t size) -> void {
  memcpy(*out, in, size);
  *out += size;
}

static auto dsp_state_load(unsigned char** in, void* out, size_t size) -> void {
  memcpy(out, *in, size);
  *in += size;
}

auto SMP::serialize(serializer &s) -> void {
  s.integer(clock);

  s.integer(opcode_number);
  s.integer(opcode_cycle);

  s.integer(regs.pc);
  s.integer(regs.sp);
  s.integer(regs.a);
  s.integer(regs.x);
  s.integer(regs.y);

  s.integer(regs.p.n);
  s.integer(regs.p.v);
  s.integer(regs.p.p);
  s.integer(regs.p.b);
  s.integer(regs.p.h);
  s.integer(regs.p.i);
  s.integer(regs.p.z);
  s.integer(regs.p.c);

  s.integer(rd);
  s.integer(wr);
  s.integer(dp);
  s.integer(sp);
  s.integer(ya);
  s.integer(bit);

  s.array(io.apu);

  s.integer(io.iplromEnable);

  s.integer(io.dspAddr);

  s.integer(io.aux4);
  s.integer(io.aux5);

  s.integer(timer0.enable);
  s.integer(timer0.target);
  s.integer(timer0.stage1);
  s.integer(timer0.stage2);
  s.integer(timer0.stage3);

  s.integer(timer1.enable);
  s.integer(timer1.target);
  s.integer(timer1.stage1);
  s.integer(timer1.stage2);
  s.integer(timer1.stage3);

  s.integer(timer2.enable);
  s.integer(timer2.target);
  s.integer(timer2.stage1);
  s.integer(timer2.stage2);
  s.integer(timer2.stage3);

  //DSP
  s.array(apuram);
  s.integer(clock);
  s.array(samplebuffer);

  unsigned char state[SPC_DSP::state_size];
  unsigned char* p = state;
  memset(state, 0, sizeof(state));
  if(s.mode() == serializer::Save) {
    spc_dsp.copy_state(&p, dsp_state_save);
    s.array(state);
  } else if(s.mode() == serializer::Load) {
    s.array(state);
    spc_dsp.copy_state(&p, dsp_state_load);
  } else {
    s.array(state);
  }
}
