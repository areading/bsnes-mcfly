//#define CYCLE_TIMING
#define MIXED_TIMING
//#define OPCODE_TIMING

#include <sfc-performance/sfc.hpp>

namespace higan::SuperFamicom {

#include "SPC_DSP.cpp"

SMP smp;
#include "algorithms.cpp"
#include "memory.cpp"
#include "io.cpp"
#include "timing.cpp"
#include "serialization.cpp"

SMP::SMP() {
}

auto SMP::main() -> void {
  while(clock < 0) op_step();
}

auto SMP::dspMute() const -> bool {
  return spc_dsp.mute();
}

auto SMP::load() -> bool {
  if(auto fp = platform->open(ID::System, "ipl.rom", File::Read, File::Required)) {
    fp->read(iplrom, 64);
    return true;
  }
  return false;
}

auto SMP::power(bool reset) -> void {
  frequency = system.apuFrequency();
  clock = 0;

  for(uint n = 0; n < 256; n++) {
    cycle_table_cpu[n] = (cycle_count_table[n] * 24) * cpu.frequency();
  }

  cycle_step_cpu = 24 * cpu.frequency();

  if(!reset) {
    random.array(apuram + 0x0000, 0x00f4);
    random.array(apuram + 0x00f8, sizeof(apuram) - 0x00f8);
    spc_dsp.init(apuram);
    spc_dsp.reset();
  }

  stream = audio.createStream(2, system.apuFrequency() / 768.0);

  spc_dsp.soft_reset();
  spc_dsp.set_output(samplebuffer, 8192);

  opcode_number = 0;
  opcode_cycle = 0;

  regs.pc = 0xffc0;
  regs.sp = 0xef;
  regs.a = 0x00;
  regs.x = 0x00;
  regs.y = 0x00;
  regs.p = 0x02;

  //external
  io.apu[0] = 0x00;
  io.apu[1] = 0x00;
  io.apu[2] = 0x00;
  io.apu[3] = 0x00;

  //$00f1
  io.iplromEnable = true;

  //$00f2
  io.dspAddr = 0x00;

  //$00f4-00f7
  apuram[0x00f4] = 0x00;
  apuram[0x00f5] = 0x00;
  apuram[0x00f6] = 0x00;
  apuram[0x00f7] = 0x00;

  //$00f8,$00f9
  io.aux4 = 0x00;
  io.aux5 = 0x00;

  //timers
  timer0.enable = timer1.enable = timer2.enable = false;
  timer0.stage1 = timer1.stage1 = timer2.stage1 = 0;
  timer0.stage2 = timer1.stage2 = timer2.stage2 = 0;
  timer0.stage3 = timer1.stage3 = timer2.stage3 = 0;

  timer0.target = 0;
  timer1.target = 0;
  timer2.target = 0;
}

auto SMP::updateMuteMask() -> void {
  uint8_t muteMask = 0x00;
  for(uint i : range(8)) muteMask |= option.hack.dsp.channelMute[i]() << i;
  spc_dsp.mute_voices(muteMask);
}

}
