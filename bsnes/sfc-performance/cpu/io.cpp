auto CPU::readRAM(uint24 addr, uint8 data) -> uint8 {
  return wram[addr];
}

auto CPU::readAPU(uint24 addr, uint8 data) -> uint8 {
  while(smp.clock < 0) smp.main();
  return smp.portRead(addr);
}

auto CPU::readCPU(uint24 addr, uint8 data) -> uint8 {
  switch((uint16)addr) {
  case 0x2180:  //WMDATA
    data = *io.wramAddress++;
    if(io.wramAddress > (uint8_t*)wramUpperBound) io.wramAddress -= 0x20000;
    return data;

  case 0x4016:  //JOYSER0
    data &= 0xfc;
    data |= controllerPort1.data();
    return data;

  case 0x4017:  //JOYSER1
    data &= 0xe0;
    data |= 0x1c;
    data |= controllerPort2.data();
    return data;

  case 0x4210:  //RDNMI
    data &= 0x70;
    data |= status.nmi_line << 7;
    data |= 0x02;  //CPU revision
    status.nmi_line = false;
    return data;

  case 0x4211:  //TIMEUP
    data &= 0x7f;
    data |= status.irq_line << 7;
    status.irq_line = false;
    return data;

  case 0x4212:  //HVBJOY
    data &= 0x3e;
    data |= (vcounter() >= ppu.vdisp() && vcounter() <= ppu.vdisp() + 2) << 0;
    data |= (hcounter() <= 2 || hcounter() >= 1096) << 6;
    data |= (vcounter() >= ppu.vdisp()) << 7;
    return data;

  case 0x4213: return io.pio;            //RDIO

  case 0x4214: return io.rddiv.byte(0);  //RDDIVL
  case 0x4215: return io.rddiv.byte(1);  //RDDIVH
  case 0x4216: return io.rdmpy.byte(0);  //RDMPYL
  case 0x4217: return io.rdmpy.byte(1);  //RDMPYH

  case 0x4218: return controllerPort1.joy0l;
  case 0x4219: return controllerPort1.joy0h;
  case 0x421a: return controllerPort2.joy0l;
  case 0x421b: return controllerPort2.joy0h;
  case 0x421c: return controllerPort1.joy1l;
  case 0x421d: return controllerPort1.joy1h;
  case 0x421e: return controllerPort2.joy1l;
  case 0x421f: return controllerPort2.joy1h;
  }

  return data;
}

auto CPU::readDMA(uint24 addr, uint8 data) -> uint8 {
  auto& channel = this->channel[addr.bits(4,6)];

  switch(addr & 0xff8f) {
  case 0x4300: {
    return (channel.direction << 7)
         | (channel.indirect << 6)
         | (channel.unused << 5)
         | (channel.reverse_transfer << 4)
         | (channel.fixed_transfer << 3)
         | (channel.transfer_mode << 0);
  }

  case 0x4301: return channel.dest_addr;
  case 0x4302: return channel.source_addr >> 0;
  case 0x4303: return channel.source_addr >> 8;
  case 0x4304: return channel.source_bank;
  case 0x4305: return channel.transfer_size >> 0;
  case 0x4306: return channel.transfer_size >> 8;
  case 0x4307: return channel.indirect_bank;
  case 0x4308: return channel.hdma_addr >> 0;
  case 0x4309: return channel.hdma_addr >> 8;
  case 0x430a: return channel.line_counter;
  case 0x430b: case 0x430f: return channel.unknown;
  }

  return data;
}

auto CPU::writeRAM(uint24 addr, uint8 data) -> void {
  wram[addr] = data;
}

auto CPU::writeAPU(uint24 addr, uint8 data) -> void {
  while(smp.clock < 0) smp.main();
  smp.portWrite(addr, data);
}

auto CPU::writeCPU(uint24 addr, uint8 data) -> void {
  switch(addr & 0xffff) {
  case 0x2180: {
    *io.wramAddress++ = data;
    if(io.wramAddress > (uint8_t*)wramUpperBound) io.wramAddress -= 0x20000;
    return;
  }

  case 0x2181: {
    io.wramAddressLo = data;
    return;
  }

  case 0x2182: {
    io.wramAddressHi = data;
    return;
  }

  case 0x2183: {
    io.wramAddressDb = (io.wramAddressDb & 0xfe) | (data & 1);
    return;
  }

  case 0x4016: {
    controllerPort1.latch(data & 1);
    controllerPort2.latch(data & 1);
    return;
  }

  case 0x4200: {
    bool nmiEnable = io.nmiEnable;

    io.autoJoypadPoll = data & 0x01;
    io.hirqEnable = data & 0x10;
    io.virqEnable = data & 0x20;
    io.nmiEnable = data & 0x80;

    if(!nmiEnable && io.nmiEnable && status.nmi_line) {
      status.nmi_transition = true;
    }

    if(io.virqEnable && !io.hirqEnable && status.irq_line) {
      status.irq_transition = true;
    }

    if(!io.virqEnable && !io.hirqEnable) {
      status.irq_line = false;
      status.irq_transition = false;
    }

    status.irq_lock = true;
    return;
  }

  case 0x4201: {
    if((io.pio & 0x80) && !(data & 0x80)) ppu.latchCounters();
    io.pio = data;
  }

  case 0x4202: {
    io.wrmpya = data;
    return;
  }

  case 0x4203: {
    io.wrmpyb = data;
    io.rdmpy = io.wrmpya * io.wrmpyb;
    return;
  }

  case 0x4204: {
    io.wrdiva = (io.wrdiva & 0xff00) | (data << 0);
    return;
  }

  case 0x4205: {
    io.wrdiva = (data << 8) | (io.wrdiva & 0x00ff);
    return;
  }

  case 0x4206: {
    io.wrdivb = data;
    io.rddiv = io.wrdivb ? io.wrdiva / io.wrdivb : 0xffff;
    io.rdmpy = io.wrdivb ? io.wrdiva % io.wrdivb : (uint)io.wrdiva;
    return;
  }

  case 0x4207: {
    io.htime = (io.htime & 0x0100) | (data << 0);
    return;
  }

  case 0x4208: {
    io.htime = ((data & 1) << 8) | (io.htime & 0x00ff);
    return;
  }

  case 0x4209: {
    io.vtime = (io.vtime & 0x0100) | (data << 0);
    return;
  }

  case 0x420a: {
    io.vtime = ((data & 1) << 8) | (io.vtime & 0x00ff);
    return;
  }

  case 0x420b: {
    channel[0].dma_enabled = data & 0x01;
    channel[1].dma_enabled = data & 0x02;
    channel[2].dma_enabled = data & 0x04;
    channel[3].dma_enabled = data & 0x08;
    channel[4].dma_enabled = data & 0x10;
    channel[5].dma_enabled = data & 0x20;
    channel[6].dma_enabled = data & 0x40;
    channel[7].dma_enabled = data & 0x80;
    if(data) dma_run();
    return;
  }

  case 0x420c: {
    channel[0].hdma_enabled = data & 0x01;
    channel[1].hdma_enabled = data & 0x02;
    channel[2].hdma_enabled = data & 0x04;
    channel[3].hdma_enabled = data & 0x08;
    channel[4].hdma_enabled = data & 0x10;
    channel[5].hdma_enabled = data & 0x20;
    channel[6].hdma_enabled = data & 0x40;
    channel[7].hdma_enabled = data & 0x80;
    return;
  }

  case 0x420d: {
    io.romSpeed = data & 1 ? 6 : 8;
    return;
  }
  }
}

auto CPU::writeDMA(uint24 addr, uint8 data) -> void {
  auto& channel = this->channel[addr.bits(4,6)];

  switch(addr & 0xff8f) {
  case 0x4300: {
    channel.direction = data & 0x80;
    channel.indirect = data & 0x40;
    channel.unused = data & 0x20;
    channel.reverse_transfer = data & 0x10;
    channel.fixed_transfer = data & 0x08;
    channel.transfer_mode = data & 0x07;
    return;
  }

  case 0x4301: {
    channel.dest_addr = data;
    return;
  }

  case 0x4302: {
    channel.source_addr = (channel.source_addr & 0xff00) | (data << 0);
    return;
  }

  case 0x4303: {
    channel.source_addr = (data << 8) | (channel.source_addr & 0x00ff);
    return;
  }

  case 0x4304: {
    channel.source_bank = data;
    return;
  }

  case 0x4305: {
    channel.transfer_size = (channel.transfer_size & 0xff00) | (data << 0);
    return;
  }

  case 0x4306: {
    channel.transfer_size = (data << 8) | (channel.transfer_size & 0x00ff);
    return;
  }

  case 0x4307: {
    channel.indirect_bank = data;
    return;
  }

  case 0x4308: {
    channel.hdma_addr = (channel.hdma_addr & 0xff00) | (data << 0);
    return;
  }

  case 0x4309: {
    channel.hdma_addr = (data << 8) | (channel.hdma_addr & 0x00ff);
    return;
  }

  case 0x430a: {
    channel.line_counter = data;
    return;
  }

  case 0x430b: case 0x430f: {
    channel.unknown = data;
    return;
  }
  }
}
