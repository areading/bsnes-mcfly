#include <sfc-performance/sfc.hpp>
#include <stdlib.h>

namespace higan::SuperFamicom {

CPU cpu;

#define WDC65816 CPU
#include "core/core.cpp"
#undef WDC65816

#include "serialization.cpp"
#include "dma.cpp"
#include "memory.cpp"
#include "io.cpp"
#include "timing.cpp"

CPU::CPU() : queue(512, {&CPU::queue_event, this}) {
  wram = reinterpret_cast<uint8*>(aligned_alloc(131072, 131072));
  wramUpperBound = wram + 0x1ffff;
}

CPU::~CPU() {
  free(reinterpret_cast<void*>(wram));
}

auto CPU::Enter() -> void {
  while(true) scheduler.synchronize(), cpu.main();
}

auto CPU::main() -> void {
  if(r.wai) return instructionWait();
  if(r.stp) return instructionStop();

  if(status.nmi_pending) {
    status.nmi_pending = false;
    r.vector = r.e ? 0xfffa : 0xffea;
    interrupt();
  }

  if(status.irq_pending) {
    status.irq_pending = false;
    r.vector = r.e ? 0xfffe : 0xffee;
    interrupt();
  }

  instruction();
}

auto CPU::interrupt() -> void {
  read(r.pc);
  idle();
  if(!r.e) push(r.pc.byte(2));
  push(r.pc.byte(1));
  push(r.pc.byte(0));
  push(r.e ? r.p & ~0x10 : r.p);
  r.p.i = 1;
  r.p.d = 0;
  r.pc.byte(0) = read(r.vector + 0);
  r.pc.byte(1) = read(r.vector + 1);
  r.pc.byte(2) = 0x00;
}

auto CPU::load() -> bool {
  return true;
}

auto CPU::power(bool reset) -> void {
  r.pc = 0x000000;
  r.a  = 0x0000;
  r.x  = 0x0000;
  r.y  = 0x0000;
  r.s  = 0x01ff;
  r.d  = 0x0000;
  r.b  = 0x00;
  r.p  = 0x34;
  r.e  = 1;

  r.irq = false;
  r.wai = false;
  r.stp = false;
  r.mdr = 0x00;
  r.vector = 0xfffc;  //reset vector address

  bus.wramOffset = wram - 0x7e0000;

  create(Enter, system.cpuFrequency());
  coprocessors.reset();
  PPUcounter::reset();
  PPUcounter::scanline = {&CPU::scanline, this};

  if(!reset) random.array(wram, sizeof(wram));

  r.pc.byte(0) = bus.read(0xfffc, r.mdr);
  r.pc.byte(1) = bus.read(0xfffd, r.mdr);
  r.pc.byte(2) = 0x00;

  status.nmi_valid = false;
  status.nmi_line = false;
  status.nmi_transition = false;
  status.nmi_pending = false;

  status.irq_valid = false;
  status.irq_line = false;
  status.irq_transition = false;
  status.irq_pending = false;

  status.irq_lock = false;
  status.hdma_pending = false;

  io = {};
  io.wramAddress = (uint8_t*)wram;

  dma_reset();
}

}
