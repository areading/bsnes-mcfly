auto ControllerPort::superMultitapData() -> uint2 {
  if(latched) return 2;  //device detection
  uint counter;

  if(cpu.pio() & (port == ID::Port::Controller1 ? 0x40 : 0x80)) {
    counter = counter1;
    if(counter >= 16) return 3;
    counter1++;
  } else {
    counter = counter2;
    if(counter >= 16) return 3;
    counter2++;
  }

  return (joy0 >> (15 - counter) & 1) | (joy1 >> (14 - counter) & 2);
}

auto ControllerPort::superMultitapLatch(bool data) -> void {
  if(latched == data) return;
  latched = data;
  counter1 = 0;
  counter2 = 0;
  bool iobit = cpu.pio() & (port == ID::Port::Controller1 ? 0x40 : 0x80);

  if(latched == 0) {
    auto poll = [&](uint offset) -> uint16_t {
      uint16_t data = 0x0000;
      data |= platform->inputPoll(port, ID::Device::SuperMultitap, offset + SuperMultitap::B)      << 15;
      data |= platform->inputPoll(port, ID::Device::SuperMultitap, offset + SuperMultitap::Y)      << 14;
      data |= platform->inputPoll(port, ID::Device::SuperMultitap, offset + SuperMultitap::Select) << 13;
      data |= platform->inputPoll(port, ID::Device::SuperMultitap, offset + SuperMultitap::Start)  << 12;
      data |= platform->inputPoll(port, ID::Device::SuperMultitap, offset + SuperMultitap::Up)     << 11;
      data |= platform->inputPoll(port, ID::Device::SuperMultitap, offset + SuperMultitap::Down)   << 10;
      data |= platform->inputPoll(port, ID::Device::SuperMultitap, offset + SuperMultitap::Left)   <<  9;
      data |= platform->inputPoll(port, ID::Device::SuperMultitap, offset + SuperMultitap::Right)  <<  8;
      data |= platform->inputPoll(port, ID::Device::SuperMultitap, offset + SuperMultitap::A)      <<  7;
      data |= platform->inputPoll(port, ID::Device::SuperMultitap, offset + SuperMultitap::X)      <<  6;
      data |= platform->inputPoll(port, ID::Device::SuperMultitap, offset + SuperMultitap::L)      <<  5;
      data |= platform->inputPoll(port, ID::Device::SuperMultitap, offset + SuperMultitap::R)      <<  4;
      return data;
    };
    joy0 = poll(iobit ?  0 : 24);
    joy1 = poll(iobit ? 12 : 36);
    superMultitapBackupJoy0 = poll(iobit ? 24 :  0);
    superMultitapBackupJoy1 = poll(iobit ? 36 : 12);
  }
}
