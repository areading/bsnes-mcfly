struct LightGun : Thread {
  static auto Enter() -> void;

  virtual auto main() -> void = 0;
  auto iobit() -> bool;
  auto iobit(bool data) -> void;
  virtual auto data() -> uint2 { return 0; }
  virtual auto latch(bool data) -> void {}
};

struct ControllerPort {
  auto connect(uint deviceID) -> void;

  auto data() -> uint2;
  auto latch(bool data) -> void;

  auto power(uint port) -> void;
  auto unload() -> void;
  auto serialize(serializer&) -> void;

  uint port;
  uint deviceID;

  bool latched;
  uint counter1;
  uint counter2;
  LightGun* lightgun = nullptr;

  union {
    uint16_t joy0;
    struct { uint8_t order_lsb2(joy0l, joy0h); };
  };
  union {
    uint16_t joy1;
    struct { uint8_t order_lsb2(joy1l, joy1h); };
  };

private:
  alwaysinline auto gamepadData() -> uint2;
  alwaysinline auto gamepadLatch(bool data) -> void;

  alwaysinline auto mouseData() -> uint2;
  alwaysinline auto mouseLatch(bool data) -> void;
  uint mouseSpeed;  //0 = slow, 1 = normal, 2 = fast
  int  mouseX;      //x-coordinate
  int  mouseY;      //y-coordinate
  bool mouseDX;     //x-direction
  bool mouseDY;     //y-direction
  bool mouseL;      //left button
  bool mouseR;      //right button

  auto superMultitapData() -> uint2;
  auto superMultitapLatch(bool data) -> void;
  uint16_t superMultitapBackupJoy0;
  uint16_t superMultitapBackupJoy1;
};

struct Gamepad { enum : uint {
  Up, Down, Left, Right, B, A, Y, X, L, R, Select, Start,
};};

struct Mouse { enum : uint {
  X, Y, Left, Right,
};};

struct SuperMultitap { enum : uint {
  Up, Down, Left, Right, B, A, Y, X, L, R, Select, Start,
};};

extern ControllerPort controllerPort1;
extern ControllerPort controllerPort2;

#include "super-scope/super-scope.hpp"
#include "justifier/justifier.hpp"
