auto ControllerPort::mouseData() -> uint2 {
  if(latched == 1) {
    uint16_t mouseSpeed = (joy0 + 16) & 0x0030;
    if(mouseSpeed == 0x0030) mouseSpeed = 0x0000;
    joy0 &= 0x00c1;
    joy0 |= mouseSpeed;
    return 0;
  }

  if(counter1 >= 32) return 1;
  if(counter1 <  16) return (joy0 >> (15 - counter1++)) & 1;

  switch(counter1++) {
  case 16: return mouseDY;
  case 17: return (mouseY >> 6) & 1;
  case 18: return (mouseY >> 5) & 1;
  case 19: return (mouseY >> 4) & 1;
  case 20: return (mouseY >> 3) & 1;
  case 21: return (mouseY >> 2) & 1;
  case 22: return (mouseY >> 1) & 1;
  case 23: return (mouseY >> 0) & 1;

  case 24: return mouseDX;
  case 25: return (mouseX >> 6) & 1;
  case 26: return (mouseX >> 5) & 1;
  case 27: return (mouseX >> 4) & 1;
  case 28: return (mouseX >> 3) & 1;
  case 29: return (mouseX >> 2) & 1;
  case 30: return (mouseX >> 1) & 1;
  case 31: return (mouseX >> 0) & 1;
  }
  unreachable;
}

auto ControllerPort::mouseLatch(bool data) -> void {
  if(latched == data) return;
  latched = data;
  counter1 = 0;

  joy0 &= 0x0031;
  mouseX = platform->inputPoll(port, ID::Device::Mouse, Mouse::X);  //-n = left, 0 = center, +n = right
  mouseY = platform->inputPoll(port, ID::Device::Mouse, Mouse::Y);  //-n = up,   0 = center, +n = down
  joy0  |= platform->inputPoll(port, ID::Device::Mouse, Mouse::Left)  << 6;
  joy0  |= platform->inputPoll(port, ID::Device::Mouse, Mouse::Right) << 7;

  mouseDX = mouseX < 0;  //0 = right, 1 = left
  mouseDY = mouseY < 0;  //0 = down,  1 = up

  if(mouseX < 0) mouseX = -mouseX;  //abs(position_x)
  if(mouseY < 0) mouseY = -mouseY;  //abs(position_y)

  double multiplier = 1.0;
  if(mouseSpeed == 1) multiplier = 1.5;
  if(mouseSpeed == 2) multiplier = 2.0;
  mouseX = (double)mouseX * multiplier;
  mouseY = (double)mouseY * multiplier;

  mouseX = min(127, mouseX);
  mouseY = min(127, mouseY);
}
