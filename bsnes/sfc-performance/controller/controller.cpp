#include <sfc-performance/sfc.hpp>

namespace higan::SuperFamicom {

ControllerPort controllerPort1;
ControllerPort controllerPort2;
#include "gamepad.cpp"
#include "mouse.cpp"
#include "super-multitap.cpp"
#include "super-scope/super-scope.cpp"
#include "justifier/justifier.cpp"

auto LightGun::Enter() -> void {
  while(true) {
    scheduler.synchronize();
    controllerPort2.lightgun->main();
  }
}

auto LightGun::iobit() -> bool {
  return cpu.pio() & 0x80;
}

auto LightGun::iobit(bool data) -> void {
  bus.write(0x4201, (cpu.pio() & ~0x80) | (data << 7));
}

//

auto ControllerPort::connect(uint deviceID_) -> void {
  if(!system.loaded()) return;
  deviceID = deviceID_;

  switch(deviceID) {
  case ID::Device::None:
    joy0 = 0x0000;
    joy1 = 0x0000;
    break;
  case ID::Device::Gamepad:
    joy0 = 0x0000;
    joy1 = 0x0000;
    break;
  case ID::Device::Mouse:
    joy0 = 0x0001;
    joy1 = 0x0000;
    mouseSpeed = 0;
    mouseX = 0;
    mouseY = 0;
    mouseDX = 0;
    mouseDY = 0;
    mouseL = 0;
    mouseR = 0;
    break;
  case ID::Device::SuperMultitap:
    joy0 = 0x0000;
    joy1 = 0x0000;
    superMultitapBackupJoy0 = 0x0000;
    superMultitapBackupJoy1 = 0x0000;
    break;
  case ID::Device::SuperScope:
    joy0 = 0x0000;
    joy1 = 0x0000;
    break;
  case ID::Device::Justifier:
  case ID::Device::Justifiers:
    joy0 = 0x000e;
    joy1 = 0x0000;
    break;
  }

  latched = 0;
  counter1 = 0;
  counter2 = 0;

  if(port == ID::Port::Controller2) {
    unload();
    if(controllerPort2.deviceID == ID::Device::SuperScope) {
      lightgun = new SuperScope();
    }
    if(controllerPort2.deviceID == ID::Device::Justifier) {
      lightgun = new Justifier(false);
    }
    if(controllerPort2.deviceID == ID::Device::Justifiers) {
      lightgun = new Justifier(true);
    }
  }
}

auto ControllerPort::data() -> uint2 {
  switch(deviceID) {
  case ID::Device::Gamepad: return gamepadData();
  case ID::Device::Mouse: return mouseData();
  case ID::Device::SuperMultitap: return superMultitapData();
  case ID::Device::SuperScope:
  case ID::Device::Justifier:
  case ID::Device::Justifiers: return lightgun->data();
  }
  return 0;
}

auto ControllerPort::latch(bool data) -> void {
  switch(deviceID) {
  case ID::Device::Gamepad: return gamepadLatch(data);
  case ID::Device::Mouse: return mouseLatch(data);
  case ID::Device::SuperMultitap: return superMultitapLatch(data);
  case ID::Device::SuperScope:
  case ID::Device::Justifier:
  case ID::Device::Justifiers: return lightgun->latch(data);
  }
}

auto ControllerPort::power(uint port) -> void {
  this->port = port;
}

auto ControllerPort::unload() -> void {
  if(lightgun) delete lightgun;
  lightgun = nullptr;
}

auto ControllerPort::serialize(serializer& s) -> void {
}

}
