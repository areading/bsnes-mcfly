SuperScope::SuperScope() {
  create(LightGun::Enter, system.cpuFrequency());
  sprite = video.createSprite(32, 32);
  scale = !ppu.hires() || !ppu.interlace();
  sprite->setPixels(scale ? (
    (image)Resource::Sprite::CrosshairGreenSmall
  ) : (
    (image)Resource::Sprite::CrosshairGreen
  ));

  //center cursor onscreen
  x = 256 / 2;
  y = 240 / 2;

  trigger   = false;
  cursor    = false;
  turbo     = false;
  pause     = false;
  offscreen = false;

  oldturbo    = false;
  triggerlock = false;
  pauselock   = false;

  prev = 0;
}

SuperScope::~SuperScope() {
  video.removeSprite(sprite);
  scheduler.remove(*this);
}

auto SuperScope::main() -> void {
  uint next = cpu.vcounter() * 1364 + cpu.hcounter();

  if(!offscreen) {
    uint target = y * 1364 + (x + 24) * 4;
    if(next >= target && prev < target) {
      //CRT raster detected, toggle iobit to latch counters
      iobit(0);
      iobit(1);
    }
  }

  int multiplierX = ppu.hires()     ? 2 : 1;
  int multiplierY = ppu.interlace() ? 2 : 1;

  if(next < prev) {
    //Vcounter wrapped back to zero; update cursor coordinates for start of new frame
    int nx = platform->inputPoll(ID::Port::Controller2, ID::Device::SuperScope, X);
    int ny = platform->inputPoll(ID::Port::Controller2, ID::Device::SuperScope, Y);
    nx += x;
    ny += y;
    x = max(-16, min(256 + 16, nx));
    y = max(-16, min(240 + 16, ny));
    offscreen = (x < 0 || y < 0 || x >= 256 || y >= ppu.vdisp());
    sprite->setPosition(x * multiplierX - 16, y * multiplierY - 16);
    sprite->setVisible(true);
  }

  bool scale_ = !ppu.hires() || !ppu.interlace();
  if(scale != scale_) {
    scale = scale_;
    sprite->setPixels(scale ? (
      turbo ? (image)Resource::Sprite::CrosshairRedSmall : (image)Resource::Sprite::CrosshairGreenSmall
    ) : (
      turbo ? (image)Resource::Sprite::CrosshairRed : (image)Resource::Sprite::CrosshairGreen
    ));
  }

  prev = next;
  step(2);
  synchronize(cpu);
}

auto SuperScope::data() -> uint2 {
  if(controllerPort2.counter1 >= 8) return 1;

  if(controllerPort2.counter1 == 0) {
    //turbo is a switch; toggle is edge sensitive
    bool newturbo = platform->inputPoll(ID::Port::Controller2, ID::Device::SuperScope, Turbo);
    if(newturbo && !oldturbo) {
      turbo = !turbo;  //toggle state
      sprite->setPixels(scale ? (
        turbo ? (image)Resource::Sprite::CrosshairRedSmall : (image)Resource::Sprite::CrosshairGreenSmall
      ) : (
        turbo ? (image)Resource::Sprite::CrosshairRed : (image)Resource::Sprite::CrosshairGreen
      ));
    }
    oldturbo = newturbo;

    //trigger is a button
    //if turbo is active, trigger is level sensitive; otherwise, it is edge sensitive
    trigger = false;
    bool newtrigger = platform->inputPoll(ID::Port::Controller2, ID::Device::SuperScope, Trigger);
    if(newtrigger && (turbo || !triggerlock)) {
      trigger = true;
      triggerlock = true;
    } else if(!newtrigger) {
      triggerlock = false;
    }

    //cursor is a button; it is always level sensitive
    cursor = platform->inputPoll(ID::Port::Controller2, ID::Device::SuperScope, Cursor);

    //pause is a button; it is always edge sensitive
    pause = false;
    bool newpause = platform->inputPoll(ID::Port::Controller2, ID::Device::SuperScope, Pause);
    if(newpause && !pauselock) {
      pause = true;
      pauselock = true;
    } else if(!newpause) {
      pauselock = false;
    }

    offscreen = (x < 0 || y < 0 || x >= 256 || y >= ppu.vdisp());
  }

  switch(controllerPort2.counter1++) {
  case 0: return offscreen ? 0 : trigger;
  case 1: return cursor;
  case 2: return turbo;
  case 3: return pause;
  case 4: return 0;
  case 5: return 0;
  case 6: return offscreen;
  case 7: return 0;  //noise (1 = yes)
  }

  unreachable;
}

auto SuperScope::latch(bool data) -> void {
  if(controllerPort2.latched == data) return;
  controllerPort2.latched = data;
  controllerPort2.counter1 = 0;
  controllerPort2.joy0 = 0x00ff | (!offscreen && trigger) << 15 | cursor << 14 | turbo << 13 | pause << 12 | offscreen << 9;
}
