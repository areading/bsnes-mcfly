struct HitachiDSP {
  ReadableMemory rom;
  WritableMemory ram;

  //hitachidsp.cpp
  static auto Enter() -> void;

  auto unload() -> void;
  auto power() -> void;

  //HG51B read/write
  auto read(unsigned addr) -> uint8;
  auto write(unsigned addr, uint8 data) -> void;

  //CPU ROM read/write
  auto readROM(uint24 addr, uint8 data) -> uint8;
  auto writeROM(uint24 addr, uint8 data) -> void;

  //CPU RAM read/write
  auto readRAM(uint24 addr, uint8 data) -> uint8;
  auto writeRAM(uint24 addr, uint8 data) -> void;

  //HG51B data RAM read/write
  auto readDRAM(uint24 addr, uint8 data) -> uint8;
  auto writeDRAM(uint24 addr, uint8 data) -> void;

  //CPU IO read/write
  auto readIO(uint24 addr, uint8 data) -> uint8;
  auto writeIO(uint24 addr, uint8 data) -> void;

  auto firmware() const -> vector<uint8>;
  auto serialize(serializer&) -> void;

  uint24 dataROM[1024];
  uint8  dataRAM[3072];

  uint Frequency;
  uint Roms;
  bool Mapping;

private:
  uint8_t  reg[0x0100];
  uint32_t r0, r1, r2,  r3,  r4,  r5,  r6,  r7,
           r8, r9, r10, r11, r12, r13, r14, r15;

  static const uint8_t  immediate_data[48];
  static const uint16_t wave_data[40];
  static const uint32_t sin_table[256];

  static const int16_t SinTable[512];
  static const int16_t CosTable[512];

  int16_t C4WFXVal, C4WFYVal, C4WFZVal, C4WFX2Val, C4WFY2Val, C4WFDist, C4WFScale;
  int16_t C41FXVal, C41FYVal, C41FAngleRes, C41FDist, C41FDistVal;

  void C4TransfWireFrame();
  void C4TransfWireFrame2();
  void C4CalcWireFrame();
  void C4DrawLine(int32_t X1, int32_t Y1, int16_t Z1, int32_t X2, int32_t Y2, int16_t Z2, uint8_t Color);
  void C4DrawWireFrame();
  void C4DoScaleRotate(int row_padding);

public:
  uint32_t ldr(uint8_t r);
  void     str(uint8_t r, uint32_t data);
  void     mul(uint32_t x, uint32_t y, uint32_t &rl, uint32_t &rh);
  uint32_t sin(uint32_t rx);
  uint32_t cos(uint32_t rx);

  void transfer_data();
  void immediate_reg(uint32_t num);

  void op000000();

  void op000205();
  void op00020d();
  void op000210();
  void op000213();
  void op000215();
  void op00021f();
  void op000222();
  void op000225();
  void op00022d();

  void op000300();

  void op000500();

  void op000604();

  void op000700();

  void op000800();
  void op000801();

  void op000b00();

  void op000c00();

  void op000e40();
  void op000e54();
  void op000e5c();
  void op000e5e();
  void op000e60();
  void op000e62();
  void op000e64();
  void op000e66();
  void op000e68();
  void op000e6a();
  void op000e6c();
  void op000e6e();
  void op000e70();
  void op000e72();
  void op000e74();
  void op000e76();
  void op000e78();
  void op000e7a();
  void op000e7c();
  void op000e89();

  uint8_t  readb(uint16_t addr);
  uint16_t readw(uint16_t addr);
  uint32_t readl(uint16_t addr);

  void writeb(uint16_t addr, uint8_t data);
  void writew(uint16_t addr, uint16_t data);
  void writel(uint16_t addr, uint32_t data);
};

extern HitachiDSP hitachidsp;
