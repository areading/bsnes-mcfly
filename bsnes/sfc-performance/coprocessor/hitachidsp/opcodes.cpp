//Build OAM
void HitachiDSP::op000000() {
  uint32_t oamptr = dataRAM[0x626] << 2;
  for(int32_t i = 0x1fd; i > oamptr && i >= 0; i -= 4) {
    //clear oam-to-be
    if(i >= 0) dataRAM[i] = 0xe0;
  }

  uint16_t globalx, globaly;
  uint32_t oamptr2;
  int16_t  sprx, spry;
  uint8_t  sprname, sprattr;
  uint8_t  sprcount;

  globalx = readw(0x621);
  globaly = readw(0x623);
  oamptr2 = 0x200 + (dataRAM[0x626] >> 2);

  if(!dataRAM[0x620]) return;

  sprcount = 128 - dataRAM[0x626];
  uint8_t offset = (dataRAM[0x626] & 3) * 2;
  uint32_t srcptr = 0x220;

  uint8_t mdr = cpu.r.mdr;
  for(int i = dataRAM[0x620]; i > 0 && sprcount > 0; i--, srcptr += 16) {
    sprx = readw(srcptr)     - globalx;
    spry = readw(srcptr + 2) - globaly;
    sprname = dataRAM[srcptr + 5];
    sprattr = dataRAM[srcptr + 4] | dataRAM[srcptr + 6];

    uint32_t spraddr = readl(srcptr + 7);
    if(bus.read(spraddr, mdr)) {
      int16_t x, y;
      for(int sprcnt = bus.read(spraddr++, mdr); sprcnt > 0 && sprcount > 0; sprcnt--, spraddr += 4) {
        x = (int8)bus.read(spraddr + 1, mdr);
        if(sprattr & 0x40) {
          x = -x - ((bus.read(spraddr, mdr) & 0x20) ? 16 : 8);
        }
        x += sprx;
        if(x >= -16 && x <= 272) {
          y = (int8)bus.read(spraddr + 2, mdr);
          if(sprattr & 0x80) {
            y = -y - ((bus.read(spraddr, mdr) & 0x20) ? 16 : 8);
          }
          y += spry;
          if(y >= -16 && y <= 224) {
            dataRAM[oamptr    ] = (uint8_t)x;
            dataRAM[oamptr + 1] = (uint8_t)y;
            dataRAM[oamptr + 2] = sprname + bus.read(spraddr + 3, mdr);
            dataRAM[oamptr + 3] = sprattr ^ (bus.read(spraddr, mdr) & 0xc0);
            dataRAM[oamptr2] &= ~(3 << offset);
            if(x & 0x100) dataRAM[oamptr2] |= 1 << offset;
            if(bus.read(spraddr, mdr) & 0x20) dataRAM[oamptr2] |= 2 << offset;
            oamptr += 4;
            sprcount--;
            offset = (offset + 2) & 6;
            if(!offset)oamptr2++;
          }
        }
      }
    } else if(sprcount > 0) {
      dataRAM[oamptr    ] = (uint8_t)sprx;
      dataRAM[oamptr + 1] = (uint8_t)spry;
      dataRAM[oamptr + 2] = sprname;
      dataRAM[oamptr + 3] = sprattr;
      dataRAM[oamptr2] &= ~(3 << offset);
      if(sprx & 0x100) dataRAM[oamptr2] |= 3 << offset;
      else dataRAM[oamptr2] |= 2 << offset;
      oamptr += 4;
      sprcount--;
      offset = (offset + 2) & 6;
      if(!offset) oamptr2++;
    }
  }
}

//Propulsion
void HitachiDSP::op000205() {
  int32 temp = 0x10000;
  if(readw(0x1f83)) {
    temp = sar((temp / readw(0x1f83)) * readw(0x1f81), 8);
  }
  writew(0x1f80, temp);
}

//Set Vector length
void HitachiDSP::op00020d() {
  C41FXVal    = readw(0x1f80);
  C41FYVal    = readw(0x1f83);
  C41FDistVal = readw(0x1f86);
  double tanval = sqrt(((double)C41FYVal) * ((double)C41FYVal) + ((double)C41FXVal) * ((double)C41FXVal));
  tanval = (double)C41FDistVal / tanval;
  C41FYVal = (int16)(((double)C41FYVal * tanval) * 0.99);
  C41FXVal = (int16)(((double)C41FXVal * tanval) * 0.98);
  writew(0x1f89, C41FXVal);
  writew(0x1f8c, C41FYVal);
}

//Triangle
void HitachiDSP::op000210() {
  r0 = ldr(0);
  r1 = ldr(1);

  r4 = r0 & 0x1ff;
  if(r1 & 0x8000)r1 |= ~0x7fff;
  else r1 &= 0x7fff;

  mul(cos(r4), r1, r5, r2);
  r5 = (r5 >> 16) & 0xff;
  r2 = (r2 << 8) + r5;

  mul(sin(r4), r1, r5, r3);
  r5 = (r5 >> 16) & 0xff;
  r3 = (r3 << 8) + r5;

  str(0, r0);
  str(1, r1);
  str(2, r2);
  str(3, r3);
  str(4, r4);
  str(5, r5);
}

//Triangle
void HitachiDSP::op000213() {
  r0 = ldr(0);
  r1 = ldr(1);

  r4 = r0 & 0x1ff;

  mul(cos(r4), r1, r5, r2);
  r5 = (r5 >> 8) & 0xffff;
  r2 = (r2 << 16) + r5;

  mul(sin(r4), r1, r5, r3);
  r5 = (r5 >> 8) & 0xffff;
  r3 = (r3 << 16) + r5;

  str(0, r0);
  str(1, r1);
  str(2, r2);
  str(3, r3);
  str(4, r4);
  str(5, r5);
}

//Pythagorean
void HitachiDSP::op000215() {
  C41FXVal = readw(0x1f80);
  C41FYVal = readw(0x1f83);
  C41FDist = (int16)sqrt((double)C41FXVal * (double)C41FXVal + (double)C41FYVal * (double)C41FYVal);
  writew(0x1f80, C41FDist);
}

//Calculate distance
void HitachiDSP::op00021f() {
  C41FXVal = readw(0x1f80);
  C41FYVal = readw(0x1f83);
  if(!C41FXVal) {
    C41FAngleRes = (C41FYVal > 0) ? 0x080 : 0x180;
  } else {
    double tanval = ((double)C41FYVal) / ((double)C41FXVal);
    C41FAngleRes = (short)(atan(tanval) / (Math::Pi * 2) * 512);
    C41FAngleRes = C41FAngleRes;
    if(C41FXVal < 0) {
      C41FAngleRes += 0x100;
    }
    C41FAngleRes &= 0x1ff;
  }
  writew(0x1f86, C41FAngleRes);
}

//Trapezoid
void HitachiDSP::op000222() {
  int16 angle1 = readw(0x1f8c) & 0x1ff;
  int16 angle2 = readw(0x1f8f) & 0x1ff;
  int32 tan1 = Tan(angle1);
  int32 tan2 = Tan(angle2);
  int16 y = readw(0x1f83) - readw(0x1f89);
  int16 left, right;

  for(int32 j = 0; j < 225; j++, y++) {
    if(y >= 0) {
      left  = sar((int32)tan1 * y, 16) - readw(0x1f80) + readw(0x1f86);
      right = sar((int32)tan2 * y, 16) - readw(0x1f80) + readw(0x1f86) + readw(0x1f93);

      if(left < 0 && right < 0) {
        left  = 1;
        right = 0;
      } else if(left < 0) {
        left  = 0;
      } else if(right < 0) {
        right = 0;
      }

      if(left > 255 && right > 255) {
        left  = 255;
        right = 254;
      } else if(left > 255) {
        left  = 255;
      } else if(right > 255) {
        right = 255;
      }
    } else {
      left  = 1;
      right = 0;
    }
    dataRAM[j + 0x800] = (uint8)left;
    dataRAM[j + 0x900] = (uint8)right;
  }
}

//Multiply
void HitachiDSP::op000225() {
  r0 = ldr(0);
  r1 = ldr(1);
  mul(r0, r1, r0, r1);
  str(0, r0);
  str(1, r1);
}

//Transform Coords
void HitachiDSP::op00022d() {
  C4WFXVal  = readw(0x1f81);
  C4WFYVal  = readw(0x1f84);
  C4WFZVal  = readw(0x1f87);
  C4WFX2Val = read (0x1f89);
  C4WFY2Val = read (0x1f8a);
  C4WFDist  = read (0x1f8b);
  C4WFScale = readw(0x1f90);
  C4TransfWireFrame2();
  writew(0x1f80, C4WFXVal);
  writew(0x1f83, C4WFYVal);
}

//Scale and Rotate
void HitachiDSP::op000300() {
  C4DoScaleRotate(0);
}

//Transform Lines
void HitachiDSP::op000500() {
  C4WFX2Val = read(0x1f83);
  C4WFY2Val = read(0x1f86);
  C4WFDist  = read(0x1f89);
  C4WFScale = read(0x1f8c);

  //Transform Vertices
  uint32_t ptr = 0;
  for(int32_t i = readw(0x1f80); i > 0; i--, ptr += 0x10) {
    C4WFXVal = readw(ptr + 1);
    C4WFYVal = readw(ptr + 5);
    C4WFZVal = readw(ptr + 9);
    C4TransfWireFrame();

    //Displace
    writew(ptr + 1, C4WFXVal + 0x80);
    writew(ptr + 5, C4WFYVal + 0x50);
  }

  writew(0x600,     23);
  writew(0x602,     0x60);
  writew(0x605,     0x40);
  writew(0x600 + 8, 23);
  writew(0x602 + 8, 0x60);
  writew(0x605 + 8, 0x40);

  ptr = 0xb02;
  uint32_t ptr2 = 0;

  for(int32_t i = readw(0xb00); i > 0; i--, ptr += 2, ptr2 += 8) {
    C4WFXVal  = readw((read(ptr + 0) << 4) + 1);
    C4WFYVal  = readw((read(ptr + 0) << 4) + 5);
    C4WFX2Val = readw((read(ptr + 1) << 4) + 1);
    C4WFY2Val = readw((read(ptr + 1) << 4) + 5);
    C4CalcWireFrame();
    writew(ptr2 + 0x600, C4WFDist ? C4WFDist : 1);
    writew(ptr2 + 0x602, C4WFXVal);
    writew(ptr2 + 0x605, C4WFYVal);
  }
}

//Overload's exploit to read the data ROM
void HitachiDSP::op000604() {
  str(12, dataROM[ldr(11)]);
}

//Scale and Rotate
void HitachiDSP::op000700() {
  C4DoScaleRotate(64);
}

//Draw Wireframe
void HitachiDSP::op000800() {
  C4DrawWireFrame();
}

//Draw Wireframe
void HitachiDSP::op000801() {
  memset(dataRAM + 0x300, 0, 2304);
  C4DrawWireFrame();
}

//Disintegrate
void HitachiDSP::op000b00() {
  uint8_t  width, height;
  uint32_t startx, starty;
  uint32_t srcptr;
  uint32_t x, y;
  int32_t  scalex, scaley;
  int32_t  cx, cy;
  int32_t  i, j;

  width  = read(0x1f89);
  height = read(0x1f8c);
  cx     = readw(0x1f80);
  cy     = readw(0x1f83);

  scalex = (int16_t)readw(0x1f86);
  scaley = (int16_t)readw(0x1f8f);
  startx = -cx * scalex + (cx << 8);
  starty = -cy * scaley + (cy << 8);
  srcptr = 0x600;

  for(i = 0; i < (width * height) >> 1; i++) {
    write(i, 0);
  }

  for(y = starty, i = 0;i < height; i++, y += scaley) {
    for(x = startx, j = 0;j < width; j++, x += scalex) {
      if((x >> 8) < width && (y >> 8) < height && (y >> 8) * width + (x >> 8) < 0x2000) {
        uint8_t pixel = (j & 1) ? uint8(dataRAM[srcptr] >> 4) : uint8(dataRAM[srcptr]);
        int32_t index = (y >> 11) * width * 4 + (x >> 11) * 32 + ((y >> 8) & 7) * 2;
        uint8_t mask = 0x80 >> ((x >> 8) & 7);

        if(pixel & 1) dataRAM[index     ] |= mask;
        if(pixel & 2) dataRAM[index +  1] |= mask;
        if(pixel & 4) dataRAM[index + 16] |= mask;
        if(pixel & 8) dataRAM[index + 17] |= mask;
      }
      if(j & 1) srcptr++;
    }
  }
}

//Bitplane Wave
void HitachiDSP::op000c00() {
  uint32_t destptr = 0;
  uint32_t waveptr = read(0x1f83);
  uint16_t mask1   = 0xc0c0;
  uint16_t mask2   = 0x3f3f;

  for(int j = 0; j < 0x10; j++) {
    do {
      int16_t height = -((int8)read(waveptr + 0xb00)) - 16;
      for(int i = 0; i < 40; i++) {
        uint16_t temp = readw(destptr + wave_data[i]) & mask2;
        if(height >= 0) {
          if(height < 8) {
            temp |= mask1 & readw(0xa00 + height * 2);
          } else {
            temp |= mask1 & 0xff00;
          }
        }
        writew(destptr + wave_data[i], temp);
        height++;
      }
      waveptr = (waveptr + 1) & 0x7f;
      mask1   = (mask1 >> 2) | (mask1 << 6);
      mask2   = (mask2 >> 2) | (mask2 << 6);
    } while(mask1 != 0xc0c0);
    destptr += 16;

    do {
      int16_t height = -((int8)read(waveptr + 0xb00)) - 16;
      for(int i = 0; i < 40; i++) {
        uint16_t temp = readw(destptr + wave_data[i]) & mask2;
        if(height >= 0) {
          if(height < 8) {
            temp |= mask1 & readw(0xa10 + height * 2);
          } else {
            temp |= mask1 & 0xff00;
          }
        }
        writew(destptr + wave_data[i], temp);
        height++;
      }
      waveptr = (waveptr + 1) & 0x7f;
      mask1   = (mask1 >> 2) | (mask1 << 6);
      mask2   = (mask2 >> 2) | (mask2 << 6);
    } while(mask1 != 0xc0c0);
    destptr += 16;
  }
}

//Sum
void HitachiDSP::op000e40() {
  r0 = 0;
  for(uint32 i=0;i<0x800;i++) {
    r0 += dataRAM[i];
  }
  str(0, r0);
}

//Square
void HitachiDSP::op000e54() {
  r0 = ldr(0);
  mul(r0, r0, r1, r2);
  str(1, r1);
  str(2, r2);
}

//Immediate Register
void HitachiDSP::op000e5c() {
  str(0, 0x000000);
  immediate_reg(0);
}

//Immediate Register (Multiple)
void HitachiDSP::op000e5e() { immediate_reg( 0); }
void HitachiDSP::op000e60() { immediate_reg( 3); }
void HitachiDSP::op000e62() { immediate_reg( 6); }
void HitachiDSP::op000e64() { immediate_reg( 9); }
void HitachiDSP::op000e66() { immediate_reg(12); }
void HitachiDSP::op000e68() { immediate_reg(15); }
void HitachiDSP::op000e6a() { immediate_reg(18); }
void HitachiDSP::op000e6c() { immediate_reg(21); }
void HitachiDSP::op000e6e() { immediate_reg(24); }
void HitachiDSP::op000e70() { immediate_reg(27); }
void HitachiDSP::op000e72() { immediate_reg(30); }
void HitachiDSP::op000e74() { immediate_reg(33); }
void HitachiDSP::op000e76() { immediate_reg(36); }
void HitachiDSP::op000e78() { immediate_reg(39); }
void HitachiDSP::op000e7a() { immediate_reg(42); }
void HitachiDSP::op000e7c() { immediate_reg(45); }

//Immediate ROM
void HitachiDSP::op000e89() {
  uint sum = 0;
  for(uint24 word : dataROM) {
    sum += word.byte(0) + word.byte(1) + word.byte(2);
  }
  str(0, sum & 0xffffff);
  str(1, 0xffffff);
}
