//DSP-4 emulator code
//Copyright (c) 2004-2006 Dreamer Nom, John Weidman, Kris Bleakley, Nach, z80 gaiden

/*
Due recognition and credit are given on Overload's DSP website.
Thank those contributors for their hard work on this chip.


Fixed-point math reminder:

[sign, integer, fraction]
1.15.00 * 1.15.00 = 2.30.00 -> 1.30.00 (DSP) -> 1.31.00 (LSB is '0')
1.15.00 * 1.00.15 = 2.15.15 -> 1.15.15 (DSP) -> 1.15.16 (LSB is '0')
*/

//////////////////////////////////////////////////////////////

// input protocol

int16 DSP4::READ_WORD()
{
  int16 out = (
    protocol.parameters[protocol.in_index + 0] << 0 |
    protocol.parameters[protocol.in_index + 1] << 8
  );
  protocol.in_index += 2;

  return out;
}

int32 DSP4::READ_DWORD()
{
  int32 out = (
    protocol.parameters[protocol.in_index + 0] <<  0 |
    protocol.parameters[protocol.in_index + 1] <<  8 |
    protocol.parameters[protocol.in_index + 2] << 16 |
    protocol.parameters[protocol.in_index + 3] << 24
  );
  protocol.in_index += 4;

  return out;
}


//////////////////////////////////////////////////////////////

// output protocol

void DSP4::CLEAR_OUT() {
  protocol.out_count = 0;
  protocol.out_index = 0;
}

void DSP4::WRITE_BYTE(uint8 data) {
  protocol.output[protocol.out_count + 0] = data;
  protocol.output[protocol.out_count + 1] = 0x00;
  protocol.out_count++;
}

void DSP4::WRITE_WORD(uint16 data) {
  protocol.output[protocol.out_count + 0] = data >> 0;
  protocol.output[protocol.out_count + 1] = data >> 8;
  protocol.out_count += 2;
}

void DSP4::WRITE_16_WORD(int16* data) {
  for(uint n : range(16)) WRITE_WORD(data[n]);
}

//////////////////////////////////////////////////////////////

// used to wait for dsp i/o

#define WAIT( x ) \
  protocol.in_index = 0; vars.Logic = x; return;

//////////////////////////////////////////////////////////////

// 1.7.8 -> 1.15.16
#define SEX78( a ) ( ( (int32) ( (int16) (a) ) ) << 8 )

// 1.15.0 -> 1.15.16
#define SEX16( a ) ( ( (int32) ( (int16) (a) ) ) << 16 )

//////////////////////////////////////////////////////////////

const uint16 DSP4::div_lut[64] = {
  0x0000, 0x4000, 0x2000, 0x1555, 0x1000, 0x0ccc, 0x0aaa, 0x0924,
  0x0800, 0x071c, 0x0666, 0x05d1, 0x0555, 0x04ec, 0x0492, 0x0444,
  0x0400, 0x03c3, 0x038e, 0x035e, 0x0333, 0x030c, 0x02e8, 0x02c8,
  0x02aa, 0x028f, 0x0276, 0x025e, 0x0249, 0x0234, 0x0222, 0x0210,
  0x0200, 0x01f0, 0x01e1, 0x01d4, 0x01c7, 0x01ba, 0x01af, 0x01a4,
  0x0199, 0x018f, 0x0186, 0x017d, 0x0174, 0x016c, 0x0164, 0x015c,
  0x0155, 0x014e, 0x0147, 0x0141, 0x013b, 0x0135, 0x012f, 0x0129,
  0x0124, 0x011f, 0x011a, 0x0115, 0x0111, 0x010c, 0x0108, 0x0104,
};

int16 DSP4::Inverse(int16 value)
{
  // saturate bounds
  if(value <  0) value =  0;
  if(value > 63) value = 63;

  return div_lut[value] << 1;
}

//////////////////////////////////////////////////////////////

// OP00
void DSP4::Multiply(int16 Multiplicand, int16 Multiplier, int32 *Product)
{
  *Product = (Multiplicand * Multiplier << 1) >> 1;
}

//////////////////////////////////////////////////////////////


void DSP4::OP01()
{
  protocol.waiting4command = false;

  // op flow control
  switch (vars.Logic)
  {
    case 1:
      goto resume1; break;
    case 2:
      goto resume2; break;
    case 3:
      goto resume3; break;
  }

  ////////////////////////////////////////////////////
  // process initial inputs

  // sort inputs
  vars.world_y = READ_DWORD();
  vars.poly_bottom[0][0] = READ_WORD();
  vars.poly_top[0][0] = READ_WORD();
  vars.poly_cx[1][0] = READ_WORD();
  vars.viewport_bottom = READ_WORD();
  vars.world_x = READ_DWORD();
  vars.poly_cx[0][0] = READ_WORD();
  vars.poly_ptr[0][0] = READ_WORD();
  vars.world_yofs = READ_WORD();
  vars.world_dy = READ_DWORD();
  vars.world_dx = READ_DWORD();
  vars.distance = READ_WORD();
  READ_WORD(); // 0x0000
  vars.world_xenv = READ_DWORD();
  vars.world_ddy = READ_WORD();
  vars.world_ddx = READ_WORD();
  vars.view_yofsenv = READ_WORD();

  // initial (x,y,offset) at starting vars.raster line
  vars.view_x1 = (int16)((vars.world_x + vars.world_xenv) >> 16);
  vars.view_y1 = (int16)(vars.world_y >> 16);
  vars.view_xofs1 = (int16)(vars.world_x >> 16);
  vars.view_yofs1 = vars.world_yofs;
  vars.view_turnoff_x = 0;
  vars.view_turnoff_dx = 0;

  // first vars.raster line
  vars.poly_raster[0][0] = vars.poly_bottom[0][0];

  do
  {
    ////////////////////////////////////////////////////
    // process one iteration of projection

    // perspective projection of world (x,y,scroll) points
    // based on the current projection lines
    vars.view_x2 = (int16)(( ( ( vars.world_x + vars.world_xenv ) >> 16 ) * vars.distance >> 15 ) + ( vars.view_turnoff_x * vars.distance >> 15 ));
    vars.view_y2 = (int16)((vars.world_y >> 16) * vars.distance >> 15);
    vars.view_xofs2 = vars.view_x2;
    vars.view_yofs2 = (vars.world_yofs * vars.distance >> 15) + vars.poly_bottom[0][0] - vars.view_y2;


    // 1. World x-location before transformation
    // 2. Viewer x-position at the next
    // 3. World y-location before perspective projection
    // 4. Viewer y-position below the horizon
    // 5. Number of vars.raster lines drawn in this iteration

    CLEAR_OUT();
    WRITE_WORD((uint16)((vars.world_x + vars.world_xenv) >> 16));
    WRITE_WORD(vars.view_x2);
    WRITE_WORD((uint16)(vars.world_y >> 16));
    WRITE_WORD(vars.view_y2);

    //////////////////////////////////////////////////////

    // SR = 0x00

    // determine # of vars.raster lines used
    vars.segments = vars.poly_raster[0][0] - vars.view_y2;

    // prevent overdraw
    if (vars.view_y2 >= vars.poly_raster[0][0])
      vars.segments = 0;
    else
      vars.poly_raster[0][0] = vars.view_y2;

    // don't draw outside the window
    if (vars.view_y2 < vars.poly_top[0][0])
    {
      vars.segments = 0;

      // flush remaining vars.raster lines
      if (vars.view_y1 >= vars.poly_top[0][0])
        vars.segments = vars.view_y1 - vars.poly_top[0][0];
    }

    // SR = 0x80

    WRITE_WORD(vars.segments);

    //////////////////////////////////////////////////////

    // scan next command if no SR check needed
    if (vars.segments)
    {
      int32 px_dx, py_dy;
      int32 x_scroll, y_scroll;

      // SR = 0x00

      // linear interpolation (lerp) between projected points
      px_dx = (vars.view_xofs2 - vars.view_xofs1) * Inverse(vars.segments) << 1;
      py_dy = (vars.view_yofs2 - vars.view_yofs1) * Inverse(vars.segments) << 1;

      // starting step values
      x_scroll = SEX16(vars.poly_cx[0][0] + vars.view_xofs1);
      y_scroll = SEX16(-vars.viewport_bottom + vars.view_yofs1 + vars.view_yofsenv + vars.poly_cx[1][0] - vars.world_yofs);

      // SR = 0x80

      // rasterize line
      for (vars.lcv = 0; vars.lcv < vars.segments; vars.lcv++)
      {
        // 1. HDMA memory pointer (bg1)
        // 2. vertical scroll offset ($210E)
        // 3. horizontal scroll offset ($210D)

        WRITE_WORD(vars.poly_ptr[0][0]);
        WRITE_WORD((uint16)((y_scroll + 0x8000) >> 16));
        WRITE_WORD((uint16)((x_scroll + 0x8000) >> 16));


        // update memory address
        vars.poly_ptr[0][0] -= 4;

        // update screen values
        x_scroll += px_dx;
        y_scroll += py_dy;
      }
    }

    ////////////////////////////////////////////////////
    // Post-update

    // update new viewer (x,y,scroll) to last vars.raster line drawn
    vars.view_x1 = vars.view_x2;
    vars.view_y1 = vars.view_y2;
    vars.view_xofs1 = vars.view_xofs2;
    vars.view_yofs1 = vars.view_yofs2;

    // add deltas for projection lines
    vars.world_dx += SEX78(vars.world_ddx);
    vars.world_dy += SEX78(vars.world_ddy);

    // update projection lines
    vars.world_x += (vars.world_dx + vars.world_xenv);
    vars.world_y += vars.world_dy;

    // update road turnoff position
    vars.view_turnoff_x += vars.view_turnoff_dx;

    ////////////////////////////////////////////////////
    // command check

    // scan next command
    protocol.in_count = 2;
    WAIT(1) resume1 :

    // check for termination
    vars.distance = READ_WORD();
    if (vars.distance == -0x8000)
      break;

    // road turnoff
    if( (uint16) vars.distance == 0x8001 )
    {
      protocol.in_count = 6;
      WAIT(2) resume2:

      vars.distance = READ_WORD();
      vars.view_turnoff_x = READ_WORD();
      vars.view_turnoff_dx = READ_WORD();

      // factor in new changes
      vars.view_x1 += ( vars.view_turnoff_x * vars.distance >> 15 );
      vars.view_xofs1 += ( vars.view_turnoff_x * vars.distance >> 15 );

      // update stepping values
      vars.view_turnoff_x += vars.view_turnoff_dx;

      protocol.in_count = 2;
      WAIT(1)
    }

    // already have 2 bytes read
    protocol.in_count = 6;
    WAIT(3) resume3 :

    // inspect inputs
    vars.world_ddy = READ_WORD();
    vars.world_ddx = READ_WORD();
    vars.view_yofsenv = READ_WORD();

    // no envelope here
    vars.world_xenv = 0;
  }
  while (1);

  // terminate op
  protocol.waiting4command = true;
}

//////////////////////////////////////////////////////////////


void DSP4::OP03()
{
  vars.OAM_RowMax = 33;
  memset(vars.OAM_Row, 0, 64);
}


//////////////////////////////////////////////////////////////


void DSP4::OP05()
{
  vars.OAM_index = 0;
  vars.OAM_bits = 0;
  memset(vars.OAM_attr, 0, 32);
  vars.sprite_count = 0;
}


//////////////////////////////////////////////////////////////

void DSP4::OP06()
{
  CLEAR_OUT();
  WRITE_16_WORD(vars.OAM_attr);
}

//////////////////////////////////////////////////////////////


void DSP4::OP07()
{
  protocol.waiting4command = false;

  // op flow control
  switch (vars.Logic)
  {
    case 1:
      goto resume1; break;
    case 2:
      goto resume2; break;
  }

  ////////////////////////////////////////////////////
  // sort inputs

  vars.world_y = READ_DWORD();
  vars.poly_bottom[0][0] = READ_WORD();
  vars.poly_top[0][0] = READ_WORD();
  vars.poly_cx[1][0] = READ_WORD();
  vars.viewport_bottom = READ_WORD();
  vars.world_x = READ_DWORD();
  vars.poly_cx[0][0] = READ_WORD();
  vars.poly_ptr[0][0] = READ_WORD();
  vars.world_yofs = READ_WORD();
  vars.distance = READ_WORD();
  vars.view_y2 = READ_WORD();
  vars.view_dy = READ_WORD() * vars.distance >> 15;
  vars.view_x2 = READ_WORD();
  vars.view_dx = READ_WORD() * vars.distance >> 15;
  vars.view_yofsenv = READ_WORD();

  // initial (x,y,offset) at starting vars.raster line
  vars.view_x1 = (int16)(vars.world_x >> 16);
  vars.view_y1 = (int16)(vars.world_y >> 16);
  vars.view_xofs1 = vars.view_x1;
  vars.view_yofs1 = vars.world_yofs;

  // first vars.raster line
  vars.poly_raster[0][0] = vars.poly_bottom[0][0];


  do
  {
    ////////////////////////////////////////////////////
    // process one iteration of projection

    // add shaping
    vars.view_x2 += vars.view_dx;
    vars.view_y2 += vars.view_dy;

    // vertical scroll calculation
    vars.view_xofs2 = vars.view_x2;
    vars.view_yofs2 = (vars.world_yofs * vars.distance >> 15) + vars.poly_bottom[0][0] - vars.view_y2;

    // 1. Viewer x-position at the next
    // 2. Viewer y-position below the horizon
    // 3. Number of vars.raster lines drawn in this iteration

    CLEAR_OUT();
    WRITE_WORD(vars.view_x2);
    WRITE_WORD(vars.view_y2);

    //////////////////////////////////////////////////////

    // SR = 0x00

    // determine # of vars.raster lines used
    vars.segments = vars.view_y1 - vars.view_y2;

    // prevent overdraw
    if (vars.view_y2 >= vars.poly_raster[0][0])
      vars.segments = 0;
    else
      vars.poly_raster[0][0] = vars.view_y2;

    // don't draw outside the window
    if (vars.view_y2 < vars.poly_top[0][0])
    {
      vars.segments = 0;

      // flush remaining vars.raster lines
      if (vars.view_y1 >= vars.poly_top[0][0])
        vars.segments = vars.view_y1 - vars.poly_top[0][0];
    }

    // SR = 0x80

    WRITE_WORD(vars.segments);

    //////////////////////////////////////////////////////

    // scan next command if no SR check needed
    if (vars.segments)
    {
      int32 px_dx, py_dy;
      int32 x_scroll, y_scroll;

      // SR = 0x00

      // linear interpolation (lerp) between projected points
      px_dx = (vars.view_xofs2 - vars.view_xofs1) * Inverse(vars.segments) << 1;
      py_dy = (vars.view_yofs2 - vars.view_yofs1) * Inverse(vars.segments) << 1;

      // starting step values
      x_scroll = SEX16(vars.poly_cx[0][0] + vars.view_xofs1);
      y_scroll = SEX16(-vars.viewport_bottom + vars.view_yofs1 + vars.view_yofsenv + vars.poly_cx[1][0] - vars.world_yofs);

      // SR = 0x80

      // rasterize line
      for (vars.lcv = 0; vars.lcv < vars.segments; vars.lcv++)
      {
        // 1. HDMA memory pointer (bg2)
        // 2. vertical scroll offset ($2110)
        // 3. horizontal scroll offset ($210F)

        WRITE_WORD(vars.poly_ptr[0][0]);
        WRITE_WORD((uint16)((y_scroll + 0x8000) >> 16));
        WRITE_WORD((uint16)((x_scroll + 0x8000) >> 16));

        // update memory address
        vars.poly_ptr[0][0] -= 4;

        // update screen values
        x_scroll += px_dx;
        y_scroll += py_dy;
      }
    }

    /////////////////////////////////////////////////////
    // Post-update

    // update new viewer (x,y,scroll) to last vars.raster line drawn
    vars.view_x1 = vars.view_x2;
    vars.view_y1 = vars.view_y2;
    vars.view_xofs1 = vars.view_xofs2;
    vars.view_yofs1 = vars.view_yofs2;

    ////////////////////////////////////////////////////
    // command check

    // scan next command
    protocol.in_count = 2;
    WAIT(1) resume1 :

    // check for opcode termination
    vars.distance = READ_WORD();
    if (vars.distance == -0x8000)
      break;

    // already have 2 bytes in queue
    protocol.in_count = 10;
    WAIT(2) resume2 :

    // inspect inputs
    vars.view_y2 = READ_WORD();
    vars.view_dy = READ_WORD() * vars.distance >> 15;
    vars.view_x2 = READ_WORD();
    vars.view_dx = READ_WORD() * vars.distance >> 15;
    vars.view_yofsenv = READ_WORD();
  }
  while (1);

  protocol.waiting4command = true;
}

//////////////////////////////////////////////////////////////

void DSP4::OP08()
{
  int16 win_left, win_right;
  int16 view_x[2], view_y[2];
  int16 envelope[2][2];

  protocol.waiting4command = false;

  // op flow control
  switch (vars.Logic)
  {
    case 1:
      goto resume1; break;
    case 2:
      goto resume2; break;
  }

  ////////////////////////////////////////////////////
  // process initial inputs for two polygons

  // clip values
  vars.poly_clipRt[0][0] = READ_WORD();
  vars.poly_clipRt[0][1] = READ_WORD();
  vars.poly_clipRt[1][0] = READ_WORD();
  vars.poly_clipRt[1][1] = READ_WORD();

  vars.poly_clipLf[0][0] = READ_WORD();
  vars.poly_clipLf[0][1] = READ_WORD();
  vars.poly_clipLf[1][0] = READ_WORD();
  vars.poly_clipLf[1][1] = READ_WORD();

  // unknown (constant) (ex. 1P/2P = $00A6, $00A6, $00A6, $00A6)
  READ_WORD();
  READ_WORD();
  READ_WORD();
  READ_WORD();

  // unknown (constant) (ex. 1P/2P = $00A5, $00A5, $00A7, $00A7)
  READ_WORD();
  READ_WORD();
  READ_WORD();
  READ_WORD();

  // polygon centering (left,right)
  vars.poly_cx[0][0] = READ_WORD();
  vars.poly_cx[0][1] = READ_WORD();
  vars.poly_cx[1][0] = READ_WORD();
  vars.poly_cx[1][1] = READ_WORD();

  // HDMA pointer locations
  vars.poly_ptr[0][0] = READ_WORD();
  vars.poly_ptr[0][1] = READ_WORD();
  vars.poly_ptr[1][0] = READ_WORD();
  vars.poly_ptr[1][1] = READ_WORD();

  // starting vars.raster line below the horizon
  vars.poly_bottom[0][0] = READ_WORD();
  vars.poly_bottom[0][1] = READ_WORD();
  vars.poly_bottom[1][0] = READ_WORD();
  vars.poly_bottom[1][1] = READ_WORD();

  // top boundary line to clip
  vars.poly_top[0][0] = READ_WORD();
  vars.poly_top[0][1] = READ_WORD();
  vars.poly_top[1][0] = READ_WORD();
  vars.poly_top[1][1] = READ_WORD();

  // unknown
  // (ex. 1P = $2FC8, $0034, $FF5C, $0035)
  //
  // (ex. 2P = $3178, $0034, $FFCC, $0035)
  // (ex. 2P = $2FC8, $0034, $FFCC, $0035)

  READ_WORD();
  READ_WORD();
  READ_WORD();
  READ_WORD();

  // look at guidelines for both polygon shapes
  vars.distance = READ_WORD();
  view_x[0] = READ_WORD();
  view_y[0] = READ_WORD();
  view_x[1] = READ_WORD();
  view_y[1] = READ_WORD();

  // envelope shaping guidelines (one frame only)
  envelope[0][0] = READ_WORD();
  envelope[0][1] = READ_WORD();
  envelope[1][0] = READ_WORD();
  envelope[1][1] = READ_WORD();

  // starting base values to project from
  vars.poly_start[0] = view_x[0];
  vars.poly_start[1] = view_x[1];

  // starting vars.raster lines to begin drawing
  vars.poly_raster[0][0] = view_y[0];
  vars.poly_raster[0][1] = view_y[0];
  vars.poly_raster[1][0] = view_y[1];
  vars.poly_raster[1][1] = view_y[1];

  // starting distances
  vars.poly_plane[0] = vars.distance;
  vars.poly_plane[1] = vars.distance;

  // SR = 0x00

  // re-center coordinates
  win_left = vars.poly_cx[0][0] - view_x[0] + envelope[0][0];
  win_right = vars.poly_cx[0][1] - view_x[0] + envelope[0][1];

  // saturate offscreen data for polygon #1
  if (win_left < vars.poly_clipLf[0][0])
  {
    win_left = vars.poly_clipLf[0][0];
  }
  if (win_left > vars.poly_clipRt[0][0])
  {
    win_left = vars.poly_clipRt[0][0];
  }
  if (win_right < vars.poly_clipLf[0][1])
  {
    win_right = vars.poly_clipLf[0][1];
  }
  if (win_right > vars.poly_clipRt[0][1])
  {
    win_right = vars.poly_clipRt[0][1];
  }

  // SR = 0x80

  // initial output for polygon #1
  CLEAR_OUT();
  WRITE_BYTE(win_left & 0xff);
  WRITE_BYTE(win_right & 0xff);


  do
  {
    int16_t polygon;
    ////////////////////////////////////////////////////
    // command check

    // scan next command
    protocol.in_count = 2;
    WAIT(1) resume1 :

    // terminate op
    vars.distance = READ_WORD();
    if (vars.distance == -0x8000)
      break;

    // already have 2 bytes in queue
    protocol.in_count = 16;

    WAIT(2) resume2 :

    // look at guidelines for both polygon shapes
    view_x[0] = READ_WORD();
    view_y[0] = READ_WORD();
    view_x[1] = READ_WORD();
    view_y[1] = READ_WORD();

    // envelope shaping guidelines (one frame only)
    envelope[0][0] = READ_WORD();
    envelope[0][1] = READ_WORD();
    envelope[1][0] = READ_WORD();
    envelope[1][1] = READ_WORD();

    ////////////////////////////////////////////////////
    // projection begins

    // init
    CLEAR_OUT();


    //////////////////////////////////////////////
    // solid polygon renderer - 2 shapes

    for (polygon = 0; polygon < 2; polygon++)
    {
      int32 left_inc, right_inc;
      int16 x1_final, x2_final;
      int16 env[2][2];
      int16 poly;

      // SR = 0x00

      // # vars.raster lines to draw
      vars.segments = vars.poly_raster[polygon][0] - view_y[polygon];

      // prevent overdraw
      if (vars.segments > 0)
      {
        // bump drawing cursor
        vars.poly_raster[polygon][0] = view_y[polygon];
        vars.poly_raster[polygon][1] = view_y[polygon];
      }
      else
        vars.segments = 0;

      // don't draw outside the window
      if (view_y[polygon] < vars.poly_top[polygon][0])
      {
        vars.segments = 0;

        // flush remaining vars.raster lines
        if (view_y[polygon] >= vars.poly_top[polygon][0])
          vars.segments = view_y[polygon] - vars.poly_top[polygon][0];
      }

      // SR = 0x80

      // tell user how many vars.raster structures to read in
      WRITE_WORD(vars.segments);

      // normal parameters
      poly = polygon;

      /////////////////////////////////////////////////////

      // scan next command if no SR check needed
      if (vars.segments)
      {
        int32 win_left, win_right;

        // road turnoff selection
        if( (uint16) envelope[ polygon ][ 0 ] == (uint16) 0xc001 )
          poly = 1;
        else if( envelope[ polygon ][ 1 ] == 0x3fff )
          poly = 1;

        ///////////////////////////////////////////////
        // left side of polygon

        // perspective correction on additional shaping parameters
        env[0][0] = envelope[polygon][0] * vars.poly_plane[poly] >> 15;
        env[0][1] = envelope[polygon][0] * vars.distance >> 15;

        // project new shapes (left side)
        x1_final = view_x[poly] + env[0][0];
        x2_final = vars.poly_start[poly] + env[0][1];

        // interpolate between projected points with shaping
        left_inc = (x2_final - x1_final) * Inverse(vars.segments) << 1;
        if (vars.segments == 1)
          left_inc = -left_inc;

        ///////////////////////////////////////////////
        // right side of polygon

        // perspective correction on additional shaping parameters
        env[1][0] = envelope[polygon][1] * vars.poly_plane[poly] >> 15;;
        env[1][1] = envelope[polygon][1] * vars.distance >> 15;

        // project new shapes (right side)
        x1_final = view_x[poly] + env[1][0];
        x2_final = vars.poly_start[poly] + env[1][1];


        // interpolate between projected points with shaping
        right_inc = (x2_final - x1_final) * Inverse(vars.segments) << 1;
        if (vars.segments == 1)
          right_inc = -right_inc;

        ///////////////////////////////////////////////
        // update each point on the line

        win_left = SEX16(vars.poly_cx[polygon][0] - vars.poly_start[poly] + env[0][0]);
        win_right = SEX16(vars.poly_cx[polygon][1] - vars.poly_start[poly] + env[1][0]);

        // update vars.distance drawn into world
        vars.poly_plane[polygon] = vars.distance;

        // rasterize line
        for (vars.lcv = 0; vars.lcv < vars.segments; vars.lcv++)
        {
          int16 x_left, x_right;

          // project new coordinates
          win_left += left_inc;
          win_right += right_inc;

          // grab integer portion, drop fraction (no rounding)
          x_left = (int16)(win_left >> 16);
          x_right = (int16)(win_right >> 16);

          // saturate offscreen data
          if (x_left < vars.poly_clipLf[polygon][0])
            x_left = vars.poly_clipLf[polygon][0];
          if (x_left > vars.poly_clipRt[polygon][0])
            x_left = vars.poly_clipRt[polygon][0];
          if (x_right < vars.poly_clipLf[polygon][1])
            x_right = vars.poly_clipLf[polygon][1];
          if (x_right > vars.poly_clipRt[polygon][1])
            x_right = vars.poly_clipRt[polygon][1];

          // 1. HDMA memory pointer
          // 2. Left window position ($2126/$2128)
          // 3. Right window position ($2127/$2129)

          WRITE_WORD(vars.poly_ptr[polygon][0]);
          WRITE_BYTE(x_left & 0xff);
          WRITE_BYTE(x_right & 0xff);


          // update memory pointers
          vars.poly_ptr[polygon][0] -= 4;
          vars.poly_ptr[polygon][1] -= 4;
        } // end rasterize line
      }

      ////////////////////////////////////////////////
      // Post-update

      // new projection spot to continue rasterizing from
      vars.poly_start[polygon] = view_x[poly];
    } // end polygon rasterizer
  }
  while (1);

  // unknown output
  CLEAR_OUT();
  WRITE_WORD(0);


  protocol.waiting4command = true;
}

//////////////////////////////////////////////////////////////

void DSP4::OP09()
{
  protocol.waiting4command = false;

  // op flow control
  switch (vars.Logic)
  {
    case 1:
      goto resume1; break;
    case 2:
      goto resume2; break;
    case 3:
      goto resume3; break;
    case 4:
      goto resume4; break;
    case 5:
      goto resume5; break;
    case 6:
      goto resume6; break;
  }

  ////////////////////////////////////////////////////
  // process initial inputs

  // grab screen information
  vars.viewport_cx = READ_WORD();
  vars.viewport_cy = READ_WORD();
  READ_WORD(); // 0x0000
  vars.viewport_left = READ_WORD();
  vars.viewport_right = READ_WORD();
  vars.viewport_top = READ_WORD();
  vars.viewport_bottom = READ_WORD();

  // starting vars.raster line below the horizon
  vars.poly_bottom[0][0] = vars.viewport_bottom - vars.viewport_cy;
  vars.poly_raster[0][0] = 0x100;

  do
  {
    ////////////////////////////////////////////////////
    // check for new sprites

    protocol.in_count = 4;
    WAIT(1) resume1 :

    ////////////////////////////////////////////////
    // vars.raster overdraw check

    vars.raster = READ_WORD();

    // continue updating the vars.raster line where overdraw begins
    if (vars.raster < vars.poly_raster[0][0])
    {
      vars.sprite_clipy = vars.viewport_bottom - (vars.poly_bottom[0][0] - vars.raster);
      vars.poly_raster[0][0] = vars.raster;
    }

    /////////////////////////////////////////////////
    // identify sprite

    // op termination
    vars.distance = READ_WORD();
    if (vars.distance == -0x8000)
      goto terminate;


    // no sprite
    if (vars.distance == 0x0000)
    {
      continue;
    }

    ////////////////////////////////////////////////////
    // process projection information

    // vehicle sprite
    if ((uint16) vars.distance == 0x9000)
    {
      int16_t car_left, car_right, car_back;
      int16_t impact_left, impact_back;
      int16_t world_spx, world_spy;
      int16_t view_spx, view_spy;
      uint16_t energy;

      // we already have 4 bytes we want
      protocol.in_count = 14;
      WAIT(2) resume2 :

      // filter inputs
      energy = READ_WORD();
      impact_back = READ_WORD();
      car_back = READ_WORD();
      impact_left = READ_WORD();
      car_left = READ_WORD();
      vars.distance = READ_WORD();
      car_right = READ_WORD();

      // calculate car's world (x,y) values
      world_spx = car_right - car_left;
      world_spy = car_back;

      // add in collision vector [needs bit-twiddling]
      world_spx -= energy * (impact_left - car_left) >> 16;
      world_spy -= energy * (car_back - impact_back) >> 16;

      // perspective correction for world (x,y)
      view_spx = world_spx * vars.distance >> 15;
      view_spy = world_spy * vars.distance >> 15;

      // convert to screen values
      vars.sprite_x = vars.viewport_cx + view_spx;
      vars.sprite_y = vars.viewport_bottom - (vars.poly_bottom[0][0] - view_spy);

      // make the car's (x)-coordinate available
      CLEAR_OUT();
      WRITE_WORD(world_spx);

      // grab a few remaining vehicle values
      protocol.in_count = 4;
      WAIT(3) resume3 :

      // add vertical lift factor
      vars.sprite_y += READ_WORD();
    }
    // terrain sprite
    else
    {
      int16_t world_spx, world_spy;
      int16_t view_spx, view_spy;

      // we already have 4 bytes we want
      protocol.in_count = 10;
      WAIT(4) resume4 :

      // sort loop inputs
      vars.poly_cx[0][0] = READ_WORD();
      vars.poly_raster[0][1] = READ_WORD();
      world_spx = READ_WORD();
      world_spy = READ_WORD();

      // compute base vars.raster line from the bottom
      vars.segments = vars.poly_bottom[0][0] - vars.raster;

      // perspective correction for world (x,y)
      view_spx = world_spx * vars.distance >> 15;
      view_spy = world_spy * vars.distance >> 15;

      // convert to screen values
      vars.sprite_x = vars.viewport_cx + view_spx - vars.poly_cx[0][0];
      vars.sprite_y = vars.viewport_bottom - vars.segments + view_spy;
    }

    // default sprite size: 16x16
    vars.sprite_size = 1;
    vars.sprite_attr = READ_WORD();

    ////////////////////////////////////////////////////
    // convert tile data to SNES OAM format

    do
    {
      uint16_t header;

      int16_t sp_x, sp_y, sp_attr, sp_dattr;
      int16_t sp_dx, sp_dy;
      int16_t pixels;

      bool draw;

      protocol.in_count = 2;
      WAIT(5) resume5 :

      draw = true;

      // opcode termination
      vars.raster = READ_WORD();
      if (vars.raster == -0x8000)
        goto terminate;

      // stop code
      if (vars.raster == 0x0000 && !vars.sprite_size)
        break;

      // toggle sprite size
      if (vars.raster == 0x0000)
      {
        vars.sprite_size = !vars.sprite_size;
        continue;
      }

      // check for valid sprite header
      header = vars.raster;
      header >>= 8;
      if (header != 0x20 &&
          header != 0x2e && //This is for attractor sprite
          header != 0x40 &&
          header != 0x60 &&
          header != 0xa0 &&
          header != 0xc0 &&
          header != 0xe0)
        break;

      // read in rest of sprite data
      protocol.in_count = 4;
      WAIT(6) resume6 :

      draw = true;

      /////////////////////////////////////
      // process tile data

      // sprite deltas
      sp_dattr = vars.raster;
      sp_dy = READ_WORD();
      sp_dx = READ_WORD();

      // update coordinates to screen space
      sp_x = vars.sprite_x + sp_dx;
      sp_y = vars.sprite_y + sp_dy;

      // update sprite nametable/attribute information
      sp_attr = vars.sprite_attr + sp_dattr;

      // allow partially visibile tiles
      pixels = vars.sprite_size ? 15 : 7;

      CLEAR_OUT();

      // transparent tile to clip off parts of a sprite (overdraw)
      if (vars.sprite_clipy - pixels <= sp_y &&
          sp_y <= vars.sprite_clipy &&
          sp_x >= vars.viewport_left - pixels &&
          sp_x <= vars.viewport_right &&
          vars.sprite_clipy >= vars.viewport_top - pixels &&
          vars.sprite_clipy <= vars.viewport_bottom)
      {
        OP0B(&draw, sp_x, vars.sprite_clipy, 0x00EE, vars.sprite_size, 0);
      }


      // normal sprite tile
      if (sp_x >= vars.viewport_left - pixels &&
          sp_x <= vars.viewport_right &&
          sp_y >= vars.viewport_top - pixels &&
          sp_y <= vars.viewport_bottom &&
          sp_y <= vars.sprite_clipy)
      {
        OP0B(&draw, sp_x, sp_y, sp_attr, vars.sprite_size, 0);
      }


      // no following OAM data
      OP0B(&draw, 0, 0x0100, 0, 0, 1);
    }
    while (1);
  }
  while (1);

  terminate : protocol.waiting4command = true;
}

//////////////////////////////////////////////////////////////

const uint16 DSP4::OP0A_Values[16] = {
  0x0000, 0x0030, 0x0060, 0x0090, 0x00c0, 0x00f0, 0x0120, 0x0150,
  0xfe80, 0xfeb0, 0xfee0, 0xff10, 0xff40, 0xff70, 0xffa0, 0xffd0,
};

void DSP4::OP0A(int16 n2, int16 *o1, int16 *o2, int16 *o3, int16 *o4)
{
  *o4 = OP0A_Values[n2 >>  0 & 15];
  *o3 = OP0A_Values[n2 >>  4 & 15];
  *o2 = OP0A_Values[n2 >>  8 & 15];
  *o1 = OP0A_Values[n2 >> 12 & 15];
}

//////////////////////////////////////////////////////////////

void DSP4::OP0B(bool *draw, int16 sp_x, int16 sp_y, int16 sp_attr, bool size, bool stop)
{
  int16 Row1, Row2;

  // SR = 0x00

  // align to nearest 8-pixel row
  Row1 = (sp_y >> 3) & 0x1f;
  Row2 = (Row1 + 1) & 0x1f;

  // check boundaries
  if (!((sp_y < 0) || ((sp_y & 0x01ff) < 0x00eb)))
  {
    *draw = 0;
  }
  if (size)
  {
    if (vars.OAM_Row[Row1] + 1 >= vars.OAM_RowMax)
      *draw = 0;
    if (vars.OAM_Row[Row2] + 1 >= vars.OAM_RowMax)
      *draw = 0;
  }
  else
  {
    if (vars.OAM_Row[Row1] >= vars.OAM_RowMax)
    {
      *draw = 0;
    }
  }

  // emulator fail-safe (unknown if this really exists)
  if (vars.sprite_count >= 128)
  {
    *draw = 0;
  }

  // SR = 0x80

  if (*draw)
  {
    // Row tiles
    if (size)
    {
      vars.OAM_Row[Row1] += 2;
      vars.OAM_Row[Row2] += 2;
    }
    else
    {
      vars.OAM_Row[Row1]++;
    }

    // yield OAM output
    WRITE_WORD(1);

    // pack OAM data: x,y,name,attr
    WRITE_BYTE(sp_x & 0xff);
    WRITE_BYTE(sp_y & 0xff);
    WRITE_WORD(sp_attr);

    vars.sprite_count++;

    // OAM: size,msb data
    // save post-oam table data for future retrieval
    vars.OAM_attr[vars.OAM_index] |= ((sp_x <0 || sp_x> 255) << vars.OAM_bits);
    vars.OAM_bits++;

    vars.OAM_attr[vars.OAM_index] |= (size << vars.OAM_bits);
    vars.OAM_bits++;

    // move to next byte in buffer
    if (vars.OAM_bits == 16)
    {
      vars.OAM_bits = 0;
      vars.OAM_index++;
    }
  }
  else if (stop)
  {
    // yield no OAM output
    WRITE_WORD(0);
  }
}

//////////////////////////////////////////////////////////////

void DSP4::OP0D()
{
  protocol.waiting4command = false;

  // op flow control
  switch (vars.Logic)
  {
    case 1:
      goto resume1; break;
    case 2:
      goto resume2; break;
  }

  ////////////////////////////////////////////////////
  // process initial inputs

  // sort inputs
  vars.world_y = READ_DWORD();
  vars.poly_bottom[0][0] = READ_WORD();
  vars.poly_top[0][0] = READ_WORD();
  vars.poly_cx[1][0] = READ_WORD();
  vars.viewport_bottom = READ_WORD();
  vars.world_x = READ_DWORD();
  vars.poly_cx[0][0] = READ_WORD();
  vars.poly_ptr[0][0] = READ_WORD();
  vars.world_yofs = READ_WORD();
  vars.world_dy = READ_DWORD();
  vars.world_dx = READ_DWORD();
  vars.distance = READ_WORD();
  READ_WORD(); // 0x0000
  vars.world_xenv = SEX78(READ_WORD());
  vars.world_ddy = READ_WORD();
  vars.world_ddx = READ_WORD();
  vars.view_yofsenv = READ_WORD();

  // initial (x,y,offset) at starting vars.raster line
  vars.view_x1 = (int16)((vars.world_x + vars.world_xenv) >> 16);
  vars.view_y1 = (int16)(vars.world_y >> 16);
  vars.view_xofs1 = (int16)(vars.world_x >> 16);
  vars.view_yofs1 = vars.world_yofs;

  // first vars.raster line
  vars.poly_raster[0][0] = vars.poly_bottom[0][0];


  do
  {
    ////////////////////////////////////////////////////
    // process one iteration of projection

    // perspective projection of world (x,y,scroll) points
    // based on the current projection lines
    vars.view_x2 = (int16)(( ( ( vars.world_x + vars.world_xenv ) >> 16 ) * vars.distance >> 15 ) + ( vars.view_turnoff_x * vars.distance >> 15 ));
    vars.view_y2 = (int16)((vars.world_y >> 16) * vars.distance >> 15);
    vars.view_xofs2 = vars.view_x2;
    vars.view_yofs2 = (vars.world_yofs * vars.distance >> 15) + vars.poly_bottom[0][0] - vars.view_y2;

    // 1. World x-location before transformation
    // 2. Viewer x-position at the current
    // 3. World y-location before perspective projection
    // 4. Viewer y-position below the horizon
    // 5. Number of vars.raster lines drawn in this iteration

    CLEAR_OUT();
    WRITE_WORD((uint16)((vars.world_x + vars.world_xenv) >> 16));
    WRITE_WORD(vars.view_x2);
    WRITE_WORD((uint16)(vars.world_y >> 16));
    WRITE_WORD(vars.view_y2);

    //////////////////////////////////////////////////////////

    // SR = 0x00

    // determine # of vars.raster lines used
    vars.segments = vars.view_y1 - vars.view_y2;

    // prevent overdraw
    if (vars.view_y2 >= vars.poly_raster[0][0])
      vars.segments = 0;
    else
      vars.poly_raster[0][0] = vars.view_y2;

    // don't draw outside the window
    if (vars.view_y2 < vars.poly_top[0][0])
    {
      vars.segments = 0;

      // flush remaining vars.raster lines
      if (vars.view_y1 >= vars.poly_top[0][0])
        vars.segments = vars.view_y1 - vars.poly_top[0][0];
    }

    // SR = 0x80

    WRITE_WORD(vars.segments);

    //////////////////////////////////////////////////////////

    // scan next command if no SR check needed
    if (vars.segments)
    {
      int32 px_dx, py_dy;
      int32 x_scroll, y_scroll;

      // SR = 0x00

      // linear interpolation (lerp) between projected points
      px_dx = (vars.view_xofs2 - vars.view_xofs1) * Inverse(vars.segments) << 1;
      py_dy = (vars.view_yofs2 - vars.view_yofs1) * Inverse(vars.segments) << 1;

      // starting step values
      x_scroll = SEX16(vars.poly_cx[0][0] + vars.view_xofs1);
      y_scroll = SEX16(-vars.viewport_bottom + vars.view_yofs1 + vars.view_yofsenv + vars.poly_cx[1][0] - vars.world_yofs);

      // SR = 0x80

      // rasterize line
      for (vars.lcv = 0; vars.lcv < vars.segments; vars.lcv++)
      {
        // 1. HDMA memory pointer (bg1)
        // 2. vertical scroll offset ($210E)
        // 3. horizontal scroll offset ($210D)

        WRITE_WORD(vars.poly_ptr[0][0]);
        WRITE_WORD((uint16)((y_scroll + 0x8000) >> 16));
        WRITE_WORD((uint16)((x_scroll + 0x8000) >> 16));


        // update memory address
        vars.poly_ptr[0][0] -= 4;

        // update screen values
        x_scroll += px_dx;
        y_scroll += py_dy;
      }
    }

    /////////////////////////////////////////////////////
    // Post-update

    // update new viewer (x,y,scroll) to last vars.raster line drawn
    vars.view_x1 = vars.view_x2;
    vars.view_y1 = vars.view_y2;
    vars.view_xofs1 = vars.view_xofs2;
    vars.view_yofs1 = vars.view_yofs2;

    // add deltas for projection lines
    vars.world_dx += SEX78(vars.world_ddx);
    vars.world_dy += SEX78(vars.world_ddy);

    // update projection lines
    vars.world_x += (vars.world_dx + vars.world_xenv);
    vars.world_y += vars.world_dy;

    ////////////////////////////////////////////////////
    // command check

    // scan next command
    protocol.in_count = 2;
    WAIT(1) resume1 :

    // inspect input
    vars.distance = READ_WORD();

    // terminate op
    if (vars.distance == -0x8000)
      break;

    // already have 2 bytes in queue
    protocol.in_count = 6;
    WAIT(2) resume2:

    // inspect inputs
    vars.world_ddy = READ_WORD();
    vars.world_ddx = READ_WORD();
    vars.view_yofsenv = READ_WORD();

    // no envelope here
    vars.world_xenv = 0;
  }
  while (1);

  protocol.waiting4command = true;
}

//////////////////////////////////////////////////////////////


void DSP4::OP0E()
{
  vars.OAM_RowMax = 16;
  memset(vars.OAM_Row, 0, 64);
}


//////////////////////////////////////////////////////////////

void DSP4::OP0F()
{
  protocol.waiting4command = false;

  // op flow control
  switch (vars.Logic)
  {
    case 1:
      goto resume1; break;
    case 2:
      goto resume2; break;
    case 3:
      goto resume3; break;
    case 4:
      goto resume4; break;
  }

  ////////////////////////////////////////////////////
  // process initial inputs

  // sort inputs
  READ_WORD(); // 0x0000
  vars.world_y = READ_DWORD();
  vars.poly_bottom[0][0] = READ_WORD();
  vars.poly_top[0][0] = READ_WORD();
  vars.poly_cx[1][0] = READ_WORD();
  vars.viewport_bottom = READ_WORD();
  vars.world_x = READ_DWORD();
  vars.poly_cx[0][0] = READ_WORD();
  vars.poly_ptr[0][0] = READ_WORD();
  vars.world_yofs = READ_WORD();
  vars.world_dy = READ_DWORD();
  vars.world_dx = READ_DWORD();
  vars.distance = READ_WORD();
  READ_WORD(); // 0x0000
  vars.world_xenv = READ_DWORD();
  vars.world_ddy = READ_WORD();
  vars.world_ddx = READ_WORD();
  vars.view_yofsenv = READ_WORD();

  // initial (x,y,offset) at starting vars.raster line
  vars.view_x1 = (int16)((vars.world_x + vars.world_xenv) >> 16);
  vars.view_y1 = (int16)(vars.world_y >> 16);
  vars.view_xofs1 = (int16)(vars.world_x >> 16);
  vars.view_yofs1 = vars.world_yofs;
  vars.view_turnoff_x = 0;
  vars.view_turnoff_dx = 0;

  // first vars.raster line
  vars.poly_raster[0][0] = vars.poly_bottom[0][0];


  do
  {
    ////////////////////////////////////////////////////
    // process one iteration of projection

    // perspective projection of world (x,y,scroll) points
    // based on the current projection lines
    vars.view_x2 = (int16)(((vars.world_x + vars.world_xenv) >> 16) * vars.distance >> 15);
    vars.view_y2 = (int16)((vars.world_y >> 16) * vars.distance >> 15);
    vars.view_xofs2 = vars.view_x2;
    vars.view_yofs2 = (vars.world_yofs * vars.distance >> 15) + vars.poly_bottom[0][0] - vars.view_y2;

    // 1. World x-location before transformation
    // 2. Viewer x-position at the next
    // 3. World y-location before perspective projection
    // 4. Viewer y-position below the horizon
    // 5. Number of vars.raster lines drawn in this iteration

    CLEAR_OUT();
    WRITE_WORD((uint16)((vars.world_x + vars.world_xenv) >> 16));
    WRITE_WORD(vars.view_x2);
    WRITE_WORD((uint16)(vars.world_y >> 16));
    WRITE_WORD(vars.view_y2);

    //////////////////////////////////////////////////////

    // SR = 0x00

    // determine # of vars.raster lines used
    vars.segments = vars.poly_raster[0][0] - vars.view_y2;

    // prevent overdraw
    if (vars.view_y2 >= vars.poly_raster[0][0])
      vars.segments = 0;
    else
      vars.poly_raster[0][0] = vars.view_y2;

    // don't draw outside the window
    if (vars.view_y2 < vars.poly_top[0][0])
    {
      vars.segments = 0;

      // flush remaining vars.raster lines
      if (vars.view_y1 >= vars.poly_top[0][0])
        vars.segments = vars.view_y1 - vars.poly_top[0][0];
    }

    // SR = 0x80

    WRITE_WORD(vars.segments);

    //////////////////////////////////////////////////////

    // scan next command if no SR check needed
    if (vars.segments)
    {
      int32_t px_dx, py_dy;
      int32_t x_scroll, y_scroll;

      for (vars.lcv = 0; vars.lcv < 4; vars.lcv++)
      {
        // grab inputs
        protocol.in_count = 4;
        WAIT(1);
        resume1 :
        for (;;)
        {
          int16 distance;
          int16 color, red, green, blue;

          distance = READ_WORD();
          color = READ_WORD();

          // U1+B5+G5+R5
          red = color & 0x1f;
          green = (color >> 5) & 0x1f;
          blue = (color >> 10) & 0x1f;

          // dynamic lighting
          red = (red * distance >> 15) & 0x1f;
          green = (green * distance >> 15) & 0x1f;
          blue = (blue * distance >> 15) & 0x1f;
          color = red | (green << 5) | (blue << 10);

          CLEAR_OUT();
          WRITE_WORD(color);
          break;
        }
      }

      //////////////////////////////////////////////////////

      // SR = 0x00

      // linear interpolation (lerp) between projected points
      px_dx = (vars.view_xofs2 - vars.view_xofs1) * Inverse(vars.segments) << 1;
      py_dy = (vars.view_yofs2 - vars.view_yofs1) * Inverse(vars.segments) << 1;


      // starting step values
      x_scroll = SEX16(vars.poly_cx[0][0] + vars.view_xofs1);
      y_scroll = SEX16(-vars.viewport_bottom + vars.view_yofs1 + vars.view_yofsenv + vars.poly_cx[1][0] - vars.world_yofs);

      // SR = 0x80

      // rasterize line
      for (vars.lcv = 0; vars.lcv < vars.segments; vars.lcv++)
      {
        // 1. HDMA memory pointer
        // 2. vertical scroll offset ($210E)
        // 3. horizontal scroll offset ($210D)

        WRITE_WORD(vars.poly_ptr[0][0]);
        WRITE_WORD((uint16)((y_scroll + 0x8000) >> 16));
        WRITE_WORD((uint16)((x_scroll + 0x8000) >> 16));

        // update memory address
        vars.poly_ptr[0][0] -= 4;

        // update screen values
        x_scroll += px_dx;
        y_scroll += py_dy;
      }
    }

    ////////////////////////////////////////////////////
    // Post-update

    // update new viewer (x,y,scroll) to last vars.raster line drawn
    vars.view_x1 = vars.view_x2;
    vars.view_y1 = vars.view_y2;
    vars.view_xofs1 = vars.view_xofs2;
    vars.view_yofs1 = vars.view_yofs2;

    // add deltas for projection lines
    vars.world_dx += SEX78(vars.world_ddx);
    vars.world_dy += SEX78(vars.world_ddy);

    // update projection lines
    vars.world_x += (vars.world_dx + vars.world_xenv);
    vars.world_y += vars.world_dy;

    // update road turnoff position
    vars.view_turnoff_x += vars.view_turnoff_dx;

    ////////////////////////////////////////////////////
    // command check

    // scan next command
    protocol.in_count = 2;
    WAIT(2) resume2:

    // check for termination
    vars.distance = READ_WORD();
    if (vars.distance == -0x8000)
      break;

    // road splice
    if( (uint16) vars.distance == 0x8001 )
    {
      protocol.in_count = 6;
      WAIT(3) resume3:

      vars.distance = READ_WORD();
      vars.view_turnoff_x = READ_WORD();
      vars.view_turnoff_dx = READ_WORD();

      // factor in new changes
      vars.view_x1 += ( vars.view_turnoff_x * vars.distance >> 15 );
      vars.view_xofs1 += ( vars.view_turnoff_x * vars.distance >> 15 );

      // update stepping values
      vars.view_turnoff_x += vars.view_turnoff_dx;

      protocol.in_count = 2;
      WAIT(2)
    }

    // already have 2 bytes in queue
    protocol.in_count = 6;
    WAIT(4) resume4 :

    // inspect inputs
    vars.world_ddy = READ_WORD();
    vars.world_ddx = READ_WORD();
    vars.view_yofsenv = READ_WORD();

    // no envelope here
    vars.world_xenv = 0;
  }
  while (1);

  // terminate op
  protocol.waiting4command = true;
}

//////////////////////////////////////////////////////////////


void DSP4::OP10()
{
  protocol.waiting4command = false;

  // op flow control
  switch (vars.Logic)
  {
    case 1:
      goto resume1; break;
    case 2:
      goto resume2; break;
    case 3:
      goto resume3; break;
  }

  ////////////////////////////////////////////////////
  // sort inputs

  READ_WORD(); // 0x0000
  vars.world_y = READ_DWORD();
  vars.poly_bottom[0][0] = READ_WORD();
  vars.poly_top[0][0] = READ_WORD();
  vars.poly_cx[1][0] = READ_WORD();
  vars.viewport_bottom = READ_WORD();
  vars.world_x = READ_DWORD();
  vars.poly_cx[0][0] = READ_WORD();
  vars.poly_ptr[0][0] = READ_WORD();
  vars.world_yofs = READ_WORD();
  vars.distance = READ_WORD();
  vars.view_y2 = READ_WORD();
  vars.view_dy = READ_WORD() * vars.distance >> 15;
  vars.view_x2 = READ_WORD();
  vars.view_dx = READ_WORD() * vars.distance >> 15;
  vars.view_yofsenv = READ_WORD();

  // initial (x,y,offset) at starting vars.raster line
  vars.view_x1 = (int16)(vars.world_x >> 16);
  vars.view_y1 = (int16)(vars.world_y >> 16);
  vars.view_xofs1 = vars.view_x1;
  vars.view_yofs1 = vars.world_yofs;

  // first vars.raster line
  vars.poly_raster[0][0] = vars.poly_bottom[0][0];

  do
  {
    ////////////////////////////////////////////////////
    // process one iteration of projection

    // add shaping
    vars.view_x2 += vars.view_dx;
    vars.view_y2 += vars.view_dy;

    // vertical scroll calculation
    vars.view_xofs2 = vars.view_x2;
    vars.view_yofs2 = (vars.world_yofs * vars.distance >> 15) + vars.poly_bottom[0][0] - vars.view_y2;

    // 1. Viewer x-position at the next
    // 2. Viewer y-position below the horizon
    // 3. Number of vars.raster lines drawn in this iteration

    CLEAR_OUT();
    WRITE_WORD(vars.view_x2);
    WRITE_WORD(vars.view_y2);

    //////////////////////////////////////////////////////

    // SR = 0x00

    // determine # of vars.raster lines used
    vars.segments = vars.view_y1 - vars.view_y2;

    // prevent overdraw
    if (vars.view_y2 >= vars.poly_raster[0][0])
      vars.segments = 0;
    else
      vars.poly_raster[0][0] = vars.view_y2;

    // don't draw outside the window
    if (vars.view_y2 < vars.poly_top[0][0])
    {
      vars.segments = 0;

      // flush remaining vars.raster lines
      if (vars.view_y1 >= vars.poly_top[0][0])
        vars.segments = vars.view_y1 - vars.poly_top[0][0];
    }

    // SR = 0x80

    WRITE_WORD(vars.segments);

    //////////////////////////////////////////////////////

    // scan next command if no SR check needed
    if (vars.segments)
    {
      for (vars.lcv = 0; vars.lcv < 4; vars.lcv++)
      {
        // grab inputs
        protocol.in_count = 4;
        WAIT(1);
        resume1 :
        for (;;)
        {
          int16 distance;
          int16 color, red, green, blue;

          distance = READ_WORD();
          color = READ_WORD();

          // U1+B5+G5+R5
          red = color & 0x1f;
          green = (color >> 5) & 0x1f;
          blue = (color >> 10) & 0x1f;

          // dynamic lighting
          red = (red * distance >> 15) & 0x1f;
          green = (green * distance >> 15) & 0x1f;
          blue = (blue * distance >> 15) & 0x1f;
          color = red | (green << 5) | (blue << 10);

          CLEAR_OUT();
          WRITE_WORD(color);
          break;
        }
      }
    }

    //////////////////////////////////////////////////////

    // scan next command if no SR check needed
    if (vars.segments)
    {
      int32 px_dx, py_dy;
      int32 x_scroll, y_scroll;

      // SR = 0x00

      // linear interpolation (lerp) between projected points
      px_dx = (vars.view_xofs2 - vars.view_xofs1) * Inverse(vars.segments) << 1;
      py_dy = (vars.view_yofs2 - vars.view_yofs1) * Inverse(vars.segments) << 1;

      // starting step values
      x_scroll = SEX16(vars.poly_cx[0][0] + vars.view_xofs1);
      y_scroll = SEX16(-vars.viewport_bottom + vars.view_yofs1 + vars.view_yofsenv + vars.poly_cx[1][0] - vars.world_yofs);

      // SR = 0x80

      // rasterize line
      for (vars.lcv = 0; vars.lcv < vars.segments; vars.lcv++)
      {
        // 1. HDMA memory pointer (bg2)
        // 2. vertical scroll offset ($2110)
        // 3. horizontal scroll offset ($210F)

        WRITE_WORD(vars.poly_ptr[0][0]);
        WRITE_WORD((uint16)((y_scroll + 0x8000) >> 16));
        WRITE_WORD((uint16)((x_scroll + 0x8000) >> 16));

        // update memory address
        vars.poly_ptr[0][0] -= 4;

        // update screen values
        x_scroll += px_dx;
        y_scroll += py_dy;
      }
    }

    /////////////////////////////////////////////////////
    // Post-update

    // update new viewer (x,y,scroll) to last vars.raster line drawn
    vars.view_x1 = vars.view_x2;
    vars.view_y1 = vars.view_y2;
    vars.view_xofs1 = vars.view_xofs2;
    vars.view_yofs1 = vars.view_yofs2;

    ////////////////////////////////////////////////////
    // command check

    // scan next command
    protocol.in_count = 2;
    WAIT(2) resume2 :

    // check for opcode termination
    vars.distance = READ_WORD();
    if (vars.distance == -0x8000)
      break;

    // already have 2 bytes in queue
    protocol.in_count = 10;
    WAIT(3) resume3 :


    // inspect inputs
    vars.view_y2 = READ_WORD();
    vars.view_dy = READ_WORD() * vars.distance >> 15;
    vars.view_x2 = READ_WORD();
    vars.view_dx = READ_WORD() * vars.distance >> 15;
  }
  while (1);

  protocol.waiting4command = true;
}

//////////////////////////////////////////////////////////////

void DSP4::OP11(int16 A, int16 B, int16 C, int16 D, int16 *M)
{
  // 0x155 = 341 = Horizontal Width of the Screen
  *M = ((A * 0x0155 >> 2) & 0xf000) |
       ((B * 0x0155 >> 6) & 0x0f00) |
       ((C * 0x0155 >> 10) & 0x00f0) |
       ((D * 0x0155 >> 14) & 0x000f);
}

/////////////////////////////////////////////////////////////
//Processing Code
/////////////////////////////////////////////////////////////
void DSP4::reset() {
  memset(&vars, 0, sizeof(vars));
  protocol.waiting4command = true;
}

uint8 DSP4::readSR() {
  return 0x80;
}

void DSP4::writeSR(uint8 data) {
}

uint8 DSP4::readDR() {
  if(protocol.out_count) {
    uint8 dsp4_byte = (uint8) protocol.output[protocol.out_index & 0x1FF];
    protocol.out_index++;
    if(protocol.out_count == protocol.out_index) protocol.out_count = 0;
    return dsp4_byte;
  } else {
    return 0xff;
  }
}

void DSP4::writeDR(uint8 data) {
  // clear pending read
  if (protocol.out_index < protocol.out_count)
  {
    protocol.out_index++;
    return;
  }

  if (protocol.waiting4command)
  {
    if (protocol.half_command)
    {
      protocol.command |= (data << 8);
      protocol.in_index = 0;
      protocol.waiting4command = false;
      protocol.half_command = false;
      protocol.out_count = 0;
      protocol.out_index = 0;

      vars.Logic = 0;

      switch (protocol.command)
      {
        case 0x0000:
          protocol.in_count = 4; break;
        case 0x0001:
          protocol.in_count = 44; break;
        case 0x0003:
          protocol.in_count = 0; break;
        case 0x0005:
          protocol.in_count = 0; break;
        case 0x0006:
          protocol.in_count = 0; break;
        case 0x0007:
          protocol.in_count = 34; break;
        case 0x0008:
          protocol.in_count = 90; break;
        case 0x0009:
          protocol.in_count = 14; break;
        case 0x000a:
          protocol.in_count = 6; break;
        case 0x000b:
          protocol.in_count = 6; break;
        case 0x000d:
          protocol.in_count = 42; break;
        case 0x000e:
          protocol.in_count = 0; break;
        case 0x000f:
          protocol.in_count = 46; break;
        case 0x0010:
          protocol.in_count = 36; break;
        case 0x0011:
          protocol.in_count = 8; break;
        default:
          protocol.waiting4command = true;
          break;
      }
    }
    else
    {
      protocol.command = data;
      protocol.half_command = true;
    }
  }
  else
  {
    protocol.parameters[protocol.in_index] = data;
    protocol.in_index++;
  }

  if (!protocol.waiting4command && protocol.in_count == protocol.in_index)
  {
    // Actually execute the command
    protocol.waiting4command = true;
    protocol.out_index = 0;
    protocol.in_index = 0;

    switch (protocol.command)
    {
        // 16-bit multiplication
      case 0x0000:
      {
        int16 multiplier, multiplicand;
        int32 product;

        multiplier = READ_WORD();
        multiplicand = READ_WORD();

        Multiply(multiplicand, multiplier, &product);

        CLEAR_OUT();
        WRITE_WORD((uint16)(product));
        WRITE_WORD((uint16)(product >> 16));
      }
      break;

      // single-player track projection
      case 0x0001:
        OP01(); break;

      // single-player selection
      case 0x0003:
        OP03(); break;

      // clear OAM
      case 0x0005:
        OP05(); break;

      // transfer OAM
      case 0x0006:
        OP06(); break;

      // single-player track turnoff projection
      case 0x0007:
        OP07(); break;

      // solid polygon projection
      case 0x0008:
        OP08(); break;

      // sprite projection
      case 0x0009:
        OP09(); break;

      // unknown
      case 0x000A:
      {
        int16 in1a = READ_WORD();
        int16 in2a = READ_WORD();
        int16 in3a = READ_WORD();
        int16 out1a, out2a, out3a, out4a;

        OP0A(in2a, &out2a, &out1a, &out4a, &out3a);

        CLEAR_OUT();
        WRITE_WORD(out1a);
        WRITE_WORD(out2a);
        WRITE_WORD(out3a);
        WRITE_WORD(out4a);
      }
      break;

      // set OAM
      case 0x000B:
      {
        int16 sp_x = READ_WORD();
        int16 sp_y = READ_WORD();
        int16 sp_attr = READ_WORD();
        bool draw = 1;

        CLEAR_OUT();

        OP0B(&draw, sp_x, sp_y, sp_attr, 0, 1);
      }
      break;

      // multi-player track projection
      case 0x000D:
        OP0D(); break;

      // multi-player selection
      case 0x000E:
        OP0E(); break;

      // single-player track projection with lighting
      case 0x000F:
        OP0F(); break;

      // single-player track turnoff projection with lighting
      case 0x0010:
        OP10(); break;

      // unknown: horizontal mapping command
      case 0x0011:
      {
        int16 a, b, c, d, m;


        d = READ_WORD();
        c = READ_WORD();
        b = READ_WORD();
        a = READ_WORD();

        OP11(a, b, c, d, &m);

        CLEAR_OUT();
        WRITE_WORD(m);

        break;
      }

      default:
        break;
    }
  }
}
