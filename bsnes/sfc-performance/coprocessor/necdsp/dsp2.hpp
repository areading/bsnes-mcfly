struct DSP2 : ManagedNECDSP {
  struct {
    bool waiting_for_command;
    unsigned command;
    unsigned in_count,  in_index;
    unsigned out_count, out_index;

    uint8  parameters[512];
    uint8  output[512];

    uint8  op05transparent;
    bool   op05haslen;
    int    op05len;
    bool   op06haslen;
    int    op06len;
    uint16 op09word1;
    uint16 op09word2;
    bool   op0dhaslen;
    int    op0doutlen;
    int    op0dinlen;
  } status;

  auto reset() -> void override;

  auto readSR() -> uint8 override;
  auto writeSR(uint8 data) -> void override;
  auto readDR() -> uint8 override;
  auto writeDR(uint8 data) -> void override;
  auto readDP(uint12 addr) -> uint8 override {return 0x00;}
  auto writeDP(uint12 addr, uint8 data) -> void override {}

  auto serialize(serializer&) -> void override;

private:
  void op01();
  void op03();
  void op05();
  void op06();
  void op09();
  void op0d();
};
