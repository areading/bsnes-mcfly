struct DSP3 : ManagedNECDSP {
  auto reset() -> void override;
  auto readSR() -> uint8 override;
  auto writeSR(uint8 data) -> void override;
  auto readDR() -> uint8 override;
  auto writeDR(uint8 data) -> void override;
  auto readDP(uint12 addr) -> uint8 override {return 0x00;}
  auto writeDP(uint12 addr, uint8 data) -> void override {}
  auto serialize(serializer&) -> void override;

private:
  void MemorySize();
  void TestMemory();
  void DumpDataROM();
  void MemoryDump();
  void OP06();
  void OP03();
  void OP07_B();
  void OP07_A();
  void OP07();
  void Coordinate();
  void Convert_A();
  void Convert();
  bool GetBits(uint8 Count);
  void Decode_Data();
  void Decode_Tree();
  void Decode_Symbols();
  void Decode_A();
  void Decode();
  void OP3E();
  void OP1E();
  void OP1E_A();
  void OP1E_A1();
  void OP1E_A2();
  void OP1E_A3();
  void OP1E_B();
  void OP1E_B1();
  void OP1E_B2();
  void OP1E_C();
  void OP1E_C1();
  void OP1E_C2();
  void OP1E_D( int16 move, int16 *lo, int16 *hi );
  void OP1E_D1( int16 move, int16 *lo, int16 *hi );
  void OP10();
  void OP0C_A();
  void OP0C();
  void OP1C_C();
  void OP1C_B();
  void OP1C_A();
  void OP1C();
  void Command();

  function<auto () -> void> SetDSP3;

  uint16 DR;
  uint16 SR;
  uint16 MemoryIndex;

  int16 WinLo;
  int16 WinHi;

  int16 AddLo;
  int16 AddHi;

  uint8  Bitmap[8];
  uint8  Bitplane[8];
  uint16 BMIndex;
  uint16 BPIndex;
  uint16 Count;

  uint16 Codewords;
  uint16 Outwords;
  uint16 Symbol;
  uint16 BitCount;
  uint16 Index;
  uint16 Codes[512];
  uint16 BitsLeft;
  uint16 ReqBits;
  uint16 ReqData;
  uint16 BitCommand;
  uint8  BaseLength;
  uint16 BaseCodes;
  uint16 BaseCode;
  uint8  CodeLengths[8];
  uint16 CodeOffsets[8];
  uint16 LZCode;
  uint8  LZLength;

  uint16 X;
  uint16 Y;

  int16 op3e_x;
  int16 op3e_y;

  int16 op1e_terrain[0x2000];
  int16 op1e_cost[0x2000];
  int16 op1e_weight[0x2000];

  int16 op1e_cell;
  int16 op1e_turn;
  int16 op1e_search;

  int16 op1e_x;
  int16 op1e_y;

  int16 op1e_min_radius;
  int16 op1e_max_radius;

  int16 op1e_max_search_radius;
  int16 op1e_max_path_radius;

  int16 op1e_lcv_radius;
  int16 op1e_lcv_steps;
  int16 op1e_lcv_turns;

  uint8 byte;
  uint16 address;
};
