struct ManagedNECDSP {
  virtual void reset() =0;
  virtual uint8 readSR() =0;
  virtual void writeSR(uint8) =0;
  virtual uint8 readDR() =0;
  virtual void writeDR(uint8) =0;
  virtual uint8 readDP(uint12 addr) =0;
  virtual void writeDP(uint12 addr, uint8 data) =0;

  virtual void serialize(serializer&) =0;
};

#include "dsp1.hpp"
#include "dsp2.hpp"
#include "dsp3.hpp"
#include "dsp4.hpp"
#include "st010.hpp"

struct NECDSP {
  //necdsp.cpp
  void power();

  auto read(uint24 addr, uint8 data) -> uint8;
  auto write(uint24 addr, uint8 data) -> void;

  auto readRAM(uint24 addr, uint8 data) -> uint8;
  auto writeRAM(uint24 addr, uint8 data) -> void;

  //serialization.cpp
  auto firmware() const -> vector<uint8>;
  auto serialize(serializer&) -> void;

  enum class Revision : uint { uPD7725, uPD96050 } revision;
  uint8_t version;

  uint24 programROM[16384];
  uint16 dataROM[2048];
  uint16 dataRAM[2048];

  uint Frequency = 0;

private:
  DSP1 dsp1;
  DSP2 dsp2;
  DSP3 dsp3;
  DSP4 dsp4;
  ST010 st010;

  ManagedNECDSP* dsp = nullptr;
};

extern NECDSP necdsp;
