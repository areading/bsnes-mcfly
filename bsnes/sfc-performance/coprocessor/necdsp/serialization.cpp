auto NECDSP::firmware() const -> vector<uint8> {
  vector<uint8> buffer;
  if(!cartridge.has.NECDSP) return buffer;
  uint plength = 2048, dlength = 1024;
  if(revision == Revision::uPD96050) plength = 16384, dlength = 2048;
  buffer.reserve(plength * 3 + dlength * 2);

  for(auto n : range(plength)) {
    buffer.append(programROM[n] >>  0);
    buffer.append(programROM[n] >>  8);
    buffer.append(programROM[n] >> 16);
  }

  for(auto n : range(dlength)) {
    buffer.append(dataROM[n] >> 0);
    buffer.append(dataROM[n] >> 8);
  }

  return buffer;
}

auto NECDSP::serialize(serializer& s) -> void {
  if(dsp) {
    dsp->serialize(s);
  } else {
    s.array(necdsp.dataRAM);  //workaround for caching save state size
  }
}

void DSP1::serialize(serializer& s) {
  for(unsigned i = 0; i < 3; i++) {
    s.array(shared.MatrixA[i]);
    s.array(shared.MatrixB[i]);
    s.array(shared.MatrixC[i]);
  }

  s.integer(shared.CentreX);
  s.integer(shared.CentreY);
  s.integer(shared.CentreZ);
  s.integer(shared.CentreZ_C);
  s.integer(shared.CentreZ_E);
  s.integer(shared.VOffset);
  s.integer(shared.Les);
  s.integer(shared.C_Les);
  s.integer(shared.E_Les);
  s.integer(shared.SinAas);
  s.integer(shared.CosAas);
  s.integer(shared.SinAzs);
  s.integer(shared.CosAzs);
  s.integer(shared.SinAZS);
  s.integer(shared.CosAZS);
  s.integer(shared.SecAZS_C1);
  s.integer(shared.SecAZS_E1);
  s.integer(shared.SecAZS_C2);
  s.integer(shared.SecAZS_E2);
  s.integer(shared.Nx);
  s.integer(shared.Ny);
  s.integer(shared.Nz);
  s.integer(shared.Gx);
  s.integer(shared.Gy);
  s.integer(shared.Gz);
  s.integer(shared.Hx);
  s.integer(shared.Hy);
  s.integer(shared.Vx);
  s.integer(shared.Vy);
  s.integer(shared.Vz);

  s.integer(mSr);
  s.integer(mSrLowByteAccess);
  s.integer(mDr);
  s.integer(mFsmMajorState);
  s.integer(mCommand);
  s.integer(mDataCounter);
  s.array(mReadBuffer);
  s.array(mWriteBuffer);
  s.integer(mFreeze);
}

void DSP2::serialize(serializer& s) {
  s.integer(status.waiting_for_command);
  s.integer(status.command);
  s.integer(status.in_count);
  s.integer(status.in_index);
  s.integer(status.out_count);
  s.integer(status.out_index);

  s.array(status.parameters);
  s.array(status.output);

  s.integer(status.op05transparent);
  s.integer(status.op05haslen);
  s.integer(status.op05len);
  s.integer(status.op06haslen);
  s.integer(status.op06len);
  s.integer(status.op09word1);
  s.integer(status.op09word2);
  s.integer(status.op0dhaslen);
  s.integer(status.op0doutlen);
  s.integer(status.op0dinlen);
}

void DSP3::serialize(serializer& s) {
}

void DSP4::serialize(serializer& s) {
}

void ST010::serialize(serializer& s) {
  s.array(necdsp.dataRAM);
}
