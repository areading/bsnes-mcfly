auto ICD::audioSample(const double* samples, uint channels) -> void {
  stream->write(samples);
}

auto ICD::inputPoll(uint port, uint device, uint id) -> int16 {
  uint16 data = 0x00;
  switch(joypID) {
  case 0: data = controllerPort1.joy0; break;
  case 1: data = controllerPort2.joy0; break;
  case 2: data = controllerPort2.joy1; break;
  case 3: data = controllerPort2.joy0; break;  //TODO: Fix Super Multitap support
  }

  switch((GameBoy::Input)id) {
  case GameBoy::Input::Right:  return data.bit( 8);
  case GameBoy::Input::Left:   return data.bit( 9);
  case GameBoy::Input::Up:     return data.bit(11);
  case GameBoy::Input::Down:   return data.bit(10);
  case GameBoy::Input::A:      return data.bit( 7);
  case GameBoy::Input::B:      return data.bit(15);
  case GameBoy::Input::Select: return data.bit(13);
  case GameBoy::Input::Start:  return data.bit(12);
  }

  unreachable;
}
