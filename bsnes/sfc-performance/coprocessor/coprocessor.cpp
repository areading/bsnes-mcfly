#include <sfc-performance/sfc.hpp>

namespace higan::SuperFamicom {

#include <sfc/coprocessor/armdsp/armdsp.cpp>
#include <sfc/coprocessor/dip/dip.cpp>
#include <sfc/coprocessor/epsonrtc/epsonrtc.cpp>
#include <sfc/coprocessor/event/event.cpp>
#include "hitachidsp/hitachidsp.cpp"
#include "icd/icd.cpp"
#include <sfc/coprocessor/mcc/mcc.cpp>
#include "msu1/msu1.cpp"
#include "necdsp/necdsp.cpp"
#include <sfc/coprocessor/obc1/obc1.cpp>
#include "sa1/sa1.cpp"
#include <sfc/coprocessor/sdd1/sdd1.cpp>
#include <sfc/coprocessor/sharprtc/sharprtc.cpp>
#include <sfc/coprocessor/spc7110/spc7110.cpp>
#include "superfx/superfx.cpp"

}
