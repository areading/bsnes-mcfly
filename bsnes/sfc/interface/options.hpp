struct Options : Setting<> {
  struct Port : Setting<> { using Setting::Setting;
    struct Controller1 : Setting<> { using Setting::Setting;
      Setting<natural> device{this, "device", ID::Device::Gamepad};
    } controller1{this, "controller1"};

    struct Controller2 : Setting<> { using Setting::Setting;
      Setting<natural> device{this, "device", ID::Device::Gamepad};
    } controller2{this, "controller2"};

    struct Expansion : Setting<> { using Setting::Setting;
      Setting<natural> device{this, "device", ID::Device::None};
    } expansion{this, "expansion"};
  } port{this, "port"};

  Setting<boolean> random{this, "entropy", true};

  struct Video : Setting<> { using Setting::Setting;
    Setting<boolean> colorBleed{this, "colorBleed", true};
    Setting<boolean> colorEmulation{this, "colorEmulation", true};
  } video{this, "video"};

  struct Hack : Setting<> { using Setting::Setting;
    struct PPU : Setting<> { using Setting::Setting;
      Setting<boolean> fast{this, "fast", false};
      Setting<boolean> noSpriteLimit{this, "noSpriteLimit", false};
      Setting<boolean> hiresMode7{this, "hiresMode7", false};
      Setting<natural> frameSkip{this, "frameSkip", 0};
      Setting<boolean> bgDisable[8] = {
        {this, "bgDisable0lo", false},
        {this, "bgDisable0hi", false},
        {this, "bgDisable1lo", false},
        {this, "bgDisable1hi", false},
        {this, "bgDisable2lo", false},
        {this, "bgDisable2hi", false},
        {this, "bgDisable3lo", false},
        {this, "bgDisable3hi", false}
      };
      Setting<boolean> objDisable[4] = {
        {this, "objDisable0", false},
        {this, "objDisable1", false},
        {this, "objDisable2", false},
        {this, "objDisable3", false}
      };
    } ppu{this, "ppu"};

    struct DSP : Setting<> { using Setting::Setting;
      Setting<boolean> fast{this, "fast", false};
      Setting<boolean> channelMute[8] = {
        {this, "channelMute0", false},
        {this, "channelMute1", false},
        {this, "channelMute2", false},
        {this, "channelMute3", false},
        {this, "channelMute4", false},
        {this, "channelMute5", false},
        {this, "channelMute6", false},
        {this, "channelMute7", false}
      };
    } dsp{this, "dsp"};

    struct Coprocessor : Setting<> { using Setting::Setting;
      Setting<boolean> fast{this, "fast", false};
    } coprocessor{this, "coprocessor"};
  } hack{this, "hack"};

  Options() : Setting{"options"} {
    video.colorBleed.onModify([&] {
      higan::video.setEffect(higan::Video::Effect::ColorBleed, video.colorBleed());
    });
    video.colorEmulation.onModify([&] {
      higan::video.setPalette();
    });
    hack.ppu.frameSkip.onModify({&PPUfast::updateFrameSkip, &ppufast});
    for(auto& option : hack.ppu.bgDisable) option.onModify({&PPUfast::updateEnabledLayers, &ppufast});
    for(auto& option : hack.ppu.objDisable) option.onModify({&PPUfast::updateEnabledLayers, &ppufast});
    for(auto& option : hack.dsp.channelMute) option.onModify({&DSP::updateMuteMask, &dsp});
  }
};

extern Options option;
