#define UNICODE
#define QT_NO_DEBUG
#define QT_THREAD_SUPPORT

#include <QApplication>
#include <QtWidgets>

#undef foreach

#include <emulator/emulator.hpp>
#if !defined(PROFILE_PERFORMANCE)
  #include <sfc/sfc.hpp>
#else
  #include <sfc-performance/sfc.hpp>
#endif
#undef platform
namespace SFC = higan::SuperFamicom;
extern unique_pointer<higan::Interface> emulator;

#include <nall/inode.hpp>
#include <nall/directory.hpp>
#include <nall/file.hpp>
#include <nall/decode/base64.hpp>
#include <nall/decode/rle.hpp>
#include <nall/decode/zip.hpp>
#include <nall/encode/base64.hpp>
#include <nall/encode/rle.hpp>
#include <nall/encode/zip.hpp>

#include <obj/ui-resource.hpp>

#include "template/check-action.hpp"
#include "template/file-dialog.moc.hpp"
#include "template/hex-editor.hpp"
#include "template/radio-action.hpp"
#include "template/window.hpp"

namespace nall {

template<> struct stringify<QString> {
  const QString& _text;
  auto data() const -> const char* { return _text.toUtf8().constData(); }
  auto size() const -> uint { return _text.toUtf8().size(); }
  stringify(const QString& source) : _text(source) {}
};

template<> struct stringify<const QString&> {
  const QString& _text;
  auto data() const -> const char* { return _text.toUtf8().constData(); }
  auto size() const -> uint { return _text.toUtf8().size(); }
  stringify(const QString& source) : _text(source) {}
};

//string::operator QString() const {
//  return QString::fromUtf8(*this);
//}

}

using namespace nall;

#include <ruby/ruby.hpp>
using namespace ruby;
extern Video video;
extern Audio audio;
extern Input input;

#include "hiro-shim.hpp"

#include "config/config.hpp"

#include "base/filebrowser.hpp"
#include "base/htmlviewer.hpp"
#include "base/loader.hpp"
#include "base/stateselect.hpp"

#include "input/input.hpp"
#include "movie/movie.hpp"
#include "presentation/presentation.hpp"
#include "program/program.hpp"
#include "reader/reader.hpp"
#include "settings/settings.hpp"
#include "tools/tools.hpp"
#include "window/window.hpp"

struct Style {
  static const char Monospace[64];

  enum {
    WindowMargin     = 5,
    WidgetSpacing    = 5,
    SeparatorSpacing = 5,
  };
};

extern string locate(string name);
