<html>
  <!DOCTYPE html>
  <html lang="en">
  <head>
  <meta charset="UTF-8"/>
  <title>bsnes-mcfly Usage Documentation</title>
  </head>
  <body>

== bsnes-mcfly Usage Documentation

bsnes-mcfly is a Super Nintendo / Super Famicom emulator that, being based on
higan and bsnes, strives to provide the most faithful hardware emulation
possible. It focuses on accuracy and clean code, rather than speed and special
features. It is meant as a reference emulator to document how the underlying
hardware works. It is thus very useful for development and research. And while
it can be used for general purpose gaming, it will require significantly more
powerful hardware than a typical emulator.

=== Modes of Operation

bsnes-mcfly is capable of running both in its default multi-user mode, as well
as in single-user mode.

In multi-user mode, configuration data is stored inside the user's home
directory. On Windows, this is located at "%APPDATA%\bsnes-mcfly". On
other operating systems, this is located at "~/.local/share/bsnes-mcfly".

To enable single-user mode, create a blank "settings-qt.bml" file inside the
same folder as the bsnes-mcfly executable. bsnes will then use this file to
store configuration data.

=== Supported Filetypes

* **SFC:** SNES cartridge — ROM image.
* **BS:** Satellaview BS-X flash cartridge — EEPROM image.
* **ST:** Sufami Turbo cartridge — ROM image.
* **GB:** Game Boy cartridge — ROM image.
* **SRM, PSR:** non-volatile memory, often used to save game data — PSRAM image.
* **RTC:** real-time clock non-volatile memory.
* **BPS:** patch data, used to dynamically modify cartridge of same base filename upon load.
* **IPS:** deprecated patch data, used to dynamically modify cartridge of same base filename upon load.
* **UPS:** deprecated patch data, used to dynamically modify cartridge of same base filename upon load.
* **CHT:** plain-text list of "Game Genie" / "Pro Action Replay" codes.

=== Known Limitations

**Satellaview BS-X emulation:** this hardware is only partially supported.
This is mostly because the satellite network it used (St. GIGA) has been shut
down. Access to this network would be required to properly reverse engineer much
of the hardware. Working around this would require game-specific hacks, which
are contrary to the design goals of this emulator. As a result, most BS-X
software will not function correctly.

**Netplay:** internet multiplay is not currently supported nor planned.

=== Contributors

* Andreas Naive
* anomie
* _Demo_
* Derrick Sobodash
* DMV27
* FirebrandX
* FitzRoy
* GIGO
* Jonas Quinn
* kode54
* krom
* Matthew Callis
* Nach
* neviksti
* Overload
* RedDwarf
* Richard Bannister
* Shay Green
* tetsuo55
* TRAC
* zones

<html>
  </body>
  </html>