#include "../qt.hpp"
#include <nall/decode/gzip.hpp>

unique_pointer<ArchiveReader> archiveReader;
Reader reader;

ArchiveReader::ArchiveReader() {
  if(open("archive-reader")) {
    dl_support = sym("support");
    dl_extract = sym("extract");
    if(!dl_support || !dl_extract) close();
  }
}

ArchiveReader::~ArchiveReader() {
  reset();
}

auto ArchiveReader::support() -> vector<string> {
  vector<string> compressionList = {".zip", ".gz"};
  if(open()) {
    for(string extension : string{dl_support()}.split(":")) {
      compressionList.append(extension);
    }
  }
  return compressionList;
}

auto ArchiveReader::extract(const string& filename) -> vector<ArchiveFile> {
  vector<ArchiveFile> files;

  string suffix = Location::suffix(filename);
  if(suffix == ".zip") {
    Decode::ZIP archive;
    if(!archive.open(filename)) return {};

    for(auto& compressedFile : archive.file) {
      ArchiveFile& file = files(files.size());
      file.name = compressedFile.name;
      file.contents = archive.extract(compressedFile);
    }
    archive.close();
    return files;
  } else if(suffix == ".gz") {
    Decode::GZIP archive;
    if(!archive.decompress(filename)) return {};

    ArchiveFile& file = files(0);
    file.contents.resize(archive.size);
    memory::copy<uint8_t>(file.contents.data(), archive.data, archive.size);
    return files;
  } else {
    if(open()) {
      dl_extract(filename, fileCount, fileNames, fileContents, fileSizes);

      for(uint n : range(fileCount)) {
        ArchiveFile& file = files(n);
        file.name = fileNames[n];
        file.contents.resize(fileSizes[n]);
        memory::copy(file.contents.data(), fileContents[n], fileSizes[n]);
      }
      reset();
      return files;
    }
    return {};
  }
}

auto ArchiveReader::reset() -> void {
  if(!fileCount) return;
  for(uint n : range(fileCount)) {
    delete[] fileNames[n];
    delete[] fileContents[n];
  }
  delete[] fileNames;
  delete[] fileContents;
  delete[] fileSizes;
  fileNames = nullptr;
  fileContents = nullptr;
  fileSizes = nullptr;
  fileCount = 0;
}

Reader::Reader() {
  archiveReader = new ArchiveReader;
  compressionList = archiveReader->support();

  sfcExtensionList = {
    ".sfc",".smc",".swc",".fig",
    ".ufo",".gd3",".gd7",".dx2",".mgd",".mgh",
    ".048",".058",".068",".078",".bin",
    ".usa",".eur",".jap",".aus"
  };

  gbExtensionList = {
    ".gb",".sgb",".gbc"
  };

  bsExtensionList = {
    ".bs",".bsx",".smc",".swc",".fig",
    ".ufo",".gd3",".gd7",".dx2",".mgd",".mgh",
    ".048",".058",".068",".078",".bin",
    ".jap",".sfc"
  };

  stExtensionList = {
    ".st",".smc",".swc",".fig",
    ".ufo",".gd3",".gd7",".dx2",".mgd",".mgh",
    ".048",".058",".068",".078",".bin",
    ".jap",".sfc"
  };

  for(string ext : sfcExtensionList) if(gbExtensionList.find(ext)) sharedExtensions.insert(ext);
  for(string ext : sfcExtensionList) if(bsExtensionList.find(ext)) sharedExtensions.insert(ext);
  for(string ext : sfcExtensionList) if(stExtensionList.find(ext)) sharedExtensions.insert(ext);
  for(string ext : gbExtensionList) if(bsExtensionList.find(ext)) sharedExtensions.insert(ext);
  for(string ext : gbExtensionList) if(stExtensionList.find(ext)) sharedExtensions.insert(ext);
  for(string ext : bsExtensionList) if(stExtensionList.find(ext)) sharedExtensions.insert(ext);
}

auto Reader::load(const string& location, string& type) -> vector<uint8_t> {
  if(file::exists(location) == false) return {};

  string suffix = Location::suffix(location);
  vector<uint8_t> rom;

  //1. Extract compressed archive
  if(compressionList.find(Location::suffix(location))) {
    auto files = archiveReader->extract(location);
    if(files.size() == 0) return {};
    if(files.size() == 1) {
      rom = files[0].contents;
    } else {
      for(auto& file : files) {
        auto type = Location::suffix(file.name);
        if(sfcExtensionList.find(type)
        || gbExtensionList.find(type)
        || bsExtensionList.find(type)
        || stExtensionList.find(type)
        ) {
          rom = file.contents;
          break;
        }
      }
    }
  } else {
    rom = file::read(location);
  }

  //2. Determine type (if not manually specified)
  if(!type && !sharedExtensions.find(suffix)) {
    if(sfcExtensionList.find(type)) type = ".sfc";
    #if defined(CORE_GB)
    if(gbExtensionList.find(type)) type = ".gb";
    #endif
    if(bsExtensionList.find(type)) type = ".bs";
    if(stExtensionList.find(type)) type = ".st";
  } else if(!type) {
    type = ".sfc";

    #if defined(CORE_GB)
    //detect Game Boy
    static const uint8_t gbSignature[] = {
      0xce, 0xed, 0x66, 0x66, 0xcc, 0x0d, 0x00, 0x0b,
      0x03, 0x73, 0x00, 0x83, 0x00, 0x0c, 0x00, 0x0d,
      0x00, 0x08, 0x11, 0x1f, 0x88, 0x89, 0x00, 0x0e,
      0xdc, 0xcc, 0x6e, 0xe6, 0xdd, 0xdd, 0xd9, 0x99,
      0xbb, 0xbb, 0x67, 0x63, 0x6e, 0x0e, 0xec, 0xcc,
      0xdd, 0xdc, 0x99, 0x9f, 0xbb, 0xb9, 0x33, 0x3e,
    };
    if(memory::compare(gbSignature, rom.data() + 0x0104, 0x30) == 0) type = ".gb";
    #endif

    uint header = 0;
    //For now, disallow copier headers on BS Memory images.
    for(uint headerAddress : {0x7fb0, 0xffb0}) {
      if(rom.size() < headerAddress + 0x50) continue;
      uint16_t complement = rom[headerAddress + 0x2c] | rom[headerAddress + 0x2d] << 8;
      uint16_t checksum   = rom[headerAddress + 0x2e] | rom[headerAddress + 0x2f] << 8;
      if(complement + checksum == 0xffff) {
        header = headerAddress;
        break;
      }
    }

    //detect BS Memory
    if(type == ".sfc" && header) {
      if(rom[header + 0x23] == 0x00 || rom[header + 0x23] == 0xff) {
        if(rom[header + 0x24] == 0x00) {
          const uint8_t n25 = rom[header + 0x25];
          if(n25 == 0x00 || n25 == 0x80 || n25 == 0x84
          || n25 == 0x9c || n25 == 0xbc || n25 == 0xfc) {
            if(rom[header + 0x2a] == 0x33 || rom[header + 0x2a] == 0xff) {
              type = ".bs";
            }
          }
        }
      }
    }

    //detect Sufami Turbo
    if(type == ".sfc") {
      if(memory::compare(rom.data() +  0, "BANDAI SFC-ADX", 14) == 0
      && memory::compare(rom.data() + 16, "SFC-ADX BACKUP", 14) != 0) {
        type = ".st";
      }
    }
  }

  return rom;
}
