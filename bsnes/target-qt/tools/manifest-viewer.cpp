ManifestViewer::ManifestViewer() {
  layout = new QVBoxLayout;
  layout->setAlignment(Qt::AlignTop | Qt::AlignLeft);
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(Style::WidgetSpacing);
  setLayout(layout);

  manifestView = new QPlainTextEdit;
  manifestView->setReadOnly(true);
  manifestView->setFont(QFont(Style::Monospace));
  manifestView->setLineWrapMode(QPlainTextEdit::NoWrap);
  layout->addWidget(manifestView);
}

void ManifestViewer::doRefresh() {
  manifestView->setPlainText("");
  if(emulator->loaded()) manifestView->setPlainText(QString::fromUtf8(emulator->manifests().merge("\n")));
}
