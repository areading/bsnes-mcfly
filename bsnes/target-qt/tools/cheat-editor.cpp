unique_pointer<CheatDatabase> cheatDatabase;

CheatEditor::CheatEditor() {
  lock = false;

  layout = new QVBoxLayout;
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(Style::WidgetSpacing);
  setLayout(layout);

  cheatList = new QTreeWidget;
  cheatList->setColumnCount(3);
  cheatList->setHeaderLabels(QStringList() << "Slot" << "Code" << "Description");
  cheatList->setColumnWidth(0, cheatList->fontMetrics().width("999"));
  cheatList->setColumnWidth(1, cheatList->fontMetrics().width("  89AB-CDEF+...  "));
  cheatList->setAllColumnsShowFocus(true);
  cheatList->sortByColumn(0, Qt::AscendingOrder);
  cheatList->setRootIsDecorated(false);
  cheatList->setSelectionMode(QAbstractItemView::ExtendedSelection);
  cheatList->resizeColumnToContents(0);
  layout->addWidget(cheatList);

  for(uint n = 0; n < 128; n++) {
    auto item = new QTreeWidgetItem(cheatList);
    item->setData(0, Qt::UserRole, QVariant(n));
    item->setTextAlignment(0, Qt::AlignRight);
    item->setText(0, QString::fromUtf8(pad(n + 1, 3L)));
    item->setCheckState(0, Qt::Unchecked);
    item->setText(1, "");
    item->setText(2, "");

    cheatText[n][Code] = "";
    cheatText[n][Desc] = "";
  }

  gridLayout = new QGridLayout;
  layout->addLayout(gridLayout);

  codeLabel = new QLabel("Code(s):");
  codeLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
  gridLayout->addWidget(codeLabel, 0, 0);

  codeEdit = new QLineEdit;
  gridLayout->addWidget(codeEdit, 0, 1);

  descLabel = new QLabel("Description:");
  descLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
  gridLayout->addWidget(descLabel, 1, 0);

  descEdit = new QLineEdit;
  gridLayout->addWidget(descEdit, 1, 1);

  controlLayout = new QHBoxLayout;
  layout->addLayout(controlLayout);

  cheatEnableBox = new QCheckBox("Enable Cheat Engine");
  cheatEnableBox->setToolTip("Unchecking this disables all cheat codes");
  cheatEnableBox->setChecked(config->system.cheatEnabled);
  controlLayout->addWidget(cheatEnableBox);

  spacer = new QWidget;
  spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
  controlLayout->addWidget(spacer);

  findButton = new QPushButton("Find Cheat Codes ...");
  controlLayout->addWidget(findButton);

  clearButton = new QPushButton("Clear Selected");
  controlLayout->addWidget(clearButton);

  cheatDatabase = new CheatDatabase;
  synchronize();

  connect(cheatList, &QTreeWidget::itemSelectionChanged, this, &CheatEditor::synchronize);
  connect(cheatList, QOverload<QTreeWidgetItem*, int>::of(&QTreeWidget::itemChanged), this, &CheatEditor::updateInterface);
  connect(codeEdit, QOverload<const QString&>::of(&QLineEdit::textEdited), this, &CheatEditor::updateCode);
  connect(descEdit, QOverload<const QString&>::of(&QLineEdit::textEdited), this, &CheatEditor::updateDesc);
  connect(cheatEnableBox, QOverload<int>::of(&QCheckBox::stateChanged), this, &CheatEditor::updateInterface);
  connect(findButton, &QPushButton::released, this, &CheatEditor::findCheatCodes);
  connect(clearButton, &QPushButton::released, this, &CheatEditor::clearSelected);
}

auto CheatEditor::reset() -> void {
  synchronize();
  auto items = cheatList->findItems("", Qt::MatchContains);
  for(uint n = 0; n < 128; n++) {
    items[n]->setCheckState(0, Qt::Unchecked);
    cheatText[n][Code] = "";
    cheatText[n][Desc] = "";
  }
  codeEdit->setText("");
  descEdit->setText("");
  updateUI(), updateInterface();
}

auto CheatEditor::loadCheats() -> void {
  synchronize();
  auto location = program->cheatPath(/*gameGenie = */true);
  if(!file::exists(location)) location = program->cheatPath(false);
  auto document = BML::unserialize(string::read(location));

  lock = true;
  auto items = cheatList->findItems("", Qt::MatchContains);
  uint n = 0;
  for(auto cheat : document.find("cheat")) {
    items[n]->setCheckState(0, cheat["enable"] ? Qt::Checked : Qt::Unchecked);
    cheatText[n][Code] = cheat["code"].text();
    cheatText[n][Desc] = cheat["name"].text();
    if(++n >= 128) break;
  }

  lock = false;
  updateUI(), updateInterface();
}

auto CheatEditor::saveCheats() -> void {
  synchronize();

  int saveCount = 0;
  for(int n = 127; n >= 0; n--) {
    if(cheatText[n][Code] != "" || cheatText[n][Desc] != "") {
      saveCount = n + 1;
      break;
    }
  }

  string documentPAR;
  string documentGG;
  bool gameGenieUsed = false;
  auto items = cheatList->findItems("", Qt::MatchContains);
  if(saveCount > 0) {
    for(uint n : range(saveCount)) {
      if(cheatText[n][Code] || cheatText[n][Desc]) {  //skip blank cheats
        documentPAR.append("cheat\n");
        documentPAR.append("  name:", cheatText[n][Desc], "\n");
        if(cheatText[n][Code]) {
          auto parCodes = cheatText[n][Code].replace(" ", "").split("+");
          for(auto& parCode : parCodes) {
            if(auto decoded = decode(parCode)) {
              parCode = decoded();
            }// else {
              //Leave it as is
            //}
          }
          documentPAR.append("  code:", parCodes.merge("+"), "\n");
        } else {
          documentPAR.append("  code:\n");
        }
      if(items[n]->checkState(0) == Qt::Checked)
        documentPAR.append("  enable\n");
        documentPAR.append("\n");
      }

      documentGG.append("cheat\n");
      documentGG.append("  name:", cheatText[n][Desc], "\n");
      documentGG.append("  code:", cheatText[n][Code], "\n");
    if(items[n]->checkState(0) == Qt::Checked)
      documentGG.append("  enable\n");
      documentGG.append("\n");

      if(!gameGenieUsed) {
        for(string code : cheatText[n][Code].split("+")) {
          if(code.match("????" "-" "????")) {
            gameGenieUsed = true;
            break;
          }
        }
      }
    }
  }

  auto location = program->cheatPath(false);
  if(documentPAR) {
    file::write(location, documentPAR);
  } else {
    file::remove(location);
  }

  location = program->cheatPath(true);
  if(gameGenieUsed) {
    file::write(location, documentGG);
  } else {
    file::remove(location);
  }
  usleep(2000);
}

auto CheatEditor::addCode(const string& code, const string& description) -> bool {
  auto items = cheatList->findItems("", Qt::MatchContains);
  for(uint n = 0; n < 128; n++) {
    if(cheatText[n][Code] == "" && cheatText[n][Desc] == "") {
      items[n]->setCheckState(0, Qt::Unchecked);
      cheatText[n][Code] = code;
      cheatText[n][Desc] = description;
      return true;
    }
  }
  return false;
}

void CheatEditor::synchronize() {
  if(!emulator->loaded() && cheatDatabase->isVisible()) cheatDatabase->close();

  auto items = cheatList->selectedItems();
  findButton->setEnabled(emulator->loaded());
  clearButton->setEnabled(items.count() > 0);
  if(items.count() == 1) {
    uint n = items[0]->data(0, Qt::UserRole).toUInt();
    codeEdit->setText(QString::fromUtf8(cheatText[n][Code]));
    descEdit->setText(QString::fromUtf8(cheatText[n][Desc]));
    codeEdit->setEnabled(true);
    descEdit->setEnabled(true);
  } else {
    codeEdit->setText("");
    codeEdit->setEnabled(false);
    descEdit->setText("");
    descEdit->setEnabled(false);
  }
}

void CheatEditor::updateUI() {
  cheatList->setSortingEnabled(false);
  auto items = cheatList->findItems("", Qt::MatchContains);
  for(uint n = 0; n < 128; n++) {
    auto item = items[n];
    item->setText(1, QString::fromUtf8(cheatText[n][Code]));
    item->setText(2, QString::fromUtf8(cheatText[n][Desc]));
  }
  cheatList->resizeColumnToContents(0);
  cheatList->setSortingEnabled(true);
  cheatList->header()->setSortIndicatorShown(false);

  for(auto& item : items) {
    uint index = item->data(0, Qt::UserRole).toUInt();
    string code = cheatText[index][Code];
    string desc = cheatText[index][Desc];
    if((code != "") || (desc != "")) {
      item->setForeground(0, QBrush(QColor(0, 0, 0)));
    } else {
      //highlight empty slots in gray
      item->setForeground(0, QBrush(QColor(128, 128, 128)));
    }

    bool valid = code.length() > 0;
    if(code.length() > 0) {
      auto list = code.replace(" ", "").split("+");
      for(uint i = 0; i < list.size(); i++) {
        code = list[i];
        valid = (bool)decode(list[i]);
        if(!valid) break;
      }
    }
    if(valid) {
      item->setForeground(1, QBrush(QColor(0, 0, 0)));
    } else {
      //highlight invalid codes in red
      //(this will also highlight empty codes, but as there is no text, it's not an issue)
      item->setForeground(1, QBrush(QColor(255, 0, 0)));
    }
  }
}

void CheatEditor::updateInterface() {
  if(lock) return;

  vector<string> cheatCodes;
  config->system.cheatEnabled = cheatEnableBox->isChecked();
  if(config->system.cheatEnabled) {
    auto items = cheatList->findItems("", Qt::MatchContains);
    for(uint n = 0; n < 128; n++) {
      string code = cheatText[n][Code];
      auto list = code.replace(" ", "").split("+");
      code = "";
      for(uint i = 0; i < list.size(); i++) {
        if(i > 0) code.append("+");
        if(auto decoded = decode(list[i])) {
          code.append(decoded());
        } else {
          code.append("");
        }
      }
      if(items[n]->checkState(0) == Qt::Checked && code != "") cheatCodes.append(code);
    }
  }
  emulator->cheats(cheatCodes);
}

void CheatEditor::updateCode() {
  if(lock) return;

  auto items = cheatList->selectedItems();
  if(items.count() != 1) return;
  uint n = items[0]->data(0, Qt::UserRole).toUInt();
  cheatText[n][Code] = codeEdit->text();
  updateUI(), updateInterface();
}

void CheatEditor::updateDesc() {
  if(lock) return;

  auto items = cheatList->selectedItems();
  if(items.count() != 1) return;
  uint n = items[0]->data(0, Qt::UserRole).toUInt();
  cheatText[n][Desc] = descEdit->text();
  updateUI(), updateInterface();
}

void CheatEditor::findCheatCodes() {
  cheatDatabase->findCodes();
}

void CheatEditor::clearSelected() {
  if(lock) return;

  auto items = cheatList->selectedItems();
  if(items.size() == 0) return;
  for(auto& item : items) {
    uint index = item->data(0, Qt::UserRole).toUInt();
    item->setCheckState(0, Qt::Unchecked);
    cheatText[index][Code] = "";
    cheatText[index][Desc] = "";
  }
  codeEdit->setText("");
  descEdit->setText("");
  updateUI(), updateInterface();
}

maybe<string> CheatEditor::decode(string code) {
  #if defined(CORE_GB)
  if(SFC::cartridge.has.ICD && SFC::cartridge.has.GameBoySlot) return decodeGB(code);
  #endif

  #define ischr(n) ((n >= '0' && n <= '9') || (n >= 'a' && n <= 'f'))

  code.downcase();

  if(code.size() ==  9 && code[6] == '=') {
    for(uint n : {0,1,2,3,4,5,7,8}) if(!ischr(code[n])) return nothing;
    return code;
  }

  if(code.size() == 12 && code[6] == '=' && code[9] == '?') {
    for(uint n : {0,1,2,3,4,5,7,8,10,11}) if(!ischr(code[n])) return nothing;
    return code;
  }

  if(code.size() == 9 && (code[6] == '/' || code[6] == ':')) {
    //Direct (legacy formats)
    for(uint n : {0,1,2,3,4,5,7,8}) if(!ischr(code[n])) return nothing;
    return string{slice(code, 0, 6), "=", slice(code, 7, 2)};
  }

  if(code.size() == 12 && code[6] == '/' && code[9] == '/') {
    //Direct (legacy format)
    for(uint n : {0,1,2,3,4,5,7,8,10,11}) if(!ischr(code[n])) return nothing;
    return string{slice(code, 0, 6), "=", slice(code, 7, 2), "?", slice(code, 10, 2)};
  }

  if(code.size() == 8) {
    //Pro Action Replay
    for(uint n : range(8)) if(!ischr(code[n])) return nothing;
    return string{slice(code, 0, 6), "=", slice(code, 6, 2)};
  }

  if(code.size() == 9 && code[4] == '-') {
    //Game Genie
    code = {slice(code, 0, 4), slice(code, 5, 4)};
    for(uint n : range(8)) if(!ischr(code[n])) return nothing;
    code.transform("df4709156bc8a23e", "0123456789abcdef");
    uint r = toHex(code);
    static uint bits[] = {13, 12, 11, 10, 5, 4, 3, 2, 23, 22, 21, 20, 1, 0, 15, 14, 19, 18, 17, 16, 9, 8, 7, 6};

    uint addr = 0;
    for(uint n : range(24)) addr |= r & (1 << bits[n]) ? 0x800000 >> n : 0;
    uint8_t data = r >> 24;
    return string{hex(addr, 6L), "=", hex(data, 2L)};
  }

  #undef ischr

  return nothing;
}

maybe<string> CheatEditor::decodeGB(string code) {
  static bool initialize = false;
  static uint8 mapGameGenie[256];

  if(initialize == false) {
    initialize = true;

    for(auto& n : mapGameGenie) n = ~0;
    mapGameGenie['0'] =  0; mapGameGenie['1'] =  1; mapGameGenie['2'] =  2; mapGameGenie['3'] =  3;
    mapGameGenie['4'] =  4; mapGameGenie['5'] =  5; mapGameGenie['6'] =  6; mapGameGenie['7'] =  7;
    mapGameGenie['8'] =  8; mapGameGenie['9'] =  9; mapGameGenie['a'] = 10; mapGameGenie['b'] = 11;
    mapGameGenie['c'] = 12; mapGameGenie['d'] = 13; mapGameGenie['e'] = 14; mapGameGenie['f'] = 15;
  }

  #define ischr(n) ((n >= '0' && n <= '9') || (n >= 'a' && n <= 'f'))

  code.downcase();
  uint length = code.length(), bits = 0;

  if(code.size() == 7 && code[4] == '=') {
    for(uint n : {0,1,2,3,5,6}) if(!ischr(code[n])) return nothing;
    return code;
  }

  if(code.size() == 10 && code[4] == '=' && code[7] == '?') {
    for(uint n : {0,1,2,3,5,6,8,9}) if(!ischr(code[n])) return nothing;
    return code;
  }

  if(code.size() == 7 && (code[4] == '/' || code[4] == ':')) {
    //Direct (legacy formats)
    for(uint n : {0,1,2,3,5,6}) if(!ischr(code[n])) return nothing;
    return string{slice(code, 0, 4), "=", slice(code, 5, 2)};
  }

  if(code.size() == 10) {
    //Direct (legacy formats)
    for(uint n : {0,1,2,3,5,6,8,9}) if(!ischr(code[n])) return nothing;

    if(code[4] == '/' && code[7] == '/') {  //addr/comp/data
      return string{slice(code, 0, 4), "=", slice(code, 5, 2), "?", slice(code, 8, 2)};
    }

    if(code[4] == ':' && code[7] == ':') {  //addr:data:comp
      return string{slice(code, 0, 4), "=", slice(code, 8, 2), "?", slice(code, 5, 2)};
    }
  }

  //TODO: Is this really Pro Action Replay?
  //bsnes v073's included version of Gambatte pre-dated the addition of cheats
  //to mainline Gambatte, and bgameboy/bgb mandates the use of the Direct
  //format with separators.
  /*
  if(code.size() == 6) {
    for(uint n : range(6)) if(!ischr(code[n])) return nothing;
    return string{slice(code, 0, 4), "=", slice(code, 4, 2)};
  }

  if(code.size() == 8) {
    for(uint n : range(8)) if(!ischr(code[n])) return nothing;
    return string{slice(code, 0, 4), "=", slice(code, 4, 2), "?", slice(code, 6, 2)};
  }
  */

  if(code.size() == 7 && code[3] == '-') {
    //Game Genie
    for(uint n : {0, 1, 2, 4, 5, 6}) {
      if(!ischr(code[n])) return nothing;
      bits |= mapGameGenie[code[n]] << (20 - n * 4);
    }

    uint16_t addr = (bits >> 0) & 0xffff;
    addr = (((addr >> 4) | (addr << 12)) & 0xffff) ^ 0xf000;

    uint8_t data = (bits >> 16) & 0xff;

    return string{hex(addr, 4L), "=", hex(data, 2L)};
  }

  if(code.size() == 11 && code[3] == '-' && code[7] == '-') {
    //Game Genie
    for(uint n : {0, 1, 2, 4, 5, 6, 8, 9, 10}) {
      if(!ischr(code[n])) return nothing;
      bits |= mapGameGenie[code[n]] << (28 - n * 4);
    }

    uint16_t addr = (bits >>  8) & 0xffff;
    addr = (((addr >> 4) | (addr << 12)) & 0xffff) ^ 0xf000;

    uint8_t data = (bits >> 24) & 0xff;

    uint8_t comp = (bits >>  0) & 0xff;
    comp = (((comp >> 2) | (comp << 6)) & 0xff) ^ 0xba;

    return string{hex(addr, 4L), "=", hex(comp, 2L), "?", hex(data, 2L)};
  }

  #undef ischr

  return nothing;
}

//=============
//CheatDatabase
//=============

void CheatDatabase::findCodes() {
  cheatList->clear();
  cheatCode.reset();
  audio.clear();

  auto document = BML::unserialize(Resource::Cheats::SuperFamicom);
  for(auto& node : document.find("cartridge")) {
    if(node["sha256"].text() != emulator->hashes()[0]) continue;

    title->setText(QString::fromUtf8(string{"<b>Name:</b> ", node["title"].text()}));
    for(auto& cheat : node.find("cheat")) {
      if(!cheat["code"]) continue;

      string description = cheat["description"] ? cheat["description"].text() : "<undefined>";
      string code = cheat["code"].text();

      auto item = new QTreeWidgetItem(cheatList);
      item->setCheckState(0, Qt::Unchecked);
      item->setText(0, QString::fromUtf8(description));
      item->setData(0, Qt::UserRole, QVariant(QString::fromUtf8(code)));

      cheatCode.append({cheat["code"].text(), "\t", cheat["description"].text()});
    }

    show();
    return;
  }

  QMessageBox::information(&*toolsWindow, "Cheat Code Importer",
    "Sorry, no cheat codes were found for the currently loaded cartridge."
  );
}

CheatDatabase::CheatDatabase() {
  resize(640, 360);

  setObjectName("cheat-import-window");
  setWindowTitle("Cheat Code Importer");
  setGeometryString(&config->geometry.cheatDatabase);

  layout = new QVBoxLayout;
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(Style::WidgetSpacing);
  setLayout(layout);

  title = new QLabel("Test");
  layout->addWidget(title);

  cheatList = new QTreeWidget;
  cheatList->setColumnCount(1);
  cheatList->setAllColumnsShowFocus(true);
  cheatList->setRootIsDecorated(false);
  cheatList->setHeaderHidden(true);
  layout->addWidget(cheatList);

  controlLayout = new QHBoxLayout;
  layout->addLayout(controlLayout);

  selectAllButton = new QPushButton("Select All");
  controlLayout->addWidget(selectAllButton);

  clearAllButton = new QPushButton("Clear All");
  controlLayout->addWidget(clearAllButton);

  spacer = new QWidget;
  spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
  controlLayout->addWidget(spacer);

  okButton = new QPushButton("Ok");
  controlLayout->addWidget(okButton);

  cancelButton = new QPushButton("Cancel");
  controlLayout->addWidget(cancelButton);

  connect(selectAllButton, &QPushButton::released, this, &CheatDatabase::selectAllCodes);
  connect(clearAllButton, &QPushButton::released, this, &CheatDatabase::clearAllCodes);
  connect(okButton, &QPushButton::released, this, &CheatDatabase::addCodes);
  connect(cancelButton, &QPushButton::released, this, &CheatDatabase::close);
}

void CheatDatabase::selectAllCodes() {
  auto items = cheatList->findItems("", Qt::MatchContains);
  for(auto& item : items) item->setCheckState(0, Qt::Checked);
}

void CheatDatabase::clearAllCodes() {
  auto items = cheatList->findItems("", Qt::MatchContains);
  for(auto& item : items) item->setCheckState(0, Qt::Unchecked);
}

void CheatDatabase::addCodes() {
  auto items = cheatList->findItems("", Qt::MatchContains);
  for(uint n = 0; n < cheatCode.size(); n++) {
    if(items[n]->checkState(0) == Qt::Checked) {
      auto part = cheatCode[n].split("\t", 1L);
      toolsWindow->cheatEditor->addCode(part[0], part[1]);
    }
  }

  toolsWindow->cheatEditor->updateUI();
  toolsWindow->cheatEditor->updateInterface();
  toolsWindow->cheatEditor->synchronize();
  close();
}
