#ifndef NALL_QT_CHECKACTION_HPP
#define NALL_QT_CHECKACTION_HPP

namespace nall {

class CheckAction : public QAction {
public:
  bool isChecked() const;
  void setChecked(bool);
  void toggleChecked();
  CheckAction(const QString&, QObject*);

protected:
  bool checked;
};

inline bool CheckAction::isChecked() const {
  return checked;
}

inline void CheckAction::setChecked(bool checked_) {
  checked = checked_;
  const uint8_t* icon = checked ? Resource::Widget::ItemCheckOn : Resource::Widget::ItemCheckOff;
  size_t size = checked ? sizeof(Resource::Widget::ItemCheckOn) : sizeof(Resource::Widget::ItemCheckOff);
  QPixmap pixmap;
  pixmap.loadFromData(icon, size, "PNG");
  setIcon(QIcon(pixmap));
}

inline void CheckAction::toggleChecked() {
  setChecked(!isChecked());
}

inline CheckAction::CheckAction(const QString& text, QObject* parent) : QAction(text, parent) {
  setChecked(false);
}

}

#endif
