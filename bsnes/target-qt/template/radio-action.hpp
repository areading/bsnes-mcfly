#ifndef NALL_QT_RADIOACTION_HPP
#define NALL_QT_RADIOACTION_HPP

namespace nall {

class RadioAction : public QAction {
public:
  bool isChecked() const;
  void setChecked(bool);
  void toggleChecked();
  RadioAction(const QString&, QObject*);

protected:
  bool checked;
};

inline bool RadioAction::isChecked() const {
  return checked;
}

inline void RadioAction::setChecked(bool checked_) {
  checked = checked_;
  const uint8_t* icon = checked ? Resource::Widget::ItemRadioOn : Resource::Widget::ItemRadioOff;
  size_t size = checked ? sizeof(Resource::Widget::ItemRadioOn) : sizeof(Resource::Widget::ItemRadioOff);
  QPixmap pixmap;
  pixmap.loadFromData(icon, size, "PNG");
  setIcon(QIcon(pixmap));
}

inline void RadioAction::toggleChecked() {
  setChecked(!isChecked());
}

inline RadioAction::RadioAction(const QString& text, QObject* parent) : QAction(text, parent) {
  setChecked(false);
}

}

#endif
