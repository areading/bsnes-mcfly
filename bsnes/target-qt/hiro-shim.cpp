MessageDialog::MessageDialog(const string& text) {
  state.text = text;
}

auto MessageDialog::error() -> void {
  QMessageBox::critical(state.parent, "bsnes-mcfly", QString::fromUtf8(state.text));
}

auto MessageDialog::setParent(Window& parent) -> MessageDialog& {
  state.parent = &parent;
  return *this;
}
