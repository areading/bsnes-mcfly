AboutWindow::AboutWindow() {
  setObjectName("about-window");
  setWindowTitle("About");
  setGeometryString(&config->geometry.aboutWindow);

  #if defined(PROFILE_PERFORMANCE)
  setStyleSheet("background: #80c080");
  #else
  if(config->system.profile == "accuracy") {
    setStyleSheet("background: #c08080");
  } else {
    setStyleSheet("background: #8080c0");
  }
  #endif

  layout = new QVBoxLayout;
  layout->setSizeConstraint(QLayout::SetFixedSize);
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(Style::WidgetSpacing);
  setLayout(layout);

  logo = new Logo;
  logo->setFixedSize(600, 106);
  layout->addWidget(logo);

  info = new QLabel(QString::fromUtf8(string{
    "<table width='100%'>"
    "<tr><td><b>Homepage: </b><a href='", higan::Website, "'>", higan::Website, "</a></td></tr>"
    "<tr><td><b>Version: </b>", higan::Version, "</td></tr>"
    "<tr><td><b>Emulation Author: </b>", higan::Author, "</td></tr>"
    "<tr><td><b>GUI Author: </b>hex_usr</td></tr>"
    "<tr><td><b>Pipeline Performance Patches: </b>areading</td></tr>"
    "</table>"
  }));
  layout->addWidget(info);
}

void AboutWindow::Logo::paintEvent(QPaintEvent*) {
  QPainter painter(this);
  QPixmap pixmap;
  pixmap.loadFromData(Resource::Logo, sizeof(Resource::Logo), "PNG");
  painter.drawPixmap(0, 0, pixmap);
}
