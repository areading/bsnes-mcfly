#include "../qt.hpp"
WindowManager windowManager;

void WindowManager::updateFullscreenState() {
  uintptr handle = video.context();
  if(handle) video.clear();

  if(!isFullscreen) {
    config->video.context = &config->video.windowed;
    if(video.exclusive()) input.release();
    video.setExclusive(false);
    presentation->showNormal();
    presentation->menuBar->setVisible(true);
    presentation->statusBar->setVisible(true);
  } else {
    config->video.context = &config->video.fullscreen;
    presentation->showFullScreen();
    bool showBars = !config->video.fullscreenExclusive && !config->video.autoHideFullscreenMenu;
    presentation->menuBar->setVisible(showBars);
    presentation->statusBar->setVisible(showBars);
    video.setExclusive(config->video.fullscreenExclusive);
    if(video.exclusive()) input.acquire();
  }

  QApplication::processEvents();
  #if defined(PLATFORM_LINUX) || defined(PLATFORM_BSD)
  //Xlib requires time to propagate fullscreen state change message to window manager;
  //if window is resized before this occurs, canvas may not resize correctly
  usleep(50000);
  #endif

  //refresh options that are unique to each video context
  if(handle) {
    program->updateVideoShader();
    program->bindVideoFilter();
  }
  presentation->syncUi();
}

//if max exceeds x: x is set to max, and y is scaled down to keep proportion to x
void WindowManager::constrainSize(uint& x, uint& y, uint max) {
  if(x > max) {
    double scalar = (double)max / (double)x;
    y = (uint)((double)y * (double)scalar);
    x = max;
  }
}

void WindowManager::resizeMainWindow() {
  uint screenWidth, screenHeight;
  if(!isFullscreen) {
    screenWidth = QApplication::desktop()->availableGeometry(&*presentation).width();
    screenHeight = QApplication::desktop()->availableGeometry(&*presentation).height();
  } else {
    screenWidth = presentation->canvas->size().width();
    screenHeight = presentation->canvas->size().height();
  }

  uint region = config->video.context->region;
  uint multiplier = config->video.context->multiplier;
  const auto display = emulator->display();
  uint width = display.width * multiplier;
  uint height = (region == 0 ? 224 : display.height) * multiplier;

  if(config->video.context->correctAspectRatio) {
    if(region == 0) {
      width = (double)width * config->video.ntscAspectRatio + 0.5;  //NTSC adjust
    } else {
      width = (double)width * config->video.palAspectRatio  + 0.5;  //PAL adjust
    }
  }

  program->cropLeft = config->video.cropLeft;
  program->cropTop = config->video.cropTop;
  program->cropRight = config->video.cropRight;
  program->cropBottom = config->video.cropBottom;

  //ensure window size will not be larger than viewable desktop area
  constrainSize(height, width, screenHeight);
  constrainSize(width, height, screenWidth);

  if(!isFullscreen) {
    presentation->canvas->setMinimumSize(width, height);
  } else {
    if(multiplier == 6) {
      //Scale Max - Normal
      width = (double)width * (double)screenHeight / (double)height;
      height = screenHeight;
    }

    if(multiplier == 7) {
      //Scale Max - Wide
      width = screenWidth;
      height = screenHeight;
    }

    if(multiplier == 8) {
      //Scale Max - Wide Zoom
      //1. scale width and height proportionally until width of screen is completely filled
      //2. determine how much height goes out of screen by
      //3. cut half of the above value out of the visible input display region
      //this results in a 50% compromise between correct aspect ratio and fill mode;
      //while cropping out only 50% as much height as a fully proportional zoom would
      width = (double)width * (double)screenHeight / (double)height;
      height = screenHeight;
      uint widthDifference = screenWidth - width;
      uint adjustedHeight = (double)height * (double)(width + widthDifference) / (double)(width);
      uint heightDifference = adjustedHeight - height;
      width = screenWidth;
      program->cropLeft = 0;
      program->cropTop = 100.0 / (double)height * (heightDifference / 4);
      program->cropRight = 0;
      program->cropBottom = 100.0 / (double)height * (heightDifference / 4);
    }

    presentation->canvas->setFixedSize(width, height);
  }

  //workaround for Qt/Xlib bug:
  //if window resize occurs with cursor over it, Qt shows Qt::Size*DiagCursor;
  //so force it to show Qt::ArrowCursor, as expected
  presentation->setCursor(Qt::ArrowCursor);
  presentation->canvas->setCursor(Qt::ArrowCursor);

  //workaround for DirectSound(?) bug:
  //window resizing sometimes breaks audio sync, this call re-initializes it
  audio.setBlocking(config->audio.synchronize);
}

void WindowManager::toggleSynchronizeVideo() {
  presentation->settings_emulationSpeed_syncVideo->toggleChecked();
  config->video.synchronize = presentation->settings_emulationSpeed_syncVideo->isChecked();
  video.setBlocking(config->video.synchronize);
}

void WindowManager::toggleSynchronizeAudio() {
  presentation->settings_emulationSpeed_syncAudio->toggleChecked();
  config->audio.synchronize = presentation->settings_emulationSpeed_syncAudio->isChecked();
  audio.setBlocking(config->audio.synchronize);
}

void WindowManager::setNtscMode() {
  config->video.context->region = 0;
  resizeMainWindow();
  presentation->syncUi();
}

void WindowManager::setPalMode() {
  config->video.context->region = 1;
  resizeMainWindow();
  presentation->syncUi();
}

void WindowManager::toggleSmoothVideoOutput() {
  presentation->settings_smoothVideo->toggleChecked();
  config->video.context->blur = presentation->settings_smoothVideo->isChecked();
  program->updateVideoShader();
  presentation->syncUi();
}

void WindowManager::toggleAspectCorrection() {
  presentation->settings_videoMode_correctAspectRatio->toggleChecked();
  config->video.context->correctAspectRatio = presentation->settings_videoMode_correctAspectRatio->isChecked();
  resizeMainWindow();
}

void WindowManager::setScale(uint scale) {
  config->video.context->multiplier = scale;
  resizeMainWindow();
  presentation->syncUi();
}

void WindowManager::toggleFullscreen() {
  isFullscreen = !isFullscreen;
  updateFullscreenState();
  resizeMainWindow();
  presentation->syncUi();
}

void WindowManager::toggleMenubar() {
  if(video.exclusive()) return;
  presentation->menuBar->setVisible(!presentation->menuBar->isVisibleTo(&*presentation));
  resizeMainWindow();
}

void WindowManager::toggleStatusbar() {
  if(video.exclusive()) return;
  presentation->statusBar->setVisible(!presentation->statusBar->isVisibleTo(&*presentation));
  resizeMainWindow();
}
