#include "../qt.hpp"

Movie movie;

void Movie::chooseFile() {
  fileBrowser->onChange.reset();
  fileBrowser->onActivate = { &Movie::play, this };
  fileBrowser->onAccept = { &Movie::play, this };
  fileBrowser->setWindowTitle("Select Movie");

  string path;
  if(directory::exists(program->gamePath())) {
    path = {program->gamePath(), "bsnes-mcfly/"};
  } else {
    path = config->path.data;
  }

  fileBrowser->setPath(QString::fromUtf8(path));
  fileBrowser->setNameFilters("bsnes Movies (*.bsv)");
  fileBrowser->chooseFile();
}

void Movie::play(const string& filename) {
  if(!file::exists(filename)) return;  //ignore folders

  if(Movie::state != Inactive) stop();

  if(fp = file::open(filename, file::mode::read)) {
    if(fp.size() < 32) goto corrupt;

    uint signature = fp.readl(4);
    if(signature != 0x31565342) goto corrupt;  //BSV1

    string version = fp.reads(16).data<char>();
    if(version != higan::SerializerVersion) goto corrupt;

    uint size = fp.readl(4);
    auto data = new uint8_t[size];
    fp.read({data, size});
    serializer state(data, size);
    emulator->unserialize(state);
    delete[] data;

    Movie::state = Playback;
    presentation->syncUi();
    program->showMessage("Playback started.");

    return;
  }

corrupt:
  fp.close();
  program->showMessage("Movie file is invalid, playback cancelled.");
}

void Movie::record() {
  if(Movie::state != Inactive) {
    program->showMessage("Movie mode already active, recording cancelled.");
  } else {
    serializer state = emulator->serialize();

    program->showMessage("Recording started.");

    Movie::state = Record;
    presentation->syncUi();
    string filename = program->moviePath();
    directory::create(Location::path(filename));
    fp = file::open(filename, file::mode::write);

    uint signature = 0x31565342;  //BSV1
    char version[16] = {0};
    memory::copy<char>(&version, (const char*)higan::SerializerVersion, higan::SerializerVersion.size());

    fp.writel(signature, 4);
    fp.write({(const uint8_t*)version, 16});
    fp.writel(state.size(), 4);
    fp.write({state.data(), state.size()});
  }
}

void Movie::stop() {
  if(Movie::state != Inactive) {
    Movie::state = Inactive;
    presentation->syncUi();
    fp.close();
    program->showMessage("Recording / playback stopped.");
  }
}

int16_t Movie::read() {
  int16_t result = fp.readl(2);

  if(fp.end()) {
    Movie::state = Inactive;
    presentation->syncUi();
    fp.close();
    program->showMessage("Playback finished.");
  }

  return result;
}

void Movie::write(int16_t value) {
  fp.writel(value, 2);
}

Movie::Movie() {
  state = Inactive;
}
