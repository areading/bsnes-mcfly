InputGroup userInterfaceStates(InputCategory::UserInterface, "States");

namespace UserInterfaceStates {

struct Rewind : HotkeyInput {
  void pressed() {
    program->rewind();
  }

  Rewind() : HotkeyInput("Rewind", "1/Button/Backspace") {
    userInterfaceStates.attach(this);
  }
} rewind;

struct LoadActiveState : HotkeyInput {
  void pressed() {
    program->loadState(program->activeState, false);
  }

  LoadActiveState() : HotkeyInput("Load Active Quick State", "1/Button/F4") {
    userInterfaceStates.attach(this);
  }
} loadActiveState;

struct SaveActiveState : HotkeyInput {
  void pressed() {
    program->saveState(program->activeState, false);
  }

  SaveActiveState() : HotkeyInput("Save Active Quick State", "1/Button/F2") {
    userInterfaceStates.attach(this);
  }
} saveActiveState;

struct SelectActiveState : HotkeyInput {
  void pressed() {
    stateSelectWindow->show();
  }

  SelectActiveState() : HotkeyInput("Select Active Quick State", "1/Button/F3") {
    userInterfaceStates.attach(this);
  }
} selectActiveState;

struct DecrementAndLoadState : HotkeyInput {
  void pressed() {
    if(program->activeState-- == 1) program->activeState = 10;
    program->loadState(program->activeState, false);
  }

  DecrementAndLoadState() : HotkeyInput("Decrement and Load State") {
    userInterfaceStates.attach(this);
  }
} decrementAndLoadState;

struct SaveAndIncrementState : HotkeyInput {
  void pressed() {
    program->saveState(program->activeState, false);
    if(program->activeState++ == 10) program->activeState = 1;
  }

  SaveAndIncrementState() : HotkeyInput("Save and Increment State") {
    userInterfaceStates.attach(this);
  }
} saveAndIncrementState;

struct DecrementActiveState : HotkeyInput {
  void pressed() {
    if(program->activeState-- == 1) program->activeState = 10;
    program->showMessage({"Quick state ", program->activeState, " selected."});
  }

  DecrementActiveState() : HotkeyInput("Decrement Active Quick State Slot") {
    userInterfaceStates.attach(this);
  }
} decrementActiveState;

struct IncrementActiveState : HotkeyInput {
  void pressed() {
    if(program->activeState++ == 10) program->activeState = 1;
    program->showMessage({"Quick state ", program->activeState, " selected."});
  }

  IncrementActiveState() : HotkeyInput("Increment Active Quick State Slot") {
    userInterfaceStates.attach(this);
  }
} incrementActiveState;

struct LoadState1 : HotkeyInput {
  void pressed() {
    program->loadState(1, false);
  }

  LoadState1() : HotkeyInput("Load Quick State 1") {
    userInterfaceStates.attach(this);
  }
} loadState1;

struct LoadState2 : HotkeyInput {
  void pressed() {
    program->loadState(2, false);
  }

  LoadState2() : HotkeyInput("Load Quick State 2") {
    userInterfaceStates.attach(this);
  }
} loadState2;

struct LoadState3 : HotkeyInput {
  void pressed() {
    program->loadState(3, false);
  }

  LoadState3() : HotkeyInput("Load Quick State 3") {
    userInterfaceStates.attach(this);
  }
} loadState3;

struct SaveState1 : HotkeyInput {
  void pressed() {
    program->saveState(1, false);
  }

  SaveState1() : HotkeyInput("Save Quick State 1") {
    userInterfaceStates.attach(this);
  }
} saveState1;

struct SaveState2 : HotkeyInput {
  void pressed() {
    program->saveState(2, false);
  }

  SaveState2() : HotkeyInput("Save Quick State 2") {
    userInterfaceStates.attach(this);
  }
} saveState2;

struct SaveState3 : HotkeyInput {
  void pressed() {
    program->saveState(3, false);
  }

  SaveState3() : HotkeyInput("Save Quick State 3") {
    userInterfaceStates.attach(this);
  }
} saveState3;

}
