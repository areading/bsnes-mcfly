InputGroup userInterfaceVideoSettings(InputCategory::UserInterface, "Video Settings");

namespace UserInterfaceVideoSettings {

struct ToggleFullscreen : HotkeyInput {
  void pressed() {
    windowManager.toggleFullscreen();
  }

  ToggleFullscreen() : HotkeyInput("Toggle Fullscreen Mode", "Alt+1/Button/Return") {
    userInterfaceVideoSettings.attach(this);
  }
} toggleFullscreen;

struct SmoothVideoOutput : HotkeyInput {
  void pressed() {
    windowManager.toggleSmoothVideoOutput();
  }

  SmoothVideoOutput() : HotkeyInput("Smooth Video Output", "Shift+1/Button/S") {
    userInterfaceVideoSettings.attach(this);
  }
} smoothVideoOutput;

struct SetNtscMode : HotkeyInput {
  void pressed() {
    windowManager.setNtscMode();
  }

  SetNtscMode() : HotkeyInput("Set NTSC Mode", "Shift+1/Button/N") {
    userInterfaceVideoSettings.attach(this);
  }
} setNtscMode;

struct SetPalMode : HotkeyInput {
  void pressed() {
    windowManager.setPalMode();
  }

  SetPalMode() : HotkeyInput("Set PAL Mode", "Shift+1/Button/P") {
    userInterfaceVideoSettings.attach(this);
  }
} setPalMode;

struct AspectCorrection : HotkeyInput {
  void pressed() {
    windowManager.toggleAspectCorrection();
  }

  AspectCorrection() : HotkeyInput("Aspect Correction", "Shift+1/Button/A") {
    userInterfaceVideoSettings.attach(this);
  }
} aspectCorrection;

struct Scale1x : HotkeyInput {
  void pressed() {
    windowManager.setScale(1);
  }

  Scale1x() : HotkeyInput("Scale 1x", "Shift+1/Button/Num1") {
    userInterfaceVideoSettings.attach(this);
  }
} scale1x;

struct Scale2x : HotkeyInput {
  void pressed() {
    windowManager.setScale(2);
  }

  Scale2x() : HotkeyInput("Scale 2x", "Shift+1/Button/Num2") {
    userInterfaceVideoSettings.attach(this);
  }
} scale2x;

struct Scale3x : HotkeyInput {
  void pressed() {
    windowManager.setScale(3);
  }

  Scale3x() : HotkeyInput("Scale 3x", "Shift+1/Button/Num3") {
    userInterfaceVideoSettings.attach(this);
  }
} scale3x;

struct Scale4x : HotkeyInput {
  void pressed() {
    windowManager.setScale(4);
  }

  Scale4x() : HotkeyInput("Scale 4x", "Shift+1/Button/Num4") {
    userInterfaceVideoSettings.attach(this);
  }
} scale4x;

struct Scale5x : HotkeyInput {
  void pressed() {
    windowManager.setScale(5);
  }

  Scale5x() : HotkeyInput("Scale 5x", "Shift+1/Button/Num5") {
    userInterfaceVideoSettings.attach(this);
  }
} scale5x;

struct ScaleMaxNormal : HotkeyInput {
  void pressed() {
    if(windowManager.isFullscreen) windowManager.setScale(6);
  }

  ScaleMaxNormal() : HotkeyInput("Scale Max - Normal", "Shift+1/Button/Num6") {
    userInterfaceVideoSettings.attach(this);
  }
} scaleMaxNormal;

struct ScaleMaxFill : HotkeyInput {
  void pressed() {
    if(windowManager.isFullscreen) windowManager.setScale(7);
  }

  ScaleMaxFill() : HotkeyInput("Scale Max - Fill", "Shift+1/Button/Num7") {
    userInterfaceVideoSettings.attach(this);
  }
} scaleMaxFill;

struct ScaleMaxSmart : HotkeyInput {
  void pressed() {
    if(windowManager.isFullscreen) windowManager.setScale(8);
  }

  ScaleMaxSmart() : HotkeyInput("Scale Max - Smart", "Shift+1/Button/Num8") {
    userInterfaceVideoSettings.attach(this);
  }
} scaleMaxSmart;

}
