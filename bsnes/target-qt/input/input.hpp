extern HID::Null hidNull;

struct InputModifier { enum { None = 0, Shift = 1, Control = 2, Alt = 4, Super = 8 }; };
struct InputCategory { enum { Port1 = 0, Port2 = 1, UserInterface = 2, Hidden = 3 }; };

struct InputGroup;

struct AbstractInput {
  InputGroup* parent;
  string name;
  string defaultMapping;

  string mapping;
  uint modifier;
  uint scancode;
  int16_t state;
  int16_t previousState;
  int16_t cachedState;

  struct Input {
    shared_pointer<HID::Device> device;
    uint64_t id = 0;
    uint group = 0;
    uint input = 0;
    enum class Qualifier : uint { None, Lo, Hi } qualifier;
  } item;

  void bind();
  bool append(string mapping);
  virtual bool bind(shared_pointer<HID::Device> device, uint group, uint input, int16_t oldValue, int16_t newValue) { return false; };
  virtual void poll() {}
  void bindDefault();
  void unbind();
  virtual void cache();

  AbstractInput(const char*, const char* = "None");
};

struct DigitalInput : AbstractInput {
  using AbstractInput::bind;
  bool bind(shared_pointer<HID::Device> device, uint group, uint input, int16_t oldValue, int16_t newValue);
  void poll();

  bool isPressed() const;
  bool wasPressed() const;

  DigitalInput(const char*, const char* = "None");
};

struct RelativeInput : AbstractInput {
  using AbstractInput::bind;
  bool bind(shared_pointer<HID::Device> device, uint group, uint input, int16_t oldValue, int16_t newValue);
  void poll();

  RelativeInput(const char*, const char* = "None");
};

struct HotkeyInput : DigitalInput {
  void poll();
  virtual void pressed() {}
  virtual void released() {}

  HotkeyInput(const char*, const char* = "None");
};

struct InputGroup : public vector<AbstractInput*> {
  uint category;
  string name;
  Configuration::Node deviceConfig;

  void attach(AbstractInput*);
  void bind();
  void poll();
  void cache();
  void flushCache();
  virtual int16_t status(uint) const { return 0; }

  InputGroup(uint, const char*);
};

struct InputManager {
  struct Config : Configuration::Document {
    Configuration::Node port1;
    Configuration::Node port2;
    Configuration::Node userInterface;
  } config;

  vector<shared_pointer<HID::Device>> devices;
  struct Cache {
    HID::Group* keyboard;
    shared_pointer<HID::Device> mouse;

    uint escape;
    uint f4;
    bool disambiguateLR;
    uint shift[2];
    uint control[2];
    uint alt[2];
    uint super[2];
  } cache;
  uint modifier;
  vector<InputGroup*> inputList;
  InputGroup* port1 = nullptr;
  InputGroup* port2 = nullptr;

  string sanitize(string mapping, string concatenate) const;
  void onChange(shared_pointer<HID::Device> device, uint group, uint input, int16_t oldValue, int16_t newValue);
  void bind();
  void poll();
  int16_t status(uint, uint, uint);

  string modifierString() const;

  InputManager();
  ~InputManager();
};

extern InputManager* inputManager;

#include "controller.hpp"
#include "userinterface.hpp"
