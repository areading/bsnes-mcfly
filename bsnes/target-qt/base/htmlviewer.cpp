unique_pointer<HtmlViewerWindow> htmlViewerWindow;

HtmlViewerWindow::HtmlViewerWindow() {
  setObjectName("html-window");
  resize(560, 480);
  setGeometryString(&config->geometry.htmlViewerWindow);

  layout = new QVBoxLayout;
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(0);
  setLayout(layout);

  document = new QTextBrowser;
  layout->addWidget(document);
}

void HtmlViewerWindow::show(const char *title, const char *htmlData) {
  document->setHtml(QString::fromUtf8(htmlData));
  setWindowTitle(title);
  Window::show();
}
