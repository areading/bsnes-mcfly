unique_pointer<FileBrowser> fileBrowser;

FileBrowser::FileBrowser() {
  setObjectName("file-browser");
  resize(800, 480);
  setGeometryString(&config->geometry.fileBrowser);

  previewLayout = new QVBoxLayout;
  previewLayout->setAlignment(Qt::AlignTop);
  previewFrame->setLayout(previewLayout);

  previewInfo = new QLabel;
  previewLayout->addWidget(previewInfo);

  previewImage = new QWidget;
  previewImage->setFixedSize(256, 239);
  previewLayout->addWidget(previewImage);

  previewSpacer = new QWidget;
  previewSpacer->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
  previewLayout->addWidget(previewSpacer);

  previewApplyPatch = new QCheckBox("Apply Patch");
  previewApplyPatch->setVisible(false);
  previewApplyPatch->setChecked(config->file.applyPatches);
  previewLayout->addWidget(previewApplyPatch);

  connect(this, QOverload<const string&>::of(&FileDialog::changed), this, &FileBrowser::change);
  connect(this, QOverload<const string&>::of(&FileDialog::activated), this, &FileBrowser::activate);
  connect(this, QOverload<const string&>::of(&FileDialog::accepted), this, &FileBrowser::accept);
  connect(previewApplyPatch, &QCheckBox::stateChanged, this, &FileBrowser::toggleApplyPatch);
}

void FileBrowser::chooseFile() {
  if(config->diskBrowser.useCommonDialogs == true) {
    audio.clear();
    QString qfilename = QFileDialog::getOpenFileName(0,
      windowTitle(), fileSystemModel->rootPath(), "All Files (*)"
    );
    string filename = qfilename.toUtf8().constData();
    if(filename != "") onAccept(filename);
    return;
  }

  showLoad();
}

void FileBrowser::chooseFolder() {
  if(config->diskBrowser.useCommonDialogs == true) {
    audio.clear();
    QString qfilename = QFileDialog::getExistingDirectory(0,
      windowTitle(), QString::fromUtf8(config->path.current.folder),
      QFileDialog::ShowDirsOnly
    );
    string filename = qfilename.toUtf8().constData();
    if(filename != "") onAccept(filename);
    return;
  }

  previewFrame->hide();
  showFolder();
}

void FileBrowser::loadCartridge(CartridgeMode mode, int filterIndex) {
  cartridgeMode = mode;
  onChange = { &FileBrowser::onChangeCartridge, this };
  onActivate = { &FileBrowser::onAcceptCartridge, this };
  onAccept = { &FileBrowser::onAcceptCartridge, this };

  string defaultPath = config->path.rom;
  if(defaultPath == "") defaultPath = config->path.current.cartridge;

  vector<string> filters;

  if(reader.sfcExtensionList) {
    string filter;
    filter.append("SNES cartridges (*", reader.sfcExtensionList.merge(" *"));
    if(reader.compressionList) filter.append(" *", reader.compressionList.merge(" *"));
    filter.append(")");
    filters.append(filter);
  }

  if(reader.bsExtensionList) {
    string filter;
    filter.append("BS-X cartridges (*", reader.bsExtensionList.merge(" *"));
    if(reader.compressionList) filter.append(" *", reader.compressionList.merge(" *"));
    filter.append(")");
    filters.append(filter);
  }

  if(reader.stExtensionList) {
    string filter;
    filter.append("Sufami Turbo cartridges (*", reader.stExtensionList.merge(" *"));
    if(reader.compressionList) filter.append(" *", reader.compressionList.merge(" *"));
    filter.append(")");
    filters.append(filter);
  }

  #if defined(CORE_GB)
  if(reader.gbExtensionList) {
    string filter;
    filter.append("Game Boy cartridges (*", reader.gbExtensionList.merge(" *"));
    if(reader.compressionList) filter.append(" *", reader.compressionList.merge(" *"));
    filter.append(")");
    filters.append(filter);
  }
  #endif

  filters.append("All files (*)");

  if(config->diskBrowser.useCommonDialogs == true) {
    audio.clear();
    string filename = QFileDialog::getOpenFileName(0, windowTitle(),
      QString::fromUtf8(defaultPath), QString::fromUtf8(filters.merge(";;"))
    );
    if(filename != "") onAccept(filename);
    config->path.current.cartridge = Location::path(filename);
    return;
  }

  setPath(defaultPath);
  setNameFilters(QString::fromUtf8(filters.merge("\n")));
  previewFrame->show();
  filterBox->setCurrentIndex(filterIndex == -1 ? config->path.current.filter : filterIndex);
  showLoad();
}

void FileBrowser::loadShader() {
  onChange.reset();
  onActivate = {&FileBrowser::onAcceptShader, this};
  onAccept = {&FileBrowser::onAcceptShader, this};

  setPath(config->path.current.shader);
  setNameFilters({
    "Shaders (*.shader)\n"
    "All files (*)"
  });
  previewFrame->hide();
  filterBox->setCurrentIndex(0);
  showLoad();
}

void FileBrowser::change(const string& path) {
  if(onChange) onChange(path);
}

void FileBrowser::activate(const string& path) {
  if(onActivate) onActivate(path);
}

void FileBrowser::accept(const string& path) {
  if(onAccept) onAccept(path);
}

void FileBrowser::toggleApplyPatch() {
  config->file.applyPatches = previewApplyPatch->isChecked();
}

//

void FileBrowser::onChangeCartridge(const string& path) {
  //Qt directory paths don't end in slashes, so they need to be manually added.
  string filename;
  bool folder = directory::exists(path);
  if(folder) filename = {path, "/program.rom"};
  else filename = path;

  string info;
  string image, bps, ips, ups;
  if(folder) image = {path, "/screenshot.png"};
  if(!folder) image = program->path("Screenshots", filename, ".png");
  if(!folder) bps = program->path("Patches", filename, ".bps");
  if(!folder) ips = program->path("Patches", filename, ".ips");
  if(!folder) ups = program->path("Patches", filename, ".ups");

  if(file::exists(filename)) {
    string extension = Location::suffix(path);
    #if defined(PLATFORM_WINDOWS)
    extension.downcase();
    #endif

    string filter = filterBox->currentText().toUtf8().constData();

    bool isSFC = filter.beginsWith("SNES cartridges")         || reader.sfcExtensionList.find(extension);
    bool isBS  = filter.beginsWith("BS-X cartridges")         || reader.bsExtensionList.find(extension);
    bool isST  = filter.beginsWith("Sufami Turbo cartridges") || reader.stExtensionList.find(extension);
    #if defined(CORE_GB)
    bool isGB  = filter.beginsWith("Game Boy cartridges")     || reader.gbExtensionList.find(extension);
    #else
    bool isGB  = false;
    #endif
    bool isCompressed = (bool)reader.compressionList.find(extension);

    if(isSFC) {
      if(!isCompressed && cartridgeInformation(filename)) {
        info = {
          "<table>"
          "<tr><td><b>Title: </b></td><td>", cartinfo.name, "</td></tr>"
          "<tr><td><b>Region: </b></td><td>", cartinfo.region, "</td></tr>"
          "<tr><td><b>ROM: </b></td><td>", cartinfo.romSize * 8 / 1024 / 1024, "mbit</td></tr>"
          "<tr><td><b>RAM: </b></td><td>",
          cartinfo.ramSize ? string{cartinfo.ramSize * 8 / 1024, "kbit"} : string{"None"},
          "</td></tr>"
          "</table>"
        };
      }
    } else if(isGB || isBS || isST) {
      if(!isCompressed) {
        uint size = file::size(filename);
        info = {
          "<table>"
          "<tr><td><b>ROM: </b></td><td>", size * 8 / 1024 / 1024, "mbit</td></tr>"
          "</table>"
        };
      }
    }
  }

  if(!info) info = "<small><font color='#808080'>No preview available</font></small>";
  previewInfo->setText(QString::fromUtf8(info));
  //filenames can contain apostrophes.
  previewImage->setStyleSheet(QString::fromUtf8(string{
    "background: url(\"", image, "\") center left no-repeat;"
  }));
  previewApplyPatch->setVisible(file::exists(bps) || file::exists(ips) || file::exists(ups));
}

void FileBrowser::onAcceptCartridge(const string& path) {
  string filename;
  if(directory::exists(path)) {
    string ext = Location::suffix(path);
    if(ext == ".sfc"
    #if defined(CORE_GB)
    || ext == ".gb" || ext == ".gbc"
    #endif
    || ext == ".bs"
    || ext == ".st") {
      filename = {path, "/"};
    } else {
      return;
    }
  } else {
    filename = path;
  }

  close();
  config->path.current.cartridge = fileSystemModel->rootPath().toUtf8().constData();

  if(cartridgeMode == LoadDirect) {
    config->path.current.filter = filterBox->currentIndex();
    string filter = filterBox->currentText().toUtf8().constData();

    if(0);
    //filter detection
    else if(filter.beginsWith("SNES cartridges")) acceptSuperFamicom(filename);
    else if(filter.beginsWith("BS-X cartridges")) acceptBSMemory(filename);
    else if(filter.beginsWith("Sufami Turbo cartridges")) acceptSufamiTurbo(filename);
    #if defined(CORE_GB)
    else if(filter.beginsWith("Game Boy cartridges")) acceptSuperGameBoy(filename);
    #endif
    //fallback behavior
    else acceptHeuristic(filename);
  } else if(cartridgeMode == LoadBase) {
    loaderWindow->selectBaseCartridge(filename);
  } else if(cartridgeMode == LoadSlot1) {
    loaderWindow->selectSlot1Cartridge(filename);
  } else if(cartridgeMode == LoadSlot2) {
    loaderWindow->selectSlot2Cartridge(filename);
  }
}

void FileBrowser::onAcceptShader(const string& path) {
  string filename;
  if(directory::exists(path)) {
    if(Location::suffix(path) == ".shader") {
      filename = {path, "/"};
    } else {
      return;
    }
  }

  close();
  config->video.shader = {path, path.endsWith("/") ? "" : "/"};
  config->path.current.shader = fileSystemModel->rootPath().toUtf8().constData();
  settingsWindow->videoSettings->syncUi();
  program->updateVideoShader();
}

void FileBrowser::acceptSuperFamicom(const string& filename) {
  program->gameQueue = {filename};
  program->loadDirect();
  program->gameQueue.reset();
}

void FileBrowser::acceptBSMemory(const string& filename) {
  if(config->path.bios.bsx == "") {
    loaderWindow->loadBsxCartridge("", filename);
  } else {
    program->gameQueue = {config->path.bios.bsx, filename};
    program->loadDirect();
    program->gameQueue.reset();
  }
}

void FileBrowser::acceptSufamiTurbo(const string& filename) {
  if(config->path.bios.sufamiTurbo == "") {
    loaderWindow->loadSufamiTurboCartridge("", filename, "");
  } else {
    program->gameQueue = {config->path.bios.sufamiTurbo, filename, ""};
    program->loadDirect();
    program->gameQueue.reset();
  }
}

void FileBrowser::acceptSuperGameBoy(const string& filename) {
  #if defined(CORE_GB)
  if(config->path.bios.superGameBoy == "") {
    loaderWindow->loadSuperGameBoyCartridge("", filename);
  } else {
    program->gameQueue = {config->path.bios.superGameBoy, filename};
    program->loadDirect();
    program->gameQueue.reset();
  }
  #endif
}

void FileBrowser::acceptHeuristic(const string& filename) {
  program->gameQueue = {filename};
  program->load();
}

bool FileBrowser::cartridgeInformation(string filename) {
  if(auto fp = file::open(filename, file::mode::read)) {
    uint header = 0;

    uint16_t complement, checksum, reset;

    for(uint headerAddress : {0x40ffb0, 0x4101b0, 0x7fb0, 0x81b0, 0xffb0, 0x101b0}) {
      if(fp.size() < headerAddress + 0x50) continue;
      fp.seek(headerAddress + 0x2c);
      complement = fp.readl(2);
      checksum = fp.readl(2);
      fp.seek(headerAddress + 0x4c);
      reset = fp.readl(2);

      if(complement + checksum != 0xffff || reset < 0x8000) continue;
      header = headerAddress;
      break;
    }

    fp.seek(header + 0x10);
    char name[22];
    fp.read({(uint8_t*)name, 21});
    name[21] = 0;
    cartinfo.name = decodeShiftJIS(name, 21);

    fp.seek(header + 0x29);
    uint8_t region = fp.read();
    cartinfo.region = (region <= 1 || region >= 13) ? "NTSC" : "PAL";

    cartinfo.romSize = fp.size() & ~0x7fff;

    fp.seek(header + 0x28);
    cartinfo.ramSize = fp.readl(1);
    if(cartinfo.ramSize) cartinfo.ramSize = 1024 << (cartinfo.ramSize & 7);

    fp.close();
    return true;
  }
  return false;
}

string FileBrowser::decodeShiftJIS(const char* data, uint length) {
  string label;

  for(uint n = 0; n < 0x15; n++) {
    uint8_t x = data[n];
    uint8_t y = n == 0x14 ? 0 : data[n + 1];
    char ascii[] = "?";

    //null terminator (padding)
    if(x == 0x00 || x == 0xff);

    //ASCII
    else if(x >= 0x20 && x <= 0x7e) { ascii[0] = x; label.append(ascii); }

    //Shift-JIS (half-width katakana)
    else if(x == 0xa1) label.append("。");
    else if(x == 0xa2) label.append("「");
    else if(x == 0xa3) label.append("」");
    else if(x == 0xa4) label.append("、");
    else if(x == 0xa5) label.append("・");
    else if(x == 0xa6) label.append("ヲ");
    else if(x == 0xa7) label.append("ァ");
    else if(x == 0xa8) label.append("ィ");
    else if(x == 0xa9) label.append("ゥ");
    else if(x == 0xaa) label.append("ェ");
    else if(x == 0xab) label.append("ォ");
    else if(x == 0xac) label.append("ャ");
    else if(x == 0xad) label.append("ュ");
    else if(x == 0xae) label.append("ョ");
    else if(x == 0xaf) label.append("ッ");
    else if(x == 0xb0) label.append("ー");

    else if(x == 0xb1) label.append(                 "ア");
    else if(x == 0xb2) label.append(                 "イ");
    else if(x == 0xb3) label.append(y == 0xde ? "ヴ" : "ウ");
    else if(x == 0xb4) label.append(                 "エ");
    else if(x == 0xb5) label.append(                 "オ");

    else if(x == 0xb6) label.append(y == 0xde ? "ガ" : "カ");
    else if(x == 0xb7) label.append(y == 0xde ? "ギ" : "キ");
    else if(x == 0xb8) label.append(y == 0xde ? "グ" : "ク");
    else if(x == 0xb9) label.append(y == 0xde ? "ゲ" : "ケ");
    else if(x == 0xba) label.append(y == 0xde ? "ゴ" : "コ");

    else if(x == 0xbb) label.append(y == 0xde ? "ザ" : "サ");
    else if(x == 0xbc) label.append(y == 0xde ? "ジ" : "シ");
    else if(x == 0xbd) label.append(y == 0xde ? "ズ" : "ス");
    else if(x == 0xbe) label.append(y == 0xde ? "ゼ" : "セ");
    else if(x == 0xbf) label.append(y == 0xde ? "ゾ" : "ソ");

    else if(x == 0xc0) label.append(y == 0xde ? "ダ" : "タ");
    else if(x == 0xc1) label.append(y == 0xde ? "ヂ" : "チ");
    else if(x == 0xc2) label.append(y == 0xde ? "ヅ" : "ツ");
    else if(x == 0xc3) label.append(y == 0xde ? "デ" : "テ");
    else if(x == 0xc4) label.append(y == 0xde ? "ド" : "ト");

    else if(x == 0xc5) label.append("ナ");
    else if(x == 0xc6) label.append("ニ");
    else if(x == 0xc7) label.append("ヌ");
    else if(x == 0xc8) label.append("ネ");
    else if(x == 0xc9) label.append("ノ");

    else if(x == 0xca) label.append(y == 0xdf ? "パ" : y == 0xde ? "バ" : "ハ");
    else if(x == 0xcb) label.append(y == 0xdf ? "ピ" : y == 0xde ? "ビ" : "ヒ");
    else if(x == 0xcc) label.append(y == 0xdf ? "プ" : y == 0xde ? "ブ" : "フ");
    else if(x == 0xcd) label.append(y == 0xdf ? "ペ" : y == 0xde ? "ベ" : "ヘ");
    else if(x == 0xce) label.append(y == 0xdf ? "ポ" : y == 0xde ? "ボ" : "ホ");

    else if(x == 0xcf) label.append("マ");
    else if(x == 0xd0) label.append("ミ");
    else if(x == 0xd1) label.append("ム");
    else if(x == 0xd2) label.append("メ");
    else if(x == 0xd3) label.append("モ");

    else if(x == 0xd4) label.append("ヤ");
    else if(x == 0xd5) label.append("ユ");
    else if(x == 0xd6) label.append("ヨ");

    else if(x == 0xd7) label.append("ラ");
    else if(x == 0xd8) label.append("リ");
    else if(x == 0xd9) label.append("ル");
    else if(x == 0xda) label.append("レ");
    else if(x == 0xdb) label.append("ロ");

    else if(x == 0xdc) label.append("ワ");
    else if(x == 0xdd) label.append("ン");

    else if(x == 0xde) label.append("\xef\xbe\x9e");  //dakuten
    else if(x == 0xdf) label.append("\xef\xbe\x9f");  //handakuten

    //unknown
    else label.append("?");

    //(han)dakuten skip
    if(y == 0xde && x == 0xb3) n++;
    if(y == 0xde && x >= 0xb6 && x <= 0xc4) n++;
    if(y == 0xde && x >= 0xca && x <= 0xce) n++;
    if(y == 0xdf && x >= 0xca && y <= 0xce) n++;
  }

  return label;
}
