namespace Configuration {

struct Node {
  string name;
  string desc;
  enum class Type : uint { Null, Boolean, Integer, Natural, Double, String } type = Type::Null;
  void* data = nullptr;
  vector<Node> children;

  explicit operator bool() const { return data; }

  auto get() const -> string {
    switch(type) {
    case Type::Boolean: return {*(bool*)data};
    case Type::Integer: return {*(int*)data};
    case Type::Natural: return {*(uint*)data};
    case Type::Double: return {*(double*)data};
    case Type::String: return {*(string*)data};
    }
    return "";
  }

  auto set(const string& value) -> void {
    switch(type) {
    case Type::Boolean: *(bool*)data = (value != "false"); break;
    case Type::Integer: *(int*)data = toInteger(value); break;
    case Type::Natural: *(uint*)data = toNatural(value); break;
    case Type::Double: *(double*)data = toReal(value); break;
    case Type::String: *(string*)data = value; break;
    }
  }

  auto assign() { type = Type::Null; data = nullptr; }
  auto assign(bool& bind) { type = Type::Boolean; data = (void*)&bind; }
  auto assign(int& bind) { type = Type::Integer; data = (void*)&bind; }
  auto assign(uint& bind) { type = Type::Natural; data = (void*)&bind; }
  auto assign(double& bind) { type = Type::Double; data = (void*)&bind; }
  auto assign(string& bind) { type = Type::String; data = (void*)&bind; }
  auto assign(const Node& node) { operator=(node); }

  template<typename T> auto append(T& data, const string& name, const string& desc = "") -> void {
    Node node;
    node.assign(data);
    node.name = name;
    node.desc = desc;
    children.append(node);
  }

  auto find(const string& path) -> maybe<Node&> {
    auto p = path.split("/");
    auto name = p.takeLeft();
    for(auto& child : children) {
      if(child.name == name) {
        if(p.size() == 0) return child;
        return child.find(p.merge("/"));
      }
    }
    return nothing;
  }

  auto load(Markup::Node path) -> void {
    for(auto& child : children) {
      if(auto leaf = path[child.name]) {
        if(child) child.set(leaf.text());
        child.load(leaf);
      }
    }
  }

  auto save(file_buffer& fp, uint depth = 0) -> void {
    for(auto& child : children) {
      if(child.desc) {
        for(auto n : range(depth)) fp.print("  ");
        fp.print("//", child.desc, "\n");
      }
      for(auto n : range(depth)) fp.print("  ");
      fp.print(child.name);
      if(child) fp.print(": ", child.get());
      fp.print("\n");
      child.save(fp, depth + 1);
      if(depth == 0) fp.print("\n");
    }
  }
};

struct Document : Node {
  auto load(const string& filename) -> bool {
    if(!file::exists(filename)) return false;
    auto document = BML::unserialize(string::read(filename));
    Node::load(document);
    return true;
  }

  auto save(const string& filename) -> bool {
    if(auto fp = file::open(filename, file::mode::write)) {
      Node::save(fp);
      return true;
    }
    return false;
  }
};

}

struct ConfigurationSettings : Configuration::Document {
  struct Game : Configuration::Node {
    Configuration::Node recent_;
    string recent[10];
  } game;

  struct System : Configuration::Node {
    string profile;
    bool crashedOnLastRun;
    uint speed;
    uint speeds[5];
    bool autoSaveMemory;
    bool rewindEnabled;
    uint rewindGranularity;
    uint rewindHistory;
    bool cheatEnabled;
    bool stateManagerDoubleClick;
  } system;

  struct File : Configuration::Node {
    bool applyPatches;
  } file;

  struct DiskBrowser : Configuration::Node {
    bool useCommonDialogs;
    bool showPanel;
  } diskBrowser;

  struct Path : Configuration::Node {
    struct BIOS : Configuration::Node {
      string bsx;
      string sufamiTurbo;
      string superGameBoy;
    } bios;

    string rom, save, state, patch, cheat, data;

    struct Current : Configuration::Node {
      string folder, shader, cartridge;
      uint filter;  //current active filter for "Load Cartridge"
    } current;
  } path;

  struct Video : Configuration::Node {
    string driver;
    bool synchronize;
    bool flush;
    bool exclusive;
    string format;
    string filter;
    string shader;

    double ntscAspectRatio;
    double palAspectRatio;

    uint cropLeft;
    uint cropTop;
    uint cropRight;
    uint cropBottom;

    int saturation;
    int gamma;
    int luminance;
    int scanline;

    bool autoHideFullscreenMenu;
    bool fullscreenExclusive;

    struct Context : Configuration::Node {
      bool correctAspectRatio;
      uint multiplier, region;
      bool blur;
    } *context, windowed, fullscreen;
  } video;

  struct Audio : Configuration::Node {
    string driver;
    string device;
    uint frequency;
    uint latency;
    bool exclusive;
    bool synchronize;
    bool mute;
    uint volume;
  } audio;

  struct Input : Configuration::Node {
    string driver;

    uint port1;
    uint port2;
    enum policy_t { FocusPolicyPauseEmulation, FocusPolicyIgnoreInput, FocusPolicyAllowInput };
    uint focusPolicy;
    bool allowInvalidInput;
    bool modifierEnable;
  } input;

  struct Geometry : Configuration::Node {
    string presentation;
    string loaderWindow;
    string stateSelectWindow;
    string htmlViewerWindow;
    string aboutWindow;
    string fileBrowser;
    string folderCreator;
    string settingsWindow;
    string toolsWindow;
    string cheatDatabase;
  } geometry;

  struct SuperFamicom : Configuration::Node {
    string region;
    struct SuperFX : Configuration::Node {
      uint speed;
    } superfx;
  } sfc;

  void load();
  void save();
  ConfigurationSettings();
};

extern ConfigurationSettings* config;
