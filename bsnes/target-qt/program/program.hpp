struct SuperFamicomCartridge;

struct ScanlineFilter {
  ScanlineFilter();
  ~ScanlineFilter();

  auto size(uint&, uint&) -> void;
  auto render(const uint32*, uint, uint, uint) -> void;
  auto setIntensity(uint) -> void;

  bool enabled;

  uint32* data;

private:
  enum Shortcut : uint {
    None,
    Zero,
    ShiftRight1,
    ShiftRight2,
  } shortcut;

  unsigned* adjust;
};

struct Filter : public library {
  Filter();
  ~Filter();
  auto render(const uint32*, uint, uint, uint) -> void;

  function<void (uint&, uint&)> dl_size;
  function<void (uint32_t*, uint, const uint32_t*, uint, uint, uint)> dl_render;

  uint32* data;
  uint pitch;
  uint width;
  uint height;
};

extern ScanlineFilter scanlineFilter;
extern Filter filter;

class Program : public QObject, public higan::Platform {
public:
  Program();
  ~Program();

  class App : public QApplication {
  public:
    #if defined(PLATFORM_WINDOWS)
    bool winEventFilter(MSG* msg, long* result);
    #endif

    App(int& argc, char** argv) : QApplication(argc, argv) {}
  } *app;

  QTimer* timer;

  bool terminate;  //set to true to terminate main() loop and exit emulator
  bool power;
  bool pause;
  bool autopause;
  bool frameAdvance;

  clock_t clockTime;
  clock_t autosaveTime;
  clock_t screensaverTime;

  string styleSheetFilename;

  int main(int& argc, char** argv);
  void init();
  void run();

  //platform.cpp
  auto open(uint id, string name, vfs::file::mode mode, bool required) -> vfs::shared::file override;
  auto load(uint id, string name, string type, vector<string> options = {}) -> higan::Platform::Load override;
  auto videoFrame(const uint32* data, uint pitch, uint width, uint height) -> void override;
  auto audioFrame(const float* samples, uint channels) -> void override;
  auto inputPoll(uint port, uint device, uint input) -> int16 override;
  auto inputRumble(uint port, uint device, uint input, bool enable) -> void override;
  auto dipSettings(Markup::Node node) -> uint override;
  auto notify(string text) -> void override;

  //game.cpp
  auto load() -> void;
  auto loadDirect() -> void;
  auto loadFile(string location) -> vector<uint8_t>;
  auto loadSuperFamicom(string location) -> bool;
  auto loadGameBoy(string location) -> bool;
  auto loadBSMemory(string location) -> bool;
  auto loadSufamiTurboA(string location) -> bool;
  auto loadSufamiTurboB(string location) -> bool;
  auto save() -> void;
  auto reset() -> void;
  auto unload() -> void;
  auto verified() const -> bool;

  //game-pak.cpp
  auto openPakSuperFamicom(string name, vfs::file::mode mode) -> vfs::shared::file;
  auto openPakGameBoy(string name, vfs::file::mode mode) -> vfs::shared::file;
  auto openPakBSMemory(string name, vfs::file::mode mode) -> vfs::shared::file;
  auto openPakSufamiTurboA(string name, vfs::file::mode mode) -> vfs::shared::file;
  auto openPakSufamiTurboB(string name, vfs::file::mode mode) -> vfs::shared::file;

  //game-rom.cpp
  auto openRomSuperFamicom(string name, vfs::file::mode mode) -> vfs::shared::file;
  auto openRomGameBoy(string name, vfs::file::mode mode) -> vfs::shared::file;
  auto openRomBSMemory(string name, vfs::file::mode mode) -> vfs::shared::file;
  auto openRomSufamiTurboA(string name, vfs::file::mode mode) -> vfs::shared::file;
  auto openRomSufamiTurboB(string name, vfs::file::mode mode) -> vfs::shared::file;

  //paths.cpp
  auto path(string type, string location, string extension = "") -> string;
  auto gamePath() -> string;
  auto cheatPath(bool gameGenie) -> string;
  auto statePath() -> string;
  auto statePath(uint id, bool managed = false) -> string;
  auto screenshotPath() -> string;
  auto moviePath() -> string;

  //state.cpp
  struct State {
    string name;
    uint64_t date;
    static const uint Signature;
  };
  /*
  auto availableStates(string type) -> vector<State>;
  auto hasState(string filename) -> bool;
  auto loadStateData(string filename) -> vector<uint8_t>;
  auto bsnes_loadState(string filename) -> bool;
  auto bsnes_saveState(string filename) -> bool;
  auto saveUndoState() -> bool;
  auto saveRedoState() -> bool;
  auto removeState(string filename) -> bool;
  auto renameState(string from, string to) -> bool;
  */

  auto loadState(uint slot, bool managed = false) -> bool;
  auto saveState(uint slot, bool managed = false) -> bool;
  auto updateRewind() -> void;
  auto resetRewindHistory() -> void;
  auto resetRewindHistory(uint historysize, uint granularity=0u) -> void;
  auto rewind() -> bool;

  //video.cpp
  auto updateVideoFormat() -> void;
  auto updateVideoPalette() -> void;
  auto bindVideoFilter() -> void;
  auto updateVideoShader() -> void;

  //audio.cpp
  auto updateAudioDriver() -> void;
  auto updateAudioEffects() -> void;

  //utility.cpp
  auto powerOn() -> void;
  auto powerOff() -> void;
  auto powerCycle() -> void;
  auto softReset() -> void;
  auto connectDevices() -> void;
  auto showMessage(const string& text) -> void;
  auto updateStatusText() -> void;
  auto focused() -> bool;
  auto captureScreenshot(uint32_t*, uint, uint, uint) -> void;

  //patch.cpp
  auto appliedPatch() const -> bool;
  auto applyPatchIPS(vector<uint8_t>& data, string location) -> bool;
  auto applyPatchUPS(vector<uint8_t>& input, string location) -> bool;
  auto applyPatchBPS(vector<uint8_t>& input, string location) -> bool;

  //hacks.cpp
  auto hackCompatibility() -> void;
  auto hackPatchMemory(vector<uint8_t>& data) -> void;
  auto hackOverclockSuperFX() -> void;

  struct Game {
    explicit operator bool() const { return (bool)location; }

    string location;
    string manifest;
    Markup::Node document;
    boolean patched;
    boolean verified;
  };

  struct SuperFamicom : Game {
    string title;
    vector<uint8_t> program;
    vector<uint8_t> data;
    vector<uint8_t> expansion;
    vector<uint8_t> firmware;
  } superFamicom;

  struct GameBoy : Game {
    vector<uint8_t> program;
  } gameBoy;

  struct BSMemory : Game {
    vector<uint8_t> program;
  } bsMemory;

  struct SufamiTurbo : Game {
    vector<uint8_t> program;
  } sufamiTurboA, sufamiTurboB;

  vector<string> gameQueue;  //for command-line and drag-and-drop loading

  struct Screenshot {
    const uint32* data = nullptr;
    uint pitch = 0;
    uint width = 0;
    uint height = 0;
  } screenshot;

  Markup::Node sfcDatabase;
  Markup::Node gbDatabase;
  Markup::Node bsDatabase;
  Markup::Node stDatabase;
  bool firmwareAppended;

  uint cropLeft;
  uint cropTop;
  uint cropRight;
  uint cropBottom;

  uint activeState;
  struct Rewind {
    ~Rewind() { destroy(); }
    serializer* history = nullptr;
    uint size = 0;
    uint granularity = 0;
    uint index = 0;
    uint count = 0;
    uint frameCounter = 0;
    auto operator[](uint index) -> serializer& { return history[index]; }
    inline void destroy();
  } rewindHistory;

  bool saveScreenshot;
  bool framesUpdated;
  uint framesExecuted;
  uint64_t coreTickFilter;
  uint coreTickFilterLen;

  enum Profile : uint {
    Accuracy,
    Compatibility,
    Performance
  };
  Profile activeProfile;  //profile that was active at game load
};


inline void Program::Rewind::destroy() {
  if(history) {
    delete[] history;
    history = nullptr;
    size = count = 0;
  }
}

extern Program* program;
