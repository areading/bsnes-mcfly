auto Program::updateAudioDriver() -> void {
  if(!audio) return;
  audio.clear();
  audio.setExclusive(config->audio.exclusive);
  audio.setFrequency(config->audio.frequency);
  audio.setLatency(config->audio.latency);

  float scale = config->system.speeds[config->system.speed] / 100.f;
  higan::audio.setFrequency((float)config->audio.frequency / scale);
}

auto Program::updateAudioEffects() -> void {
  float volume = config->audio.mute ? 0.f : config->audio.volume * 0.01f;
  higan::audio.setVolume(volume);

  higan::audio.setBalance(0.f);
}
