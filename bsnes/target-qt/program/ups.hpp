struct ups {
  enum class result : uint {
    unknown,
    success,
    patch_unwritable,
    patch_invalid,
    source_invalid,
    target_invalid,
    target_too_small,
    patch_checksum_invalid,
    source_checksum_invalid,
    target_checksum_invalid,
  };

  function<void (uint offset, uint length)> progress;

  auto apply(
    const uint8_t* patchdata, uint patchlength,
    const uint8_t* sourcedata, uint sourcelength,
    uint8_t* targetdata, uint& targetlength
  ) -> result {
    patch_data = (uint8_t*)patchdata, source_data = (uint8_t*)sourcedata, target_data = targetdata;
    patch_length = patchlength, source_length = sourcelength, target_length = targetlength;
    patch_offset = source_offset = target_offset = 0;
    patch_checksum.reset();
    source_checksum.reset();
    target_checksum.reset();

    if(patch_length < 18) return result::patch_invalid;
    if(patch_read() != 'U') return result::patch_invalid;
    if(patch_read() != 'P') return result::patch_invalid;
    if(patch_read() != 'S') return result::patch_invalid;
    if(patch_read() != '1') return result::patch_invalid;

    uint source_read_length = decode();
    uint target_read_length = decode();

    if(source_length != source_read_length && source_length != target_read_length) return result::source_invalid;
    targetlength = (source_length == source_read_length ? target_read_length : source_read_length);
    if(target_length < targetlength) return result::target_too_small;
    target_length = targetlength;

    while(patch_offset < patch_length - 12) {
      uint length = decode();
      while(length--) target_write(source_read());
      while(true) {
        uint8_t patch_xor = patch_read();
        target_write(patch_xor ^ source_read());
        if(patch_xor == 0) break;
      }
    }
    while(source_offset < source_length) target_write(source_read());
    while(target_offset < target_length) target_write(source_read());

    uint32_t patch_read_checksum = 0, source_read_checksum = 0, target_read_checksum = 0;
    for(uint i = 0; i < 4; i++) source_read_checksum |= patch_read() << (i * 8);
    for(uint i = 0; i < 4; i++) target_read_checksum |= patch_read() << (i * 8);
    uint32_t patch_result_checksum = patch_checksum.value();
    for(uint i = 0; i < 4; i++) patch_read_checksum  |= patch_read() << (i * 8);

    if(patch_result_checksum != patch_read_checksum) return result::patch_invalid;
    if(source_checksum.value() == source_read_checksum && source_length == source_read_length) {
      if(target_checksum.value() == target_read_checksum && target_length == target_read_length) return result::success;
      return result::target_invalid;
    } else if(source_checksum.value() == target_read_checksum && source_length == target_read_length) {
      if(target_checksum.value() == source_read_checksum && target_length == source_read_length) return result::success;
      return result::target_invalid;
    } else {
      return result::source_invalid;
    }
  }

private:
  uint8_t* patch_data = nullptr;
  uint8_t* source_data = nullptr;
  uint8_t* target_data = nullptr;
  uint patch_length, source_length, target_length;
  uint patch_offset, source_offset, target_offset;
  Hash::CRC32 patch_checksum, source_checksum, target_checksum;

  auto patch_read() -> uint8_t {
    if(patch_offset < patch_length) {
      uint8_t n = patch_data[patch_offset++];
      patch_checksum.input(n);
      return n;
    }
    return 0x00;
  }

  auto source_read() -> uint8_t {
    if(source_offset < source_length) {
      uint8_t n = source_data[source_offset++];
      source_checksum.input(n);
      return n;
    }
    return 0x00;
  }

  auto target_write(uint8_t n) -> void {
    if(target_offset < target_length) {
      target_data[target_offset] = n;
      target_checksum.input(n);
    }
    if(((target_offset++ & 255) == 0) && progress) {
      progress(target_offset, source_length > target_length ? source_length : target_length);
    }
  }

  auto decode() -> uint64_t {
    uint64_t offset = 0, shift = 1;
    while(true) {
      uint8_t x = patch_read();
      offset += (x & 0x7f) * shift;
      if(x & 0x80) break;
      shift <<= 7;
      offset += shift;
    }
    return offset;
  }
};
