auto Program::hackCompatibility() -> void {
  bool fastPPU = config->system.profile != "accuracy";
  bool fastDSP = config->system.profile != "accuracy";
  bool coprocessorsDelayedSync = config->system.profile != "accuracy";

  auto title = superFamicom.title;
  if(title == "AIR STRIKE PATROL" || title == "DESERT FIGHTER") fastPPU = false;
  if(title == "KOUSHIEN_2") fastDSP = false;
  if(title == "RENDERING RANGER R2") fastDSP = false;

  emulator->setOption("hack/ppu/fast", fastPPU);
  emulator->setOption("hack/dsp/fast", fastDSP);
  emulator->setOption("hack/coprocessor/fast", coprocessorsDelayedSync);
}

auto Program::hackPatchMemory(vector<uint8_t>& data) -> void {
  auto title = superFamicom.title;

  if(title == "Satellaview BS-X" && data.size() >= 0x100000) {
    //BS-X: Sore wa Namae o Nusumareta Machi no Monogatari (JPN) (1.1)
    //disable limited play check for BS Memory flash cartridges
    //benefit: allow locked out BS Memory flash games to play without manual header patching
    //detriment: BS Memory ROM cartridges will cause the game to hang in the load menu
    if(data[0x4a9b] == 0x10) data[0x4a9b] = 0x80;
    if(data[0x4d6d] == 0x10) data[0x4d6d] = 0x80;
    if(data[0x4ded] == 0x10) data[0x4ded] = 0x80;
    if(data[0x4e9a] == 0x10) data[0x4e9a] = 0x80;
  }
}

auto Program::hackOverclockSuperFX() -> void {
  //todo: implement a better way of detecting SuperFX games
  //todo: apply multiplier changes on reset, not just on game load?
  double multiplier = config->sfc.superfx.speed / 100.0;
  if(multiplier == 1.0) return;

  auto document = BML::unserialize(superFamicom.manifest);
  string board = document["game/board"].text();
  if(board.beginsWith("SNSP-")) board.replace("SNSP-", "SHVC-", 1L);
  if(board.beginsWith("MAXI-")) board.replace("MAXI-", "SHVC-", 1L);
  if(board.beginsWith("MJSC-")) board.replace("MJSC-", "SHVC-", 1L);
  if(board.beginsWith("EA-"  )) board.replace("EA-",   "SHVC-", 1L);
  if(board.beginsWith("WEI-" )) board.replace("WEI-",  "SHVC-", 1L);
  if(board.find("SUPERFX")(~0) == 0
  || board.match("SHVC""-""?C??""-""??")  || !board.match("SHVC""-""?C????""-""??")
  || board.match("SHVC""-""?CA??""-""??") || !board.match("SHVC""-""?CA????""-""??")
  || board.match("SHVC""-""?CB??""-""??") || !board.match("SHVC""-""?CB????""-""??")
  ) {
    if(auto oscillator = document["game/board/oscillator"]) {
      //GSU-1, GSU-2 have a 21440000hz oscillator
      if(oscillator["frequency"].text() == "21440000") {
        oscillator["frequency"].setValue(uint(21440000 * multiplier));
        superFamicom.manifest = BML::serialize(document);
      }
    } else {
      //MARIO CHIP 1 uses CPU oscillator; force it to use its own crystal to overclock it
      document("game/board/oscillator/frequency").setValue(uint(21440000 * multiplier));
      superFamicom.manifest = BML::serialize(document);
    }
  }
}
