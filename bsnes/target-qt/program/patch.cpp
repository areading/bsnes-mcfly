#include <target-bsnes/program/patch.cpp>
#include "ups.hpp"

auto Program::applyPatchUPS(vector<uint8_t>& input, string location) -> bool {
  vector<uint8_t> patch;

  if(location.endsWith("/")) {
    patch = file::read({location, "patch.ups"});
  } else if(location.iendsWith(".zip")) {
    Decode::ZIP archive;
    if(archive.open(location)) {
      for(auto& file : archive.file) {
        if(file.name.iendsWith(".ups")) {
          patch = archive.extract(file);
          break;
        }
      }
    }
    if(!patch) patch = file::read(path("Patches", location, ".ups"));
  } else {
    patch = file::read(path("Patches", location, ".ups"));
  }
  if(!patch) return false;

  vector<uint8_t> output;
  uint targetSize = 0;
  ups patcher;
  if(patcher.apply(patch.data(), patch.size(), input.data(), input.size(), nullptr, targetSize) == ups::result::target_too_small) {
    output.resize(targetSize);
    if(patcher.apply(patch.data(), patch.size(), input.data(), input.size(), output.data(), targetSize) == ups::result::success) {
      input = output;
      return true;
    }
  }

  MessageDialog({
    "Error: patch checksum failure.\n\n",
    "Please ensure you are using the correct (headerless) ROM for this patch.\n",
    "Unlike with BPS, UPS will not automatically trim copier headers."
  }).setParent(*presentation).error();
  return false;
}
