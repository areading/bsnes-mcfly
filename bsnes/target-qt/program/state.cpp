//#define loadState bsnes_loadState
//#define saveState bsnes_saveState
//#include <target-bsnes/program/states.cpp>
//#undef loadState
//#undef saveState

const uint Program::State::Signature = 0x5a22'0000;

auto Program::loadState(uint slot, bool managed) -> bool {
  if(!emulator->loaded() || !power || movie.state != Movie::Inactive) {
    return showMessage("Cannot load state."), false;
  }
  string location = statePath(slot, managed);
  auto memory = file::read(location);
  if(memory.size() == 0) return showMessage({"State ", slot, " not found."}), false;
  if(memory.size() < sizeof(uint)) return showMessage({"State ", slot, " is corrupted."}), false;
  vector<uint8_t> saveState;

  std::remove_const<decltype(Program::State::Signature)>::type filesig;
  filesig = memory::readl<sizeof(uint),decltype(filesig)>(memory.data());

  if(activeProfile == Profile::Accuracy) {
    //higan expects the save state to be raw serializer data.
    saveState = memory;
  } else if(filesig == Program::State::Signature) {
    //bsnes expects the save state to be RLE-compressed.
    saveState = Decode::RLE<1>({memory.data() + 3 * sizeof(uint), memory.size() - 3 * sizeof(uint)});
  }

  if(saveState.size() == 0) return showMessage({"State ", slot, " is corrupted."}), false;

  serializer s(saveState.data(), (uint)saveState.size());
  if(emulator->unserialize(s) == false) return showMessage({"Failed to load state ", slot, "."}), false;
  return showMessage({"Loaded state ", slot, "."}), true;
}

auto Program::saveState(uint slot, bool managed) -> bool {
  if(!emulator->loaded() || !power || movie.state != Movie::Inactive) {
    return showMessage("Cannot save state."), false;
  }
  string location = statePath(slot, managed);
  serializer s = emulator->serialize();
  if(!s.size()) return showMessage({"Failed to save state ", slot, "."}), false;
  vector<uint8_t> saveState;

  if(activeProfile == Profile::Accuracy) {
    //higan expects the save state to be raw serializer data.
    saveState.resize(s.size());
    memory::copy<uint8_t>(saveState.data(), s.data(), s.size());
  } else {
    saveState.resize(3 * sizeof(uint));
    //bsnes expects the save state to be RLE-compressed with a preview screenshot.
    auto serializerRLE = Encode::RLE<1>({s.data(), s.size()});

    vector<uint8_t> previewRLE;
    //this can be null if a state is captured before the first frame of video output after power/reset
    if(screenshot.data) {
      image preview;
      preview.copy(screenshot.data, screenshot.pitch, screenshot.width, screenshot.height);
      if(preview.width() != 256 || preview.height() != 240) preview.scale(256, 240, true);
      preview.transform(0, 15, 0x8000, 0x7c00, 0x03e0, 0x001f);
      previewRLE = Encode::RLE<2>({preview.data(), preview.size()});
    }

    memory::writel<sizeof(uint)>(saveState.data() + 0 * sizeof(uint), State::Signature);
    memory::writel<sizeof(uint)>(saveState.data() + 1 * sizeof(uint), serializerRLE.size());
    memory::writel<sizeof(uint)>(saveState.data() + 2 * sizeof(uint), previewRLE.size());
    saveState.append(serializerRLE);
    saveState.append(previewRLE);
  }

  if(gamePath().endsWith("/")) directory::create(Location::path(location));
  if(!file::write(location, {saveState.data(), saveState.size()})) {
    return showMessage({"Failed to save state ", slot, "."}), false;
  }
  return showMessage({"Saved state ", slot, "."}), true;
}

auto Program::updateRewind() -> void {
  if(!emulator->loaded() || !power || movie.state != Movie::Inactive) return;
  if(!rewindHistory.size) return;

  //if a full second has passed, automatically capture state
  if(++rewindHistory.frameCounter >= rewindHistory.granularity) {
    rewindHistory.frameCounter = 0;
    rewindHistory.index = (rewindHistory.index + 1) % rewindHistory.size;
    rewindHistory.count = min(rewindHistory.count + 1, rewindHistory.size);
    rewindHistory[rewindHistory.index] = emulator->serialize();
  }
}

auto Program::resetRewindHistory() -> void {
  if(rewindHistory.size != config->system.rewindHistory) {
    resetRewindHistory(config->system.rewindHistory, config->system.rewindGranularity);
    return;
  }
  if(!config->system.rewindEnabled)
    return;

  for(uint i : range(rewindHistory.size)) rewindHistory[i] = {};
  rewindHistory.index = 0;
  rewindHistory.count = 0;
  rewindHistory.frameCounter = 0;
}

auto Program::resetRewindHistory(uint historysize, uint granularity) -> void {
  if(rewindHistory.size != historysize)
    rewindHistory.destroy();

  if(!config->system.rewindEnabled || historysize == 0u)
    return;
  if(granularity == 0u)
    granularity = (SFC::system.region() != SFC::System::Region::NTSC ? 50u : 60u);

  if(!rewindHistory.size)
    rewindHistory.history = new serializer[rewindHistory.size = historysize];

  rewindHistory.granularity = granularity;
  rewindHistory.index = 0;
  rewindHistory.count = 0;
  rewindHistory.frameCounter = 0;
}

auto Program::rewind() -> bool {
  if(!emulator->loaded() || !power || movie.state != Movie::Inactive) return false;
  if(!rewindHistory.size) return false;

  if(rewindHistory.count == 0) return false;
  serializer state(rewindHistory[rewindHistory.index].data(), rewindHistory[rewindHistory.index].size());
  bool result = emulator->unserialize(state);
  //add rewindHistory.size to prevent underflow
  rewindHistory.index = (rewindHistory.index + rewindHistory.size - 1) % rewindHistory.size;
  rewindHistory.count--;
  return true;
}
