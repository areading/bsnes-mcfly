#include "../qt.hpp"
#include "platform.cpp"
#include "filter.cpp"
#include "game.cpp"
#include <target-bsnes/program/game-pak.cpp>
#include <target-bsnes/program/game-rom.cpp>
#include "paths.cpp"
#include "state.cpp"
#include "video.cpp"
#include "audio.cpp"
#include "utility.cpp"
#include "patch.cpp"
#include "hacks.cpp"

#if defined(PLATFORM_LINUX) || defined(PLATFORM_BSD)
  #include "../platform/platform_xorg.cpp"
  const char Style::Monospace[64] = "Liberation Mono";
#elif defined(PLATFORM_MACOS)
  #include "../platform/platform_macos.cpp"
  const char Style::Monospace[64] = "Courier New";
#elif defined(PLATFORM_WINDOWS)
  #include "../platform/platform_windows.cpp"
  const char Style::Monospace[64] = "Lucida Console";
#else
  #error "unsupported platform"
#endif

const char defaultStylesheet[] =
  "#backdrop {"
  "  background: #000000;"
  "}\n";

Program* program = nullptr;

Program::Program() : timer(0) {
  terminate    = false;
  power        = false;
  pause        = false;
  autopause    = false;
  frameAdvance = false;

  clockTime       = clock();
  autosaveTime    = 0;
  screensaverTime = 0;

  cropLeft = 0;
  cropTop = 0;
  cropRight = 0;
  cropBottom = 0;

  activeState = 1;

  saveScreenshot = false;
}

Program::~Program() {
  fileBrowser.reset();
  archiveReader.reset();
  loaderWindow.reset();
  stateSelectWindow.reset();
  htmlViewerWindow.reset();
  toolsWindow.reset();
  cheatDatabase.reset();
  settingsWindow.reset();
  aboutWindow.reset();
  presentation.reset();

  delete timer;
  delete inputManager;
  delete config;

  //deleting (QApplication)app will segfault the application upon exit
  //delete app;
}

int Program::main(int& argc, char** argv) {
  app = new App(argc, argv);
  #if !defined(PLATFORM_WINDOWS)
  {
    QPixmap iconPixmap;
    iconPixmap.loadFromData(Resource::Icon, sizeof(Resource::Icon), "PNG");
    app->setWindowIcon(QIcon(iconPixmap));
  }
  #else
  //Windows port uses 256x256 icon from resource file
  CoInitialize(0);
  utf8_arguments(argc, argv);
  #endif

  app->setAttribute(Qt::AA_ForceRasterWidgets);
  app->setAttribute(Qt::AA_CompressHighFrequencyEvents, false);

  vector<string> arguments;
  for(auto n : range(argc)) {
    string argument = argv[n];

    //normalize directory and file path arguments
    if(directory::exists(argument)) argument.transform("\\", "/").trimRight("/").append("/");
    else if(file::exists(argument)) argument.transform("\\", "/").trimRight("/");

    arguments.append(argument);
  }

  styleSheetFilename = locate("style.qss");

  if(string customStylesheet = string::read(styleSheetFilename)) {
    app->setStyleSheet((const char*)customStylesheet);
  } else {
    app->setStyleSheet(defaultStylesheet);
  }

  higan::platform = this;

  if(auto properties = string::read(locate("properties.bml"))) {
    emulator->properties().unserialize(properties);
  }
  if(auto options = string::read(locate("options.bml"))) {
    emulator->options().unserialize(options);
  }

  config = new ConfigurationSettings;
  new InputManager;

  init();

  resetRewindHistory();

  arguments.takeLeft();
  for(auto& argument : arguments) {
    if(argument == "--fullscreen") {
      windowManager.toggleFullscreen();
    } else {
      if(directory::exists(argument) && !argument.endsWith("/")) argument.append("/");
      gameQueue.append(argument);
    }
  }
  if(gameQueue) load();

  timer = new QTimer(this);
  connect(timer, &QTimer::timeout, this, &Program::run);
  timer->start(0);
  app->exec();

  return 0;
}

void Program::init() {
  if(config->system.crashedOnLastRun == true) {
    //emulator crashed on last run, disable all drivers
    QMessageBox::warning(0, "bsnes-mcfly Crash Notification", QString::fromUtf8(string{
    "<p><b>Warning:</b><br>bsnes-mcfly crashed while attempting to initialize device "
    "drivers the last time it was run.</p>"
    "<p>To prevent this from occurring again, all drivers have been disabled. Please "
    "go to Settings->Configuration->Advanced and choose new driver settings, and then "
    "restart the emulator for the changes to take effect. <i>Video, audio and input "
    "will not work until you do this!</i></p>"
    "<p><b>Settings that caused failure on last run:</b><br>"
    "Video driver: ", config->video.driver, "<br>"
    "Audio driver: ", config->audio.driver, "<br>"
    "Input driver: ", config->input.driver, "<br></p>"
    }));

    config->video.driver = "None";
    config->audio.driver = "None";
    config->input.driver = "None";
  }

  if(config->video.driver == "") config->video.driver = video.safestDriver();
  if(config->audio.driver == "") config->audio.driver = audio.safestDriver();
  if(config->input.driver == "") config->input.driver = input.safestDriver();

  presentation = new Presentation;
  loaderWindow = new LoaderWindow;
  htmlViewerWindow = new HtmlViewerWindow;
  aboutWindow = new AboutWindow;
  fileBrowser = new FileBrowser;
  stateSelectWindow = new StateSelectWindow;

  //if emulator crashes while initializing drivers, next run will disable them all.
  //this will allow user to choose different driver settings.
  config->system.crashedOnLastRun = true;
  config->save();

  video.create(config->video.driver);
  audio.create(config->audio.driver);
  input.create(config->input.driver);

  //window must be onscreen and visible before initializing video interface
  updateStatusText();
  windowManager.resizeMainWindow();
  windowManager.updateFullscreenState();
  QApplication::processEvents();

  video.setContext((uintptr_t)presentation->canvas->winId());
//video.set("QWidget", (QWidget*)presentation->canvas);
  video.setBlocking(config->video.synchronize);
  video.setFlush(config->video.flush);
  video.setExclusive(config->video.exclusive);
  if(!video.ready()) {
    QMessageBox::warning(0, "bsnes-mcfly", QString::fromUtf8(string{
      "<p><b>Warning:</b> ", config->video.driver, " video driver failed to initialize. "
      "Video driver has been disabled.</p>"
      "<p>Please go to Settings->Configuration->Advanced and choose a different driver, and "
      "then restart the emulator for the changes to take effect.</p>"
    }));
    video.create("None");
  }

  //video.onUpdate([&](uint width, uint height) {
  //  if(!emulator->loaded()) presentation->clearViewport();
  //});

  audio.setExclusive(false);
  audio.setContext((uintptr_t)presentation->canvas->winId());
  audio.setDevice(config->audio.device);
  audio.setFrequency(config->audio.frequency);
  audio.setLatency(config->audio.latency);
  audio.setChannels(2);
  audio.setExclusive(config->audio.exclusive);
  audio.setBlocking(config->audio.synchronize);
  if(!audio.ready()) {
    QMessageBox::warning(0, "bsnes-mcfly", QString::fromUtf8(string{
      "<p><b>Warning:</b> ", config->audio.driver, " audio driver failed to initialize. "
      "Audio driver has been disabled.</p>"
      "<p>Please go to Settings->Configuration->Advanced and choose a different driver, and "
      "then restart the emulator for the changes to take effect.</p>"
    }));
    audio.create("None");
  }

  input.setContext((uintptr_t)presentation->canvas->winId());
  input.onChange({&InputManager::onChange, inputManager});
  if(!input.ready()) {
    QMessageBox::warning(0, "bsnes-mcfly", QString::fromUtf8(string{
      "<p><b>Warning:</b> ", config->input.driver, " input driver failed to initialize. "
      "Input driver has been disabled.</p>"
      "<p>Please go to Settings->Configuration->Advanced and choose a different driver, and "
      "then restart the emulator for the changes to take effect.</p>"
    }));
    input.create("None");
  }

  settingsWindow = new SettingsWindow;
  toolsWindow = new ToolsWindow;

  //didn't crash, note this in the config file now in case a different kind of crash occurs later
  config->system.crashedOnLastRun = false;
  config->save();

  //hide settings for incompatible drivers
  settingsWindow->videoSettings->synchronizeDriverSettings();
  settingsWindow->audioSettings->synchronizeDriverSettings();

  windowManager.resizeMainWindow();
  video.setBlocking(config->video.synchronize);
  video.setFlush(config->video.flush);
  audio.setBlocking(config->audio.synchronize);
  updateVideoFormat();
  updateVideoShader();
  bindVideoFilter();
  updateAudioDriver();
  updateAudioEffects();
  connectDevices();
}

void Program::run() {
  clock_t currentTime;

  QApplication::processEvents();

  if(terminate) {
    if(windowManager.isFullscreen) windowManager.toggleFullscreen();
    if(auto options = emulator->options().serialize()) {
      file::write(locate("options.bml"), options);
    }
    if(auto properties = emulator->properties().serialize()) {
      file::write(locate("properties.bml"), properties);
    }
    unload();
    config->save();
    video.reset();
    audio.reset();
    input.reset();
    timer->stop();
    app->quit();
    return;
  }

  if(config->input.focusPolicy == ConfigurationSettings::Input::FocusPolicyPauseEmulation) {
    bool active = focused();
    if(!autopause && !active) {
      autopause = true;
      audio.clear();
    } else if(autopause && active) {
      autopause = false;
    }
  } else {
    autopause = false;
  }

  updateStatusText();
  inputManager->poll();

  currentTime = clock();

  if(emulator->loaded() && !pause && !autopause) {
    if(!video.finished())
      return;

    {
      uint64 tickStart, tickEnd, usDelta;

      tickStart = chrono::nanosecond();
      emulator->run();
      tickEnd   = chrono::nanosecond();
      usDelta   = (tickEnd - tickStart + 500)/1000;

      coreTickFilter  = usDelta + coreTickFilterLen * coreTickFilter;
      coreTickFilter /= ++coreTickFilterLen;
    }

    {
      autosaveTime += currentTime - clockTime;
      screensaverTime += currentTime - clockTime;

      if(autosaveTime >= CLOCKS_PER_SEC * 60) {
        //auto-save RAM once per minute in case of emulator crash
        autosaveTime = 0;
        if(config->system.autoSaveMemory == true) emulator->save();
      }

      if(screensaverTime >= CLOCKS_PER_SEC * 30) {
        //supress screen saver every 30 seconds so it will not trigger during gameplay
        screensaverTime = 0;
        supressScreenSaver();
      }
    }
  }
  else
    usleep(20 * 1000);

  clockTime = currentTime;
}
