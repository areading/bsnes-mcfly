#include <icarus/heuristics/heuristics.hpp>
#include <icarus/heuristics/heuristics.cpp>
#include <icarus/heuristics/super-famicom.cpp>
#if defined(CORE_GB)
#include <icarus/heuristics/game-boy.cpp>
#endif
#include <icarus/heuristics/bs-memory.cpp>
#include <icarus/heuristics/sufami-turbo.cpp>

//Heuristically determine whether the cartridge being loaded is a slot cartridge
//and which base cartridge is needed
auto Program::load() -> void {
  if(!gameQueue) return;

  string type;
  if(directory::exists(gameQueue.left())) {
    type = Location::suffix(gameQueue.left()).trimLeft(".");
  } else if(file::exists(gameQueue.left())) {
    auto rom = reader.load(gameQueue.left(), type);
    if(!rom) gameQueue.reset();
  }
  #if defined(CORE_GB)
  if(type == "gb") gameQueue.insert(0, config->path.bios.superGameBoy);
  #endif
  if(type == "bs") gameQueue.insert(0, config->path.bios.bsx);
  if(type == "st") gameQueue.insert(0, config->path.bios.sufamiTurbo);

  loadDirect();

  gameQueue.reset();
}

//load menu cartridge selected or command-line load
auto Program::loadDirect() -> void {
  unload();

  if(!emulator->load()) return;
  #if !defined(PROFILE_PERFORMANCE)
  activeProfile = config->system.profile == "accuracy" ? Profile::Accuracy : Profile::Compatibility;
  #else
  activeProfile = Profile::Performance;
  #endif
  updateAudioDriver();
  updateAudioEffects();
  connectDevices();

  emulator->power();
  power        = true;
  pause        = false;
  frameAdvance = false;

  string title = {emulator->titles().merge(" + "), " - bsnes-mcfly v", higan::Version};
  presentation->setWindowTitle(QString::fromUtf8(title));
  presentation->syncUi();

  toolsWindow->cheatEditor->loadCheats();
  toolsWindow->cheatFinder->synchronize();
  toolsWindow->stateManager->reload();
  toolsWindow->manifestViewer->doRefresh();

  string locations = superFamicom.location;
  #if defined(CORE_GB)
  if(auto& location = gameBoy.location) locations.append("|", location);
  #endif
  if(auto& location = bsMemory.location) locations.append("|", location);
  if(auto& location = sufamiTurboA.location) locations.append("|", location);
  if(auto& location = sufamiTurboB.location) locations.append("|", location);
  presentation->addRecentGame(locations);
}

auto Program::loadFile(string location) -> vector<uint8_t> {
  string type;
  return reader.load(location, type);
}

auto Program::loadSuperFamicom(string location) -> bool {
  if(!sfcDatabase) {
    sfcDatabase = BML::unserialize(Resource::Database::SuperFamicom);
  }

  string manifest;
  vector<uint8_t> rom;

  if(location.endsWith("/")) {
    manifest = file::read({location, "manifest.bml"});
    rom.append(file::read({location, "program.rom"}));
    rom.append(file::read({location, "data.rom"}));
    rom.append(file::read({location, "expansion.rom"}));
    for(auto filename : directory::files(location, "*.boot.rom"   )) rom.append(file::read({location, filename}));
    for(auto filename : directory::files(location, "*.program.rom")) rom.append(file::read({location, filename}));
    for(auto filename : directory::files(location, "*.data.rom"   )) rom.append(file::read({location, filename}));
  } else {
    manifest = file::read({Location::notsuffix(location), ".bml"});
    rom = loadFile(location);
  }
  if(rom.size() < 0x8000) return false;

  //assume ROM and IPS/UPS agree on whether a copier header is present.
  //UPS does have a CRC32, but bsnes v073 originally applied the patch before
  //removing the copier header.
  superFamicom.patched = applyPatchIPS(rom, location) || applyPatchUPS(rom, location);
  if((rom.size() & 0x7fff) == 512) {
    //remove copier header
    memory::move(&rom[0], &rom[512], rom.size() - 512);
    rom.resize(rom.size() - 512);
  }
  //assume BPS is made against a ROM without a copier header
  if(!superFamicom.patched) superFamicom.patched = applyPatchBPS(rom, location);
  auto heuristics = Heuristics::SuperFamicom(rom, location);
  auto sha256 = Hash::SHA256(rom).digest();
  if(auto document = sfcDatabase) {
    if(auto game = document[{"game(sha256=", sha256, ")"}]) {
      manifest = BML::serialize(game);
      superFamicom.verified = true;
    }
  }
  superFamicom.title = heuristics.title();
  superFamicom.manifest = manifest ? manifest : heuristics.manifest();
  hackCompatibility();
  hackPatchMemory(rom);
  hackOverclockSuperFX();
  superFamicom.document = BML::unserialize(superFamicom.manifest);
  superFamicom.location = location;

  uint offset = 0;
  if(auto size = heuristics.programRomSize()) {
    superFamicom.program.resize(size);
    memory::copy(&superFamicom.program[0], &rom[offset], size);
    offset += size;
  }
  if(auto size = heuristics.dataRomSize()) {
    superFamicom.data.resize(size);
    memory::copy(&superFamicom.data[0], &rom[offset], size);
    offset += size;
  }
  if(auto size = heuristics.expansionRomSize()) {
    superFamicom.expansion.resize(size);
    memory::copy(&superFamicom.expansion[0], &rom[offset], size);
    offset += size;
  }
  if(auto size = heuristics.firmwareRomSize()) {
    superFamicom.firmware.resize(size);
    memory::copy(&superFamicom.firmware[0], &rom[offset], size);
    offset += size;
  }

  return true;
}

auto Program::loadGameBoy(string location) -> bool {
  #if defined(CORE_GB)
  string manifest;
  vector<uint8_t> rom;

  if(location.endsWith("/")) {
    manifest = file::read({location, "manifest.bml"});
    rom.append(file::read({location, "program.rom"}));
  } else {
    manifest = file::read({Location::notsuffix(location), ".bml"});
    rom = loadFile(location);
  }
  if(rom.size() < 0x4000) return false;

  gameBoy.patched = applyPatchIPS(rom, location) || applyPatchUPS(rom, location) || applyPatchBPS(rom, location);
  auto heuristics = Heuristics::GameBoy(rom, location);
  auto sha256 = Hash::SHA256(rom).digest();
  /*
  if(auto document = BML::unserialize(string::read(locate("database/Game Boy.bml")))) {
    if(auto game = document[{"game(sha256=", sha256, ")"}]) {
      manifest = BML::serialize(game);
      gameBoy.verified = true;
    }
  }
  */
  gameBoy.manifest = manifest ? manifest : heuristics.manifest();
  gameBoy.document = BML::unserialize(gameBoy.manifest);
  gameBoy.location = location;

  gameBoy.program = rom;
  return true;
  #else
  return false;
  #endif
}

auto Program::loadBSMemory(string location) -> bool {
  if(!bsDatabase) {
    bsDatabase = BML::unserialize(Resource::Database::BSMemory);
  }

  string manifest;
  vector<uint8_t> rom;

  if(location.endsWith("/")) {
    manifest = file::read({location, "manifest.bml"});
    rom.append(file::read({location, "program.rom"}));
    rom.append(file::read({location, "program.flash"}));
  } else {
    manifest = file::read({Location::notsuffix(location), ".bml"});
    rom = loadFile(location);
  }
  if(rom.size() < 0x8000) return false;

  bsMemory.patched = applyPatchIPS(rom, location) || applyPatchUPS(rom, location) || applyPatchBPS(rom, location);
  auto heuristics = Heuristics::BSMemory(rom, location);
  auto sha256 = Hash::SHA256(rom).digest();
  if(auto document = bsDatabase) {
    if(auto game = document[{"game(sha256=", sha256, ")"}]) {
      manifest = BML::serialize(game);
      bsMemory.verified = true;
    }
  }
  bsMemory.manifest = manifest ? manifest : heuristics.manifest();
  bsMemory.document = BML::unserialize(bsMemory.manifest);
  bsMemory.location = location;

  bsMemory.program = rom;
  return true;
}

auto Program::loadSufamiTurboA(string location) -> bool {
  if(!stDatabase) {
    stDatabase = BML::unserialize(Resource::Database::SufamiTurbo);
  }

  string manifest;
  vector<uint8_t> rom;

  if(location.endsWith("/")) {
    manifest = file::read({location, "manifest.bml"});
    rom.append(file::read({location, "program.rom"}));
  } else {
    manifest = file::read({Location::notsuffix(location), ".bml"});
    rom = loadFile(location);
  }
  if(rom.size() < 0x20000) return false;

  sufamiTurboA.patched = applyPatchIPS(rom, location) || applyPatchUPS(rom, location) || applyPatchBPS(rom, location);
  auto heuristics = Heuristics::SufamiTurbo(rom, location);
  auto sha256 = Hash::SHA256(rom).digest();
  if(auto document = stDatabase) {
    if(auto game = document[{"game(sha256=", sha256, ")"}]) {
      manifest = BML::serialize(game);
      sufamiTurboA.verified = true;
    }
  }
  sufamiTurboA.manifest = manifest ? manifest : heuristics.manifest();
  sufamiTurboA.document = BML::unserialize(sufamiTurboA.manifest);
  sufamiTurboA.location = location;

  sufamiTurboA.program = rom;
  return true;
}

auto Program::loadSufamiTurboB(string location) -> bool {
  if(!stDatabase) {
    stDatabase = BML::unserialize(Resource::Database::SufamiTurbo);
  }

  string manifest;
  vector<uint8_t> rom;

  if(location.endsWith("/")) {
    manifest = file::read({location, "manifest.bml"});
    rom.append(file::read({location, "program.rom"}));
  } else {
    manifest = file::read({Location::notsuffix(location), ".bml"});
    rom = loadFile(location);
  }
  if(rom.size() < 0x20000) return false;

  sufamiTurboB.patched = applyPatchIPS(rom, location) || applyPatchUPS(rom, location) || applyPatchBPS(rom, location);
  auto heuristics = Heuristics::SufamiTurbo(rom, location);
  auto sha256 = Hash::SHA256(rom).digest();
  if(auto document = stDatabase) {
    if(auto game = document[{"game(sha256=", sha256, ")"}]) {
      manifest = BML::serialize(game);
      sufamiTurboB.verified = true;
    }
  }
  sufamiTurboB.manifest = manifest ? manifest : heuristics.manifest();
  sufamiTurboB.document = BML::unserialize(sufamiTurboB.manifest);
  sufamiTurboB.location = location;

  sufamiTurboB.program = rom;
  return true;
}

auto Program::unload() -> void {
  if(!emulator->loaded()) return;

  video.clear();
  audio.clear();
  fileBrowser->close();  //avoid edge case oddities (eg movie playback window still open from previous game)

  toolsWindow->cheatEditor->saveCheats();
  toolsWindow->cheatFinder->synchronize();
  toolsWindow->stateManager->reload();
  resetRewindHistory();  //do not allow rewinding past a destructive system action
  movie.stop();  //movies cannot continue to record after destructive system actions

  emulator->unload();
  superFamicom = {};
  gameBoy = {};
  bsMemory = {};
  sufamiTurboA = {};
  sufamiTurboB = {};
  power = false;
  pause = true;

  presentation->setWindowTitle(QString::fromUtf8(string{"bsnes-mcfly v", higan::Version}));
  toolsWindow->cheatEditor->reset();
  toolsWindow->manifestViewer->doRefresh();
}
