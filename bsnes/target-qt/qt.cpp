#include "qt.hpp"
Video video;
Audio audio;
Input input;
unique_pointer<higan::Interface> emulator;

#include "file-dialog.moc"
#include "hiro-shim.cpp"

string locate(string name) {
  string location = {Path::program(), name};
  if(inode::exists(location)) return location;

  directory::create({Path::userData(), "bsnes-mcfly/"});
  return {Path::userData(), "bsnes-mcfly/", name};
}

int main(int argc, char** argv) {
  program = new Program;
  emulator = new higan::SuperFamicom::SuperFamicomInterface;
  int result = program->main(argc, argv);
  delete program;
  emulator.reset();
  return result;
}
