PathSettingWidget::PathSettingWidget(string& pathValue_, const char* labelText, const char* pathDefaultLabel_, const char* pathBrowseLabel_) : pathValue(pathValue_) {
  pathDefaultLabel = pathDefaultLabel_;
  pathBrowseLabel = pathBrowseLabel_;

  layout = new QVBoxLayout;
  layout->setMargin(0);
  layout->setSpacing(0);
  setLayout(layout);

  label = new QLabel(labelText);
  layout->addWidget(label);

  controlLayout = new QHBoxLayout;
  controlLayout->setSpacing(Style::WidgetSpacing);
  layout->addLayout(controlLayout);

  path = new QLineEdit;
  path->setReadOnly(true);
  controlLayout->addWidget(path);

  pathSelect = new QPushButton("Select ...");
  controlLayout->addWidget(pathSelect);

  pathDefault = new QPushButton("Default");
  controlLayout->addWidget(pathDefault);

  connect(pathSelect, &QPushButton::released, this, &PathSettingWidget::selectPath);
  connect(pathDefault, &QPushButton::released, this, &PathSettingWidget::defaultPath);
  updatePath();
}

void PathSettingWidget::acceptPath(const string &newPath) {
  fileBrowser->close();
  pathValue = {newPath, "/"};
  config->path.current.folder = Location::path(pathValue);
  updatePath();
}

void PathSettingWidget::updatePath() {
  if(pathValue == "") {
    path->setStyleSheet("color: #808080");
    path->setText(QString::fromUtf8(pathDefaultLabel));
  } else {
    path->setStyleSheet("color: #000000");
    path->setText(QString::fromUtf8(pathValue));
  }
}

void PathSettingWidget::selectPath() {
  fileBrowser->onChange.reset();
  fileBrowser->onActivate.reset();
  fileBrowser->onAccept = { &PathSettingWidget::acceptPath, this };
  fileBrowser->setWindowTitle(QString::fromUtf8(pathBrowseLabel));
  fileBrowser->setPath(config->path.current.folder);
  fileBrowser->chooseFolder();
}

void PathSettingWidget::defaultPath() {
  pathValue = "";
  updatePath();
}

PathSettings::PathSettings() {
  layout = new QVBoxLayout;
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(Style::WidgetSpacing);
  layout->setAlignment(Qt::AlignTop);
  setLayout(layout);

  gamePath  = new PathSettingWidget(config->path.rom,   "Games:",         "Remember last path",  "Default Game Path");
  savePath  = new PathSettingWidget(config->path.save,  "Save RAM:",      "Same as loaded game", "Default Save RAM Path");
  statePath = new PathSettingWidget(config->path.state, "Save states:",   "Same as loaded game", "Default Save State Path");
  patchPath = new PathSettingWidget(config->path.patch, "Patches:",   "Same as loaded game", "Default Patch Path");
  cheatPath = new PathSettingWidget(config->path.cheat, "Cheat codes:",   "Same as loaded game", "Default Cheat Code Path");
  dataPath  = new PathSettingWidget(config->path.data,  "Exported data:", "Same as loaded game", "Default Exported Data Path");

  layout->addWidget(gamePath);
  layout->addWidget(savePath);
  layout->addWidget(statePath);
  layout->addWidget(patchPath);
  layout->addWidget(cheatPath);
  layout->addWidget(dataPath);
}
