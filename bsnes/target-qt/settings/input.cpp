InputSettings::InputSettings() {
  activeInput = 0;
  activeGroup = 0;
  groupIndex = 0;

  layout = new QVBoxLayout;
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(Style::WidgetSpacing);
  setLayout(layout);

  list = new QTreeWidget;
  list->setColumnCount(2);
  list->setAllColumnsShowFocus(true);
  list->setSortingEnabled(false);
  list->header()->hide();
  list->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
  layout->addWidget(list);

  controlLayout = new QHBoxLayout;
  layout->addLayout(controlLayout);

  message = new QLabel;
  message->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
  controlLayout->addWidget(message);

  assignButton = new QPushButton("Assign");
  controlLayout->addWidget(assignButton);

  defaultButton = new QPushButton("Default");
  controlLayout->addWidget(defaultButton);

  clearButton = new QPushButton("Clear");
  controlLayout->addWidget(clearButton);

  buttonBox = new QLabel("Mouse Button");
  buttonBox->setFrameStyle(QFrame::Panel | QFrame::Raised);
  buttonBox->hide();
  controlLayout->addWidget(buttonBox);

  xAxisButton = new QPushButton("Mouse X-axis");
  xAxisButton->hide();
  controlLayout->addWidget(xAxisButton);

  yAxisButton = new QPushButton("Mouse Y-axis");
  yAxisButton->hide();
  controlLayout->addWidget(yAxisButton);

  stopButton = new QPushButton("Stop");
  stopButton->hide();
  controlLayout->addWidget(stopButton);

  connect(list, &QTreeWidget::itemSelectionChanged, this, &InputSettings::synchronize);
  connect(list, QOverload<QTreeWidgetItem*, int>::of(&QTreeWidget::itemActivated), this, &InputSettings::activateAssign);
  connect(assignButton, &QPushButton::released, this, &InputSettings::assign);
  connect(defaultButton, &QPushButton::released, this, &InputSettings::setDefault);
  connect(clearButton, &QPushButton::released, this, &InputSettings::clear);
  connect(xAxisButton, &QPushButton::released, this, &InputSettings::xAxisAssign);
  connect(yAxisButton, &QPushButton::released, this, &InputSettings::yAxisAssign);
  connect(stopButton, &QPushButton::released, this, &InputSettings::stop);

  //initialize list

  port1 = new QTreeWidgetItem(list);
  port1->setData(0, Qt::UserRole, QVariant(-1));
  port1->setText(0, "Controller Port 1");

  port2 = new QTreeWidgetItem(list);
  port2->setData(0, Qt::UserRole, QVariant(-1));
  port2->setText(0, "Controller Port 2");

  userInterface = new QTreeWidgetItem(list);
  userInterface->setData(0, Qt::UserRole, QVariant(-1));
  userInterface->setText(0, "User Interface");

  for(auto group : inputManager->inputList) {
    QTreeWidgetItem* grandparent = 0;
    if(group->category == InputCategory::Port1) { grandparent = port1; }
    if(group->category == InputCategory::Port2) { grandparent = port2; }
    if(group->category == InputCategory::UserInterface) { grandparent = userInterface; }
    if(!grandparent) continue;

    QTreeWidgetItem* parent = new QTreeWidgetItem(grandparent);
    parent->setData(0, Qt::UserRole, QVariant(-1));
    parent->setText(0, QString::fromUtf8(group->name));

    for(auto input : *group) {
      QTreeWidgetItem* child = new QTreeWidgetItem(parent);
      child->setData(0, Qt::UserRole, QVariant(qulonglong(inputTable.size())));
      inputTable.append(input);
    }
  }

  updateList();
  synchronize();
}

void InputSettings::inputEvent(shared_pointer<HID::Device> device, uint group, uint input, int16_t oldValue, int16_t newValue) {
  if(activeInput == nullptr) return;
  if(!isActiveWindow() || isMinimized()) return;
  string inputName = device->group(group).input(input).name();

  static const vector<string> modifiers{
    "Shift",   "LeftShift",   "RightShift",
    "Control", "LeftControl", "RightControl",
    "Alt",     "LeftAlt",     "RightAlt",
    "Super",   "LeftSuper",   "RightSuper",
  };
  if(dynamic_cast<DigitalInput*>(activeInput)) {
    if(device->isKeyboard() && !modifiers.find(inputName)) {
      //don't map escape key, as it is reserved by the user interface
      if(inputName == "Escape") return;

      if(activeInput->bind(device, group, input, oldValue, newValue) == false) return;
      stop();
      updateList();
    } else if(device->isKeyboard() && modifiers.find(inputName) && !config->input.modifierEnable) {
      if(activeInput->bind(device, group, input, oldValue, newValue) == false) return;
      stop();
      updateList();
    } else if(device->isMouse()) {
      //ensure button was clicked inside list box
      uint wx = 0, wy = 0;
      QWidget *widget = buttonBox;
      while(widget) {
        wx += widget->geometry().x();
        wy += widget->geometry().y();
        widget = widget->parentWidget();
      }
      uint px = QCursor::pos().x();
      uint py = QCursor::pos().y();
      if(px < wx || px >= wx + buttonBox->size().width()) return;
      if(py < wy || py >= wy + buttonBox->size().height()) return;

      if(activeInput->bind(device, group, input, oldValue, newValue) == false) return;
      stop();
      updateList();
    } else if(device->isJoypad()) {
      if(activeInput->bind(device, group, input, oldValue, newValue) == false) return;
      stop();
      updateList();
    }
  } else if(dynamic_cast<RelativeInput*>(activeInput)) {
    //for the mouse, actual assignment occurs during (x,y)AxisAssign()
    if(device->isJoypad() && abs(newValue) > 24576) {
      if(activeInput->bind(device, group, input, oldValue, newValue) == false) return;
      stop();
      updateList();
    }
  }
}

void InputSettings::synchronize() {
  bool enable = false;

  QList<QTreeWidgetItem*> items = list->selectedItems();
  if(items.count() > 0) {
    QTreeWidgetItem *item = items[0];
    int index = item->data(0, Qt::UserRole).toInt();
    if(index >= 0) enable = true;
    else {
      item = item->child(0);
      if(item) {
        index = item->data(0, Qt::UserRole).toInt();
        if(index >= 0) enable = true;
      }
    }
  }

  assignButton->setEnabled(enable);
  defaultButton->setEnabled(enable);
  clearButton->setEnabled(enable);
}

//called when double-clicking any list item;
//double-clicking a group expands or collapses it,
//so avoid triggering assign() unless this is not a group
void InputSettings::activateAssign() {
  QTreeWidgetItem* item = list->currentItem();
  if(!item) return;
  int index = item->data(0, Qt::UserRole).toInt();
  if(index >= 0) assign();
}

void InputSettings::assign() {
  QTreeWidgetItem* item = list->currentItem();
  if(!item) return;
  int index = item->data(0, Qt::UserRole).toInt();
  if(index == -1) return assignGroup();
  setActiveInput(inputTable[index]);
}

void InputSettings::assignGroup() {
  QTreeWidgetItem* item = list->currentItem();
  if(!item) return;
  item->setExpanded(true);
  item = item->child(0);
  if(!item) return;
  int index = item->data(0, Qt::UserRole).toInt();
  if(index == -1) return;

  AbstractInput* input = inputTable[index];
  activeGroup = input->parent;
  setActiveInput((*activeGroup)[groupIndex = 0]);
}

void InputSettings::setDefault() {
  QTreeWidgetItem* item = list->currentItem();
  if(!item) return;
  int index = item->data(0, Qt::UserRole).toInt();
  if(index == -1) return setGroupDefault();

  AbstractInput* input = inputTable[index];
  input->bindDefault();
  updateList();
}

void InputSettings::setGroupDefault() {
  QTreeWidgetItem* item = list->currentItem();
  if(!item) return;
  item = item->child(0);
  if(!item) return;
  int index = item->data(0, Qt::UserRole).toInt();
  if(index == -1) return;

  AbstractInput* input = inputTable[index];
  InputGroup& group = *(input->parent);
  for(uint i = 0; i < group.size(); i++) {
    group[i]->bindDefault();
  }
  updateList();
}

void InputSettings::clear() {
  QTreeWidgetItem* item = list->currentItem();
  if(!item) return;
  int index = item->data(0, Qt::UserRole).toInt();
  if(index == -1) return clearGroup();

  AbstractInput* input = inputTable[index];
  input->unbind();
  updateList();
}

void InputSettings::clearGroup() {
  QTreeWidgetItem* item = list->currentItem();
  if(!item) return;
  item = item->child(0);
  if(!item) return;
  int index = item->data(0, Qt::UserRole).toInt();
  if(index == -1) return;

  AbstractInput* input = inputTable[index];
  InputGroup& group = *(input->parent);
  for(uint i = 0; i < group.size(); i++) {
    group[i]->unbind();
  }
  updateList();
}

void InputSettings::xAxisAssign() {
  activeInput->bind(inputManager->cache.mouse, HID::Mouse::GroupID::Axis, 0, 0, 32767);
  stop();
  updateList();
}

void InputSettings::yAxisAssign() {
  activeInput->bind(inputManager->cache.mouse, HID::Mouse::GroupID::Axis, 1, 0, 32767);
  stop();
  updateList();
}

void InputSettings::stop() {
  setActiveInput(0);
  activeGroup = 0;
}

void InputSettings::updateList() {
  QList<QTreeWidgetItem*> all = list->findItems("", Qt::MatchContains);
  for(uint i = 0; i < all.size(); i++) {
    QTreeWidgetItem* grandparent = all[i];
    for(uint j = 0; j < grandparent->childCount(); j++) {
      QTreeWidgetItem* parent = grandparent->child(j);
      for(uint k = 0; k < parent->childCount(); k++) {
        QTreeWidgetItem* child = parent->child(k);
        int index = child->data(0, Qt::UserRole).toInt();
        if(index == -1) continue;
        auto abstract = inputTable[index];
        child->setText(0, QString::fromUtf8(abstract->name));
        child->setText(1, QString::fromUtf8(string{"= ", inputManager->sanitize(abstract->mapping, "+")}));
        child->setForeground(1, QBrush(QColor(128, 128, 128)));
      }
    }
  }
}

void InputSettings::setActiveInput(AbstractInput *input) {
  //flush any pending events to prevent instantaneous assignment of scancodes
  activeInput = 0;
  inputManager->poll();
  activeInput = input;

  if(activeInput) {
    assignButton->hide();
    defaultButton->hide();
    clearButton->hide();
    if(dynamic_cast<DigitalInput*>(input)) {
      buttonBox->show();
      xAxisButton->hide();
      yAxisButton->hide();
    } else {
      buttonBox->hide();
      xAxisButton->show();
      yAxisButton->show();
    }
    stopButton->show();

    message->setFocus();
    message->setText(QString::fromUtf8(string{"Set assignment for: ", activeInput->name}));
  } else {
    assignButton->show();
    defaultButton->show();
    clearButton->show();
    buttonBox->hide();
    xAxisButton->hide();
    yAxisButton->hide();
    stopButton->hide();

    list->setFocus();
    message->setText("");
  }
}
