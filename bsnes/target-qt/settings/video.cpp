VideoSettings::VideoSettings() {
  layout = new QVBoxLayout;
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(Style::WidgetSpacing);
  layout->setAlignment(Qt::AlignTop);
  setLayout(layout);

  displayLabel = new QLabel("<b>Display</b>");
  layout->addWidget(displayLabel);

  displayLayout = new QHBoxLayout;
  layout->addLayout(displayLayout);

  autoHideFullscreenMenu = new QCheckBox("Auto-hide menus when entering fullscreen mode");
  displayLayout->addWidget(autoHideFullscreenMenu);

  fullscreenExclusive = new QCheckBox("Fullscreen Exclusive");
  displayLayout->addWidget(fullscreenExclusive);

  colorLabel = new QLabel("<b>Color Adjustment</b>");
  layout->addWidget(colorLabel);

  colorLayout = new QGridLayout;
  layout->addLayout(colorLayout);

  contrastLabel = new QLabel("Contrast adjust:");
  colorLayout->addWidget(contrastLabel, 0, 0);

  contrastValue = new QLabel;
  contrastValue->setAlignment(Qt::AlignHCenter);
  contrastValue->setMinimumWidth(contrastValue->fontMetrics().width("+100%"));
  colorLayout->addWidget(contrastValue, 0, 1);

  contrastSlider = new QSlider(Qt::Horizontal);
  contrastSlider->setMinimum(-95);
  contrastSlider->setMaximum(+95);
  colorLayout->addWidget(contrastSlider, 0, 2);

  brightnessLabel = new QLabel("Brightness adjust:");
  colorLayout->addWidget(brightnessLabel, 1, 0);

  brightnessValue = new QLabel;
  brightnessValue->setAlignment(Qt::AlignHCenter);
  colorLayout->addWidget(brightnessValue, 1, 1);

  brightnessSlider = new QSlider(Qt::Horizontal);
  brightnessSlider->setMinimum(-95);
  brightnessSlider->setMaximum(+95);
  colorLayout->addWidget(brightnessSlider, 1, 2);

  gammaLabel = new QLabel("Gamma adjust:");
  colorLayout->addWidget(gammaLabel, 2, 0);

  gammaValue = new QLabel;
  gammaValue->setAlignment(Qt::AlignHCenter);
  colorLayout->addWidget(gammaValue, 2, 1);

  gammaSlider = new QSlider(Qt::Horizontal);
  gammaSlider->setMinimum(-95);
  gammaSlider->setMaximum(+95);
  colorLayout->addWidget(gammaSlider, 2, 2);

  scanlineLabel = new QLabel("Scanline adjust:");
  colorLayout->addWidget(scanlineLabel, 3, 0);

  scanlineValue = new QLabel;
  scanlineValue->setAlignment(Qt::AlignHCenter);
  colorLayout->addWidget(scanlineValue, 3, 1);

  scanlineSlider = new QSlider(Qt::Horizontal);
  scanlineSlider->setMinimum(0);
  scanlineSlider->setMaximum(20);
  scanlineSlider->setPageStep(4);
  colorLayout->addWidget(scanlineSlider, 3, 2);

  options = new QHBoxLayout;
  layout->addLayout(options);

  enableColorEmulation = new QCheckBox("Simulate NTSC TV gamma ramp");
  enableColorEmulation->setToolTip("Lower monitor gamma to more accurately match a CRT television");
  options->addWidget(enableColorEmulation);

  enableColorBleed = new QCheckBox("Simulate hires blurring");
  enableColorBleed->setToolTip("In pseudo-512 mode, blend alternating columns to simulate transparency");
  options->addWidget(enableColorBleed);

  cropLabel = new QLabel("<b>Overscan Compensation</b>");
  layout->addWidget(cropLabel);

  cropLayout = new QGridLayout;
  layout->addLayout(cropLayout);

  cropLeftLabel = new QLabel("Left:");
  cropLayout->addWidget(cropLeftLabel, 0, 0);

  cropLeftValue = new QLabel;
  cropLeftValue->setAlignment(Qt::AlignHCenter);
  cropLeftValue->setMinimumWidth(cropLeftValue->fontMetrics().width("+100%"));
  cropLayout->addWidget(cropLeftValue, 0, 1);

  cropLeftSlider = new QSlider(Qt::Horizontal);
  cropLeftSlider->setMinimum(0);
  cropLeftSlider->setMaximum(20);
  cropLayout->addWidget(cropLeftSlider, 0, 2);

  cropTopLabel = new QLabel("Top:");
  cropLayout->addWidget(cropTopLabel, 1, 0);

  cropTopValue = new QLabel;
  cropTopValue->setAlignment(Qt::AlignHCenter);
  cropLayout->addWidget(cropTopValue, 1, 1);

  cropTopSlider = new QSlider(Qt::Horizontal);
  cropTopSlider->setMinimum(0);
  cropTopSlider->setMaximum(20);
  cropLayout->addWidget(cropTopSlider, 1, 2);

  cropRightLabel = new QLabel("Right:");
  cropLayout->addWidget(cropRightLabel, 2, 0);

  cropRightValue = new QLabel;
  cropRightValue->setAlignment(Qt::AlignHCenter);
  cropLayout->addWidget(cropRightValue, 2, 1);

  cropRightSlider = new QSlider(Qt::Horizontal);
  cropRightSlider->setMinimum(0);
  cropRightSlider->setMaximum(20);
  cropLayout->addWidget(cropRightSlider, 2, 2);

  cropBottomLabel = new QLabel("Bottom:");
  cropLayout->addWidget(cropBottomLabel, 3, 0);

  cropBottomValue = new QLabel;
  cropBottomValue->setAlignment(Qt::AlignHCenter);
  cropLayout->addWidget(cropBottomValue, 3, 1);

  cropBottomSlider = new QSlider(Qt::Horizontal);
  cropBottomSlider->setMinimum(0);
  cropBottomSlider->setMaximum(20);
  cropLayout->addWidget(cropBottomSlider, 3, 2);

  pixelShaderLabel = new QLabel("<b>Pixel Shader</b>");
  layout->addWidget(pixelShaderLabel);

  pixelShaderLayout = new QGridLayout;
  layout->addLayout(pixelShaderLayout);

  shaderValue = new QLineEdit;
  pixelShaderLayout->addWidget(shaderValue, 0, 0);

  shaderSelect = new QPushButton("Select ...");
  pixelShaderLayout->addWidget(shaderSelect, 0, 1);

  shaderDefault = new QPushButton("Default");
  pixelShaderLayout->addWidget(shaderDefault, 0, 2);

  connect(autoHideFullscreenMenu, QOverload<int>::of(&QCheckBox::stateChanged), this, &VideoSettings::autoHideFullscreenMenuToggle);
  connect(fullscreenExclusive, QOverload<int>::of(&QCheckBox::stateChanged), this, &VideoSettings::fullscreenExclusiveToggle);
  connect(contrastSlider, QOverload<int>::of(&QSlider::valueChanged), this, &VideoSettings::contrastAdjust);
  connect(brightnessSlider, QOverload<int>::of(&QSlider::valueChanged), this, &VideoSettings::brightnessAdjust);
  connect(gammaSlider, QOverload<int>::of(&QSlider::valueChanged), this, &VideoSettings::gammaAdjust);
  connect(scanlineSlider, QOverload<int>::of(&QSlider::valueChanged), this, &VideoSettings::scanlineAdjust);
  connect(enableColorEmulation, QOverload<int>::of(&QCheckBox::stateChanged), this, &VideoSettings::colorEmulationToggle);
  connect(enableColorBleed, QOverload<int>::of(&QCheckBox::stateChanged), this, &VideoSettings::colorBleedToggle);
  connect(cropLeftSlider, QOverload<int>::of(&QSlider::valueChanged), this, &VideoSettings::cropLeftAdjust);
  connect(cropTopSlider, QOverload<int>::of(&QSlider::valueChanged), this, &VideoSettings::cropTopAdjust);
  connect(cropRightSlider, QOverload<int>::of(&QSlider::valueChanged), this, &VideoSettings::cropRightAdjust);
  connect(cropBottomSlider, QOverload<int>::of(&QSlider::valueChanged), this, &VideoSettings::cropBottomAdjust);
  connect(shaderSelect, &QPushButton::released, this, &VideoSettings::selectShader);
  connect(shaderDefault, &QPushButton::released, this, &VideoSettings::defaultShader);

  syncUi();
}

void VideoSettings::synchronizeDriverSettings() {
  if(video.hasExclusive()) {
    fullscreenExclusive->show();
  } else {
    fullscreenExclusive->hide();
  }
  if(video.hasShader()) {
    pixelShaderLabel->show();
    shaderValue->show();
    shaderSelect->show();
    shaderDefault->show();
  } else {
    pixelShaderLabel->hide();
    shaderValue->hide();
    shaderSelect->hide();
    shaderDefault->hide();
  }
}

void VideoSettings::syncUi() {
  int n;

  autoHideFullscreenMenu->setChecked(config->video.autoHideFullscreenMenu);
  fullscreenExclusive->setChecked(config->video.fullscreenExclusive);

  n = config->video.saturation - 100;
  contrastValue->setText(QString::fromUtf8(string{n, "%"}));
  contrastSlider->setSliderPosition(n);

  n = config->video.luminance - 100;
  brightnessValue->setText(QString::fromUtf8(string{n, "%"}));
  brightnessSlider->setSliderPosition(n);

  n = config->video.gamma - 100;
  gammaValue->setText(QString::fromUtf8(string{n, "%"}));
  gammaSlider->setSliderPosition(n);

  n = config->video.scanline;
  scanlineValue->setText(QString::fromUtf8(string{n, "%"}));
  scanlineSlider->setSliderPosition(n / 5);

  enableColorEmulation->setChecked(emulator->getOption("video/colorEmulation") == "true");
  enableColorBleed->setChecked(emulator->getOption("video/colorBleed") == "true");

  n = config->video.cropLeft;
  cropLeftValue->setText(QString::fromUtf8(string{n, "%"}));
  cropLeftSlider->setSliderPosition(n);

  n = config->video.cropTop;
  cropTopValue->setText(QString::fromUtf8(string{n, "%"}));
  cropTopSlider->setSliderPosition(n);

  n = config->video.cropRight;
  cropRightValue->setText(QString::fromUtf8(string{n, "%"}));
  cropRightSlider->setSliderPosition(n);

  n = config->video.cropBottom;
  cropBottomValue->setText(QString::fromUtf8(string{n, "%"}));
  cropBottomSlider->setSliderPosition(n);

  shaderValue->setText(QString::fromUtf8(config->video.shader));
}

void VideoSettings::autoHideFullscreenMenuToggle() {
  config->video.autoHideFullscreenMenu = autoHideFullscreenMenu->isChecked();
}

void VideoSettings::fullscreenExclusiveToggle() {
  config->video.fullscreenExclusive = fullscreenExclusive->isChecked();
}

void VideoSettings::contrastAdjust(int value) {
  config->video.saturation = value + 100;
  syncUi();
  program->updateVideoPalette();
}

void VideoSettings::brightnessAdjust(int value) {
  config->video.luminance = value + 100;
  syncUi();
  program->updateVideoPalette();
}

void VideoSettings::gammaAdjust(int value) {
  config->video.gamma = value + 100;
  syncUi();
  program->updateVideoPalette();
}

void VideoSettings::scanlineAdjust(int value) {
  config->video.scanline = value * 5;
  syncUi();
  scanlineFilter.setIntensity(value * 5);
}

void VideoSettings::colorEmulationToggle(int state) {
  emulator->setOption("video/colorEmulation", state == Qt::Checked);
  syncUi();
  program->updateVideoPalette();
}

void VideoSettings::colorBleedToggle(int state) {
  emulator->setOption("video/colorBleed", state == Qt::Checked);
  syncUi();
}

void VideoSettings::cropLeftAdjust(int state) {
  config->video.cropLeft = state;
  if(config->video.context->multiplier != 8) program->cropLeft = state;
  syncUi();
}

void VideoSettings::cropTopAdjust(int state) {
  config->video.cropTop = state;
  if(config->video.context->multiplier != 8) program->cropTop = state;
  syncUi();
}

void VideoSettings::cropRightAdjust(int state) {
  config->video.cropRight = state;
  if(config->video.context->multiplier != 8) program->cropRight = state;
  syncUi();
}

void VideoSettings::cropBottomAdjust(int state) {
  config->video.cropBottom = state;
  if(config->video.context->multiplier != 8) program->cropBottom = state;
  syncUi();
}

void VideoSettings::selectShader() {
  fileBrowser->setWindowTitle("Select Pixel Shader");
  fileBrowser->loadShader();
}

void VideoSettings::defaultShader() {
  config->video.shader = "";
  syncUi();
  program->updateVideoShader();
}
