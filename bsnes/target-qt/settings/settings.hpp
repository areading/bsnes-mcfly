class ProfileSettings : public QWidget {
public:
  ProfileSettings(bool, bool);

  QVBoxLayout* layout;
    QLabel* profileInfo;
    QRadioButton* profileAccuracy;
    QLabel* profileAccuracyInfo;
    QRadioButton* profileCompatibility;
    QLabel* profileCompatibilityInfo;
    QRadioButton* profilePerformance;
    QLabel* profilePerformanceInfo;
};

class VideoSettings : public QWidget {
public:
  VideoSettings();
  void synchronizeDriverSettings();
  void syncUi();

  void autoHideFullscreenMenuToggle();
  void fullscreenExclusiveToggle();
  void contrastAdjust(int);
  void brightnessAdjust(int);
  void gammaAdjust(int);
  void scanlineAdjust(int);
  void colorEmulationToggle(int);
  void colorBleedToggle(int);
  void cropLeftAdjust(int);
  void cropTopAdjust(int);
  void cropRightAdjust(int);
  void cropBottomAdjust(int);
  void selectShader();
  void defaultShader();

  QVBoxLayout* layout;
    QLabel* displayLabel;
    QHBoxLayout* displayLayout;
      QCheckBox* autoHideFullscreenMenu;
      QCheckBox* fullscreenExclusive;
    QLabel* colorLabel;
    QGridLayout* colorLayout;
      QLabel* contrastLabel;
      QLabel* contrastValue;
      QSlider* contrastSlider;
      QLabel* brightnessLabel;
      QLabel* brightnessValue;
      QSlider* brightnessSlider;
      QLabel* gammaLabel;
      QLabel* gammaValue;
      QSlider* gammaSlider;
      QLabel* scanlineLabel;
      QLabel* scanlineValue;
      QSlider* scanlineSlider;
    QHBoxLayout* options;
      QCheckBox* enableColorEmulation;
      QCheckBox* enableColorBleed;
    QLabel* cropLabel;
    QGridLayout* cropLayout;
      QLabel* cropLeftLabel;
      QLabel* cropLeftValue;
      QSlider* cropLeftSlider;
      QLabel* cropTopLabel;
      QLabel* cropTopValue;
      QSlider* cropTopSlider;
      QLabel* cropRightLabel;
      QLabel* cropRightValue;
      QSlider* cropRightSlider;
      QLabel* cropBottomLabel;
      QLabel* cropBottomValue;
      QSlider* cropBottomSlider;
    QLabel* pixelShaderLabel;
    QGridLayout* pixelShaderLayout;
      QLineEdit* shaderValue;
      QPushButton* shaderSelect;
      QPushButton* shaderDefault;
};

class AudioSettings : public QWidget {
public:
  AudioSettings();
  void synchronizeDriverSettings();
  void syncUi();

  void deviceChange(int value);
  void frequencyChange(int value);
  void latencyChange(int value);
  void exclusiveToggle();
  void volumeAdjust(int value);
//void frequencySkewAdjust(int value);

  QVBoxLayout* layout;
    QHBoxLayout* boxes;
      QLabel* deviceLabel;
      QComboBox* deviceList;
      QLabel* frequencyLabel;
      QComboBox* frequencyList;
      QLabel* latencyLabel;
      QComboBox* latencyList;
    QCheckBox* exclusive;
    QGridLayout* sliders;
      QLabel* volumeLabel;
      QLabel* volumeValue;
      QSlider* volume;
    //QLabel* frequencySkewLabel;
    //QLabel* frequencySkewValue;
    //QSlider* frequencySkew;
};

class InputSettings : public QWidget {
public:
  InputSettings();

  void inputEvent(shared_pointer<HID::Device> device, uint group, uint input, int16_t oldValue, int16_t newValue);

  void synchronize();
  void activateAssign();
  void assign();
  void assignGroup();
  void setDefault();
  void setGroupDefault();
  void clear();
  void clearGroup();
  void xAxisAssign();
  void yAxisAssign();
  void stop();

  QVBoxLayout* layout;
    QTreeWidget* list;
    QHBoxLayout* controlLayout;
      QLabel* message;
      QPushButton* assignButton;
      QPushButton* defaultButton;
      QPushButton* clearButton;
      QLabel* buttonBox;
      QPushButton* xAxisButton;
      QPushButton* yAxisButton;
      QPushButton* stopButton;

private:
  void updateList();
  void setActiveInput(AbstractInput*);

  QTreeWidgetItem* port1;
  QTreeWidgetItem* port2;
  QTreeWidgetItem* userInterface;
  vector<AbstractInput*> inputTable;
  AbstractInput* activeInput;
  InputGroup* activeGroup;
  uint groupIndex;
};

class PathSettingWidget : public QWidget {
public:
  PathSettingWidget(string&, const char*, const char*, const char*);
  void acceptPath(const string&);
  void updatePath();
  void selectPath();
  void defaultPath();

  QVBoxLayout* layout;
    QLabel* label;
    QHBoxLayout* controlLayout;
      QLineEdit* path;
      QPushButton* pathSelect;
      QPushButton* pathDefault;

  string& pathValue;
  string pathDefaultLabel;
  string pathBrowseLabel;
};

class PathSettings : public QWidget {
public:
  PathSettings();

  QVBoxLayout* layout;
    PathSettingWidget* gamePath;
    PathSettingWidget* savePath;
    PathSettingWidget* statePath;
    PathSettingWidget* patchPath;
    PathSettingWidget* cheatPath;
    PathSettingWidget* dataPath;
};

class AdvancedSettings : public QWidget {
public:
  AdvancedSettings();
  auto initializeUi() -> void;
  auto updateVideoDriver() -> void;
  auto updateAudioDriver() -> void;
  auto updateInputDriver() -> void;
  auto updateConfiguration() -> void;
  auto setRewindGranularity(int value) -> void;
  auto setRewindHistory(int value) -> void;

  QVBoxLayout* layout;
    QGridLayout* driverLayout;
      QLabel* videoLabel;
      QLabel* audioLabel;
      QLabel* inputLabel;
      QComboBox* videoDriver;
      QComboBox* audioDriver;
      QComboBox* inputDriver;
      QLabel* driverInfo;
    QLabel* regionTitle;
    QHBoxLayout* regionLayout;
      QButtonGroup* regionGroup;
      QRadioButton* regionAuto;
      QRadioButton* regionNTSC;
      QRadioButton* regionPAL;
    QLabel* portTitle;
    QHBoxLayout* portLayout;
      QButtonGroup* portGroup;
      QRadioButton* portSatellaview;
      QRadioButton* portNone;
      QWidget* portSpacer;
    QLabel* focusTitle;
    QHBoxLayout* focusLayout;
      QButtonGroup* focusButtonGroup;
      QRadioButton* focusPause;
      QRadioButton* focusIgnore;
      QRadioButton* focusAllow;
    QLabel* miscTitle;
    QCheckBox* rewindEnable;
    QGridLayout* rewindSliders;
      QLabel* rewindGranularityLabel;
      QLabel* rewindGranularityValue;
      QSlider* rewindGranularity;
      QLabel* rewindHistoryLabel;
      QLabel* rewindHistoryValue;
      QSlider* rewindHistory;
    QCheckBox* gpuFlushEnable;
    QCheckBox* gpuBypassWM;
    QCheckBox* useCommonDialogs;
    QLabel* hacksTitle;
    QCheckBox* hacksDoubleVRAM;
    QCheckBox* hacksNoSpriteLimit;
    QCheckBox* hacksHiresMode7;
};

class SettingsWindow : public Window {
public:
  SettingsWindow();
  ~SettingsWindow();

  QVBoxLayout* layout;
    QTabWidget* tab;
      QScrollArea* profileArea = nullptr;
        ProfileSettings* profileSettings = nullptr;
      QScrollArea* videoArea;
        VideoSettings* videoSettings;
      QScrollArea* audioArea;
        AudioSettings* audioSettings;
      QScrollArea* inputArea;
        InputSettings* inputSettings;
      QScrollArea* pathArea;
        PathSettings* pathSettings;
      QScrollArea* advancedArea;
        AdvancedSettings* advancedSettings;
};

extern unique_pointer<SettingsWindow> settingsWindow;
