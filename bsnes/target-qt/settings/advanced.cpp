AdvancedSettings::AdvancedSettings() {
  layout = new QVBoxLayout;
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(0);
  layout->setAlignment(Qt::AlignTop);
  setLayout(layout);

  driverLayout = new QGridLayout;
  driverLayout->setHorizontalSpacing(Style::WidgetSpacing);
  layout->addLayout(driverLayout);
  layout->addSpacing(Style::WidgetSpacing);

  videoLabel = new QLabel("Video driver:");
  driverLayout->addWidget(videoLabel, 0, 0);

  audioLabel = new QLabel("Audio driver:");
  driverLayout->addWidget(audioLabel, 0, 1);

  inputLabel = new QLabel("Input driver:");
  driverLayout->addWidget(inputLabel, 0, 2);

  videoDriver = new QComboBox;
  driverLayout->addWidget(videoDriver, 1, 0);

  audioDriver = new QComboBox;
  driverLayout->addWidget(audioDriver, 1, 1);

  inputDriver = new QComboBox;
  driverLayout->addWidget(inputDriver, 1, 2);

  driverInfo = new QLabel("<small>Note: driver changes require restart to take effect.</small>");
  driverInfo->setStyleSheet("margin-left: -3px; margin-top: 5px;");
  driverLayout->addWidget(driverInfo, 2, 0, 1, 3);

  regionTitle = new QLabel("Hardware region:");
  layout->addWidget(regionTitle);

  regionLayout = new QHBoxLayout;
  regionLayout->setSpacing(Style::WidgetSpacing);
  layout->addLayout(regionLayout);
  layout->addSpacing(Style::WidgetSpacing);

  regionGroup = new QButtonGroup(this);

  regionAuto = new QRadioButton("Auto-detect");
  regionAuto->setToolTip("Automatically select hardware region on cartridge load");
  regionGroup->addButton(regionAuto);
  regionLayout->addWidget(regionAuto);

  regionNTSC = new QRadioButton("NTSC");
  regionNTSC->setToolTip("Force NTSC region (Japan, Korea, US)");
  regionGroup->addButton(regionNTSC);
  regionLayout->addWidget(regionNTSC);

  regionPAL = new QRadioButton("PAL");
  regionPAL->setToolTip("Force PAL region (Europe, ...)");
  regionGroup->addButton(regionPAL);
  regionLayout->addWidget(regionPAL);

  portTitle = new QLabel("Expansion port device:");
  layout->addWidget(portTitle);

  portLayout = new QHBoxLayout;
  portLayout->setSpacing(Style::WidgetSpacing);
  layout->addLayout(portLayout);
  layout->addSpacing(Style::WidgetSpacing);

  portGroup = new QButtonGroup(this);

  portSatellaview = new QRadioButton("Satellaview");
  portGroup->addButton(portSatellaview);
  portLayout->addWidget(portSatellaview);

  portNone = new QRadioButton("None");
  portGroup->addButton(portNone);
  portLayout->addWidget(portNone);

  portSpacer = new QWidget;
  portLayout->addWidget(portSpacer);

  focusTitle = new QLabel("When main window does not have focus:");
  layout->addWidget(focusTitle);

  focusLayout = new QHBoxLayout;
  focusLayout->setSpacing(Style::WidgetSpacing);
  layout->addLayout(focusLayout);
  layout->addSpacing(Style::WidgetSpacing);

  focusButtonGroup = new QButtonGroup(this);

  focusPause = new QRadioButton("Pause emulation");
  focusPause->setToolTip("Ideal for prolonged multi-tasking");
  focusButtonGroup->addButton(focusPause);
  focusLayout->addWidget(focusPause);

  focusIgnore = new QRadioButton("Ignore input");
  focusIgnore->setToolTip("Ideal for light multi-tasking when using keyboard");
  focusButtonGroup->addButton(focusIgnore);
  focusLayout->addWidget(focusIgnore);

  focusAllow = new QRadioButton("Allow input");
  focusAllow->setToolTip("Ideal for light multi-tasking when using joypad(s)");
  focusButtonGroup->addButton(focusAllow);
  focusLayout->addWidget(focusAllow);

  miscTitle = new QLabel("Miscellaneous:");
  layout->addWidget(miscTitle);

  rewindEnable = new QCheckBox("Enable Rewind Support");
  layout->addWidget(rewindEnable);

  rewindSliders = new QGridLayout;
  layout->addLayout(rewindSliders);

  rewindGranularityLabel = new QLabel("Rewind Granularity:");
  rewindGranularityLabel->setToolTip("Number of frames between rewind snapshots. Note that too low a value may reduce performance.");
  rewindSliders->addWidget(rewindGranularityLabel, 0, 0);

  rewindGranularityValue = new QLabel;
  rewindGranularityValue->setAlignment(Qt::AlignHCenter);
  rewindGranularityValue->setMinimumWidth(rewindGranularityValue->fontMetrics().width("1000"));
  rewindSliders->addWidget(rewindGranularityValue, 0, 1);

  rewindGranularity = new QSlider(Qt::Horizontal);
  rewindGranularity->setMinimum(6);
  rewindGranularity->setMaximum(180);
  rewindSliders->addWidget(rewindGranularity, 0, 2);

  rewindHistoryLabel = new QLabel("Rewind History:");
  rewindHistoryLabel->setToolTip("Maximum number of auto-rewind snapshots to store in memory.");
  rewindSliders->addWidget(rewindHistoryLabel, 1, 0);

  rewindHistoryValue = new QLabel;
  rewindHistoryValue->setAlignment(Qt::AlignHCenter);
  rewindHistoryValue->setMinimumWidth(rewindHistoryValue->fontMetrics().width("1000"));
  rewindSliders->addWidget(rewindHistoryValue, 1, 1);

  rewindHistory = new QSlider(Qt::Horizontal);
  rewindHistory->setMinimum(30);
  rewindHistory->setMaximum(300);
  rewindSliders->addWidget(rewindHistory, 1, 2);


  useCommonDialogs = new QCheckBox("Use Native OS File Dialogs");
  layout->addWidget(useCommonDialogs);

  gpuFlushEnable = new QCheckBox("GPU Pipeline Syncing");
  gpuFlushEnable->setToolTip("(OpenGL / GL ES only) After every frame, flush the GPU pipeline & block until all rendering\n"
                             "completes. This will generally reduce input lag - up to one frame's worth - at the expense of\n"
                             "some throughput performance (i.e., maximum attainable FPS).");
  layout->addWidget(gpuFlushEnable);

  gpuBypassWM = new QCheckBox("Use Exclusive Rendering Context");
  gpuBypassWM->setToolTip("(OpenGL / X11 only) Disable Window Manager's compositor so rendering can bypass WM entirely\n"
                          "This will reduce input lag by one or two frames if a compositing WM is in use.");
  #if defined(__linux__) || defined(__unix__) || defined(__unix)
  layout->addWidget(gpuBypassWM);
  #endif

  hacksTitle = new QLabel("Hacks:");
  layout->addWidget(hacksTitle);

  hacksDoubleVRAM = new QCheckBox("Double VRAM");
  #if defined(DOUBLE_VRAM)
  hacksDoubleVRAM->setToolTip("Set VRAM to 128 KB instead of 64 KB. Some games such as Yoshi's Island can't handle this.");
  #else
  #if defined(PROFILE_PERFORMANCE)
  hacksDoubleVRAM->setToolTip("This option will not work with the Performance profile.");
  #else
  hacksDoubleVRAM->setToolTip("This option will not work unless you recompile bsnes-mcfly with double VRAM support.");
  #endif
  #endif
  #if !defined(DOUBLE_VRAM)
  hacksDoubleVRAM->setEnabled(false);
  #endif
  layout->addWidget(hacksDoubleVRAM);

  hacksNoSpriteLimit = new QCheckBox("No Sprite Limit");
  hacksNoSpriteLimit->setToolTip("Increase the maximum sprites per scanline from 32 to 128.");
  layout->addWidget(hacksNoSpriteLimit);

  hacksHiresMode7 = new QCheckBox("Hires Mode 7");
  hacksHiresMode7->setToolTip("Increase the resolution to 512×224 for crisper Mode 7 rendering.");
  layout->addWidget(hacksHiresMode7);

  updateVideoDriver();
  updateAudioDriver();
  updateInputDriver();
  updateConfiguration();

  connect(regionAuto, &QRadioButton::pressed, [this]() { config->sfc.region = "Auto"; });
  connect(regionNTSC, &QRadioButton::pressed, [this]() { config->sfc.region = "NTSC"; });
  connect(regionPAL , &QRadioButton::pressed, [this]() { config->sfc.region = "PAL";  });

  connect(portSatellaview, &QRadioButton::pressed, [this]() { emulator->connect(2, SFC::ID::Device::Satellaview); });
  connect(portSatellaview, &QRadioButton::pressed, [this]() { emulator->connect(2, SFC::ID::Device::None);        });

  connect(focusPause, &QRadioButton::pressed, [this]() {
    config->input.focusPolicy = ConfigurationSettings::Input::FocusPolicyPauseEmulation;
  });
  connect(focusIgnore, &QRadioButton::pressed, [this]() {
    config->input.focusPolicy = ConfigurationSettings::Input::FocusPolicyIgnoreInput;
  });
  connect(focusAllow, &QRadioButton::pressed, [this]() {
    config->input.focusPolicy = ConfigurationSettings::Input::FocusPolicyAllowInput;
  });

  connect(rewindEnable, &QCheckBox::stateChanged, [this]() {
    config->system.rewindEnabled = rewindEnable->isChecked();
    setRewindGranularity(uint(config->system.rewindGranularity));
    setRewindHistory(uint(config->system.rewindHistory));
    program->resetRewindHistory(config->system.rewindHistory, config->system.rewindGranularity);
  });
  connect(rewindGranularity, QOverload<int>::of(&QSlider::valueChanged), this, &AdvancedSettings::setRewindGranularity);
  connect(rewindHistory, QOverload<int>::of(&QSlider::valueChanged), this, &AdvancedSettings::setRewindHistory);
  connect(useCommonDialogs, &QCheckBox::stateChanged, [this]() {
    config->diskBrowser.useCommonDialogs = useCommonDialogs->isChecked();
  });
  connect(gpuFlushEnable, &QCheckBox::stateChanged, [this]() {
    config->video.flush = gpuFlushEnable->isChecked();
  });
  connect(gpuBypassWM, &QCheckBox::stateChanged, [this]() {
    config->video.exclusive = gpuBypassWM->isChecked();
  });
  connect(hacksDoubleVRAM, &QCheckBox::stateChanged, [this]() {
    emulator->setProperty("system/ppu1/vram/size", hacksDoubleVRAM->isChecked() ? 131072 : 65536);
  });
  connect(hacksNoSpriteLimit, &QCheckBox::stateChanged, [this]() {
    emulator->setOption("hack/ppu/noSpriteLimit", (bool)hacksNoSpriteLimit->isChecked());
  });
  connect(hacksHiresMode7, &QCheckBox::stateChanged, [this]() {
    emulator->setOption("hack/ppu/hiresMode7", (bool)hacksHiresMode7->isChecked());
  });
}

void AdvancedSettings::updateVideoDriver() {
  videoDriver->disconnect();
  videoDriver->clear();

  for(string& driver : video.hasDrivers()) {
    videoDriver->addItem(QString::fromUtf8(driver));
    if(driver == config->video.driver) videoDriver->setCurrentText(QString::fromUtf8(driver));
  }

  connect(videoDriver, QOverload<int>::of(&QComboBox::currentIndexChanged), [this](int index) {
    if(index >= 0) config->video.driver = videoDriver->itemText(index).toUtf8().data();
  });
}

void AdvancedSettings::updateAudioDriver() {
  audioDriver->disconnect();
  audioDriver->clear();

  for(string& driver : audio.hasDrivers()) {
    audioDriver->addItem(QString::fromUtf8(driver));
    if(driver == config->audio.driver) audioDriver->setCurrentText(QString::fromUtf8(driver));
  }

  connect(audioDriver, QOverload<int>::of(&QComboBox::currentIndexChanged), [this](int index) {
    if(index >= 0) config->audio.driver = audioDriver->itemText(index).toUtf8().data();
  });
}

void AdvancedSettings::updateInputDriver() {
  inputDriver->disconnect();
  inputDriver->clear();

  for(string& driver : input.hasDrivers()) {
    inputDriver->addItem(QString::fromUtf8(driver));
    if(driver == config->input.driver) inputDriver->setCurrentText(QString::fromUtf8(driver));
  }

  connect(inputDriver, QOverload<int>::of(&QComboBox::currentIndexChanged), [this](int index) {
    if(index >= 0) config->input.driver = inputDriver->itemText(index).toUtf8().data();
  });
}

void AdvancedSettings::updateConfiguration() {
  regionAuto->setChecked(config->sfc.region == "Auto");
  regionNTSC->setChecked(config->sfc.region == "NTSC");
  regionPAL->setChecked (config->sfc.region == "PAL");

  portSatellaview->setChecked(emulator->connected(SFC::ID::Port::Expansion) == SFC::ID::Device::Satellaview);
  portNone->setChecked       (emulator->connected(SFC::ID::Port::Expansion) == SFC::ID::Device::None);

  focusPause->setChecked (config->input.focusPolicy == ConfigurationSettings::Input::FocusPolicyPauseEmulation);
  focusIgnore->setChecked(config->input.focusPolicy == ConfigurationSettings::Input::FocusPolicyIgnoreInput);
  focusAllow->setChecked (config->input.focusPolicy == ConfigurationSettings::Input::FocusPolicyAllowInput);

  rewindEnable->setChecked(config->system.rewindEnabled);
  setRewindGranularity(uint(config->system.rewindGranularity));
  setRewindHistory(uint(config->system.rewindHistory));
  useCommonDialogs->setChecked(config->diskBrowser.useCommonDialogs);
  gpuFlushEnable->setChecked(config->video.flush);
  gpuBypassWM->setChecked(config->video.exclusive);

  #if defined(DOUBLE_VRAM)
  hacksDoubleVRAM->setChecked(emulator->getProperty("system/ppu1/vram/size").natural() == 131072);
  #endif
  hacksNoSpriteLimit->setChecked(emulator->getOption("hack/ppu/noSpriteLimit") == "true");
  hacksHiresMode7->setChecked(emulator->getOption("hack/ppu/hiresMode7") == "true");
}

void AdvancedSettings::setRewindGranularity(int value) {
  config->system.rewindGranularity = value;

  rewindGranularityValue->setText(QString::fromUtf8(string{config->system.rewindGranularity}));
  rewindGranularity->setSliderPosition(config->system.rewindGranularity);

  rewindGranularity->setEnabled(config->system.rewindEnabled);
  rewindGranularityLabel->setEnabled(config->system.rewindEnabled);
  rewindGranularityValue->setEnabled(config->system.rewindEnabled);

  program->resetRewindHistory(config->system.rewindHistory, config->system.rewindGranularity);
}

void AdvancedSettings::setRewindHistory(int value) {
  config->system.rewindHistory = value;

  rewindHistoryValue->setText(QString::fromUtf8(string{config->system.rewindHistory}));
  rewindHistory->setSliderPosition(config->system.rewindHistory);

  rewindHistory->setEnabled(config->system.rewindEnabled);
  rewindHistoryLabel->setEnabled(config->system.rewindEnabled);
  rewindHistoryValue->setEnabled(config->system.rewindEnabled);

  program->resetRewindHistory(config->system.rewindHistory, config->system.rewindGranularity);
}

