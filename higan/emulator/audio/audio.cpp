namespace higan {

#include "stream.cpp"
Audio audio;

Audio::~Audio() {
  reset(nullptr);
}

auto Audio::reset(Interface* interface) -> void {
  this->interface = interface;
  streams.reset();
  channels = 0;
}

auto Audio::setFrequency(double frequency) -> void {
  this->frequency = (float)frequency;
  for(auto& stream : streams) {
    stream->setFrequency(stream->inputFrequency, frequency);
  }
}

auto Audio::setVolume(float volume) -> void {
  this->volume = volume;
}

auto Audio::setBalance(float balance) -> void {
  this->balance = balance;
}

auto Audio::createStream(uint channels, double frequency) -> shared_pointer<Stream> {
  this->channels = max(this->channels, channels);
  shared_pointer<Stream> stream = new Stream;
  stream->reset(channels, (float)frequency, this->frequency);
  streams.append(stream);
  return stream;
}

auto Audio::process() -> void {
  while(streams) {
    for(auto& stream : streams) {
      if(!stream->pending()) return;
    }

    float samples[channels];
    for(auto& sample : samples) sample = 0.0f;

    for(auto& stream : streams) {
      float buffer[channels];
      uint length = stream->read(buffer), offset = 0;

      for(auto& sample : samples) {
        sample += buffer[offset];
        if(++offset >= length) offset = 0;
      }
    }

    for(auto c : range(channels)) {
      samples[c] = max(-1.f, min(+1.f, samples[c] * volume));
    }

    if(channels == 2) {
      if(balance < 0.f) samples[1] *= 1.f + balance;
      if(balance > 0.f) samples[0] *= 1.f - balance;
    }

    platform->audioFrame(samples, channels);
  }
}

}
