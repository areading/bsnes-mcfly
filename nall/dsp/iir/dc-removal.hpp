#pragma once

#include <nall/dsp/dsp.hpp>

//DC offset removal IIR filter

namespace nall::DSP::IIR {

struct DCRemoval {
  inline auto reset() -> void;
  inline auto process(float in) -> float;  //normalized sample (-1.0 to +1.0)

private:
  float x;
  float y;
};

auto DCRemoval::reset() -> void {
  x = 0.f;
  y = 0.f;
}

auto DCRemoval::process(float in) -> float {
  x = 0.999f * x + in - y;
  y = in;
  return x;
}

}
