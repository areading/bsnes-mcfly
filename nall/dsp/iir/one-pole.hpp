#pragma once

#include <nall/dsp/dsp.hpp>

//one-pole first-order IIR filter

namespace nall::DSP::IIR {

struct OnePole {
  enum class Type : uint {
    LowPass,
    HighPass,
  };

  inline auto reset(Type type, float cutoffFrequency, float samplingFrequency) -> void;
  inline auto process(float in) -> float;  //normalized sample (-1.0 to +1.0)

private:
  Type type;
  float cutoffFrequency;
  float samplingFrequency;
  float a0, b1;  //coefficients
  float z1;      //first-order IIR
};

auto OnePole::reset(Type type, float cutoffFrequency, float samplingFrequency) -> void {
  this->type = type;
  this->cutoffFrequency = cutoffFrequency;
  this->samplingFrequency = samplingFrequency;

  z1 = 0.f;
  float x = (float)cos(2.0 * Math::Pi * cutoffFrequency / samplingFrequency);
  if(type == Type::LowPass) {
    b1 = float(+2.0 - x - sqrt((+2.0 - x) * (+2.0 - x) - 1));
    a0 = 1.f - b1;
  } else {
    b1 = float(-2.0 - x + sqrt((-2.0 - x) * (-2.0 - x) - 1));
    a0 = 1.f + b1;
  }
}

auto OnePole::process(float in) -> float {
  return z1 = in * a0 + z1 * b1;
}

}
