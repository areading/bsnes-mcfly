#pragma once

#include <nall/queue.hpp>
#include <nall/dsp/dsp.hpp>

namespace nall::DSP::Resampler {

struct Cubic {
  inline auto reset(float inputFrequency, float outputFrequency = 0, uint queueSize = 0) -> void;
  inline auto setInputFrequency(float inputFrequency) -> void;
  inline auto pending() const -> bool;
  inline auto read() -> float;
  inline auto write(float sample) -> void;

private:
  float inputFrequency;
  float outputFrequency;

  float ratio;
  float fraction;
  float history[4];
  queue<float> samples;
};

auto Cubic::reset(float inputFrequency, float outputFrequency, uint queueSize) -> void {
  this->inputFrequency = inputFrequency;
  this->outputFrequency = outputFrequency ? outputFrequency : this->inputFrequency;

  ratio = inputFrequency / outputFrequency;
  fraction = 0.f;
  for(auto& sample : history) sample = 0.f;
  samples.resize(queueSize ? queueSize : this->outputFrequency * 0.02f);  //default to 20ms max queue size
}

auto Cubic::setInputFrequency(float inputFrequency) -> void {
  this->inputFrequency = inputFrequency;
  ratio = inputFrequency / outputFrequency;
}

auto Cubic::pending() const -> bool {
  return samples.pending();
}

auto Cubic::read() -> float {
  return samples.read();
}

auto Cubic::write(float sample) -> void {
  auto& mu = fraction;
  auto& s = history;

  s[0] = s[1];
  s[1] = s[2];
  s[2] = s[3];
  s[3] = sample;

  while(mu <= 1.f) {
    float A = s[3] - s[2] - s[0] + s[1];
    float B = s[0] - s[1] - A;
    float C = s[2] - s[0];
    float D = s[1];

    samples.write(A * mu * mu * mu + B * mu * mu + C * mu + D);
    mu += ratio;
  }

  mu -= 1.f;
}

}
