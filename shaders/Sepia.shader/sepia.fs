#version 150

const vec3 LightColor = { 1.0, 0.7,  0.5 };
const vec3 DarkColor  = { 0.2, 0.05, 0.0 };

uniform sampler2D source[];
uniform vec4      sourceSize[];

in Vertex {
  vec2 texCoord;
};

out vec4 fragColor;

vec3 lerp(vec3 x, vec3 y, vec3 s) {
  return x * (1.0 - s) + y * s;
}

vec4 diffColorPass(vec3 color) {
  vec2 vec;
  vec.x = 0.5;
  vec.y = 1.0;
  vec3 scnColor = LightColor * color;
  vec3 grayXfer = vec3(0.3, 0.59, 0.11);
  float gray = dot(grayXfer, scnColor);
  vec3 muted = lerp(scnColor, gray.xxx, vec.xxx);
  vec3 sepia = lerp(DarkColor, LightColor, gray.xxx);
  vec3 result = lerp(muted, sepia, vec.yyy);
  return vec4(result, 1.0);
}

void main() {
  fragColor = diffColorPass(texture2D(source[0], texCoord).xyz);
}
