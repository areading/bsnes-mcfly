#version 150
#in mx
#in k
#in maxW
#in minW
#in lumAdd

uniform sampler2D source[];
uniform vec4      sourceSize[];

in Vertex {
  vec2 texCoord[9];
};

out vec4 fragColor;

void main() {
  vec3 c00 = texture(source[0], texCoord[1]).xyz; 
  vec3 c10 = texture(source[0], texCoord[2]).xyz; 
  vec3 c20 = texture(source[0], texCoord[3]).xyz; 
  vec3 c01 = texture(source[0], texCoord[8]).xyz; 
  vec3 c11 = texture(source[0], texCoord[0]).xyz; 
  vec3 c21 = texture(source[0], texCoord[4]).xyz; 
  vec3 c02 = texture(source[0], texCoord[7]).xyz; 
  vec3 c12 = texture(source[0], texCoord[6]).xyz; 
  vec3 c22 = texture(source[0], texCoord[5]).xyz; 
  vec3 dt = vec3(1.0, 1.0, 1.0);

  float md1 = dot(abs(c00 - c22), dt);
  float md2 = dot(abs(c02 - c20), dt);

  float w1 = dot(abs(c22 - c11), dt) * md2;
  float w2 = dot(abs(c02 - c11), dt) * md1;
  float w3 = dot(abs(c00 - c11), dt) * md2;
  float w4 = dot(abs(c20 - c11), dt) * md1;

  float t1 = w1 + w3;
  float t2 = w2 + w4;
  float ww = max(t1, t2) + 0.0001;

  c11 = (w1 * c00 + w2 * c20 + w3 * c22 + w4 * c02 + ww * c11) / (t1 + t2 + ww);

  float lc1 = k / (0.12 * dot(c10 + c12 + c11, dt) + lumAdd);
  float lc2 = k / (0.12 * dot(c01 + c21 + c11, dt) + lumAdd);

  w1 = clamp(lc1 * dot(abs(c11 - c10), dt) + mx, minW, maxW);
  w2 = clamp(lc2 * dot(abs(c11 - c21), dt) + mx, minW, maxW);
  w3 = clamp(lc1 * dot(abs(c11 - c12), dt) + mx, minW, maxW);
  w4 = clamp(lc2 * dot(abs(c11 - c01), dt) + mx, minW, maxW);

  fragColor.xyz = w1 * c10 + w2 * c21 + w3 * c12 + w4 * c01 + (1.0 - w1 - w2 - w3 - w4) * c11;
}
