#version 150

uniform sampler2D source[];
uniform vec4      sourceSize[];

in Vertex {
  vec2 texCoord[5];
};

out vec4 fragColor;

void main() {
  vec4 colD, colF, colB, colH, col, tmp;
  vec2 sel;

  col  = texture(source[0], texCoord[0]);  //central (can be E0-E3)
  colD = texture(source[0], texCoord[1]);  //D (left)
  colF = texture(source[0], texCoord[2]);  //F (right)
  colB = texture(source[0], texCoord[3]);  //B (top)
  colH = texture(source[0], texCoord[4]);  //H (bottom)

  sel = fract(texCoord[0].xy * sourceSize[0].xy);  //where are we (E0-E3)? E0 is default

  if(sel.y >= 0.5) { tmp = colB; colB = colH; colH = tmp; }  //E1 (or E3): swap B and H
  if(sel.x >= 0.5) { tmp = colF; colF = colD; colD = tmp; }  //E2 (or E3): swap D and F 

  if(colB == colD && colB != colF && colD != colH) {  //do the Scale2x rule
    col = colD;
  }

  fragColor = col;
}
