#version 150

uniform vec4 targetSize;

in vec4 vertex;
in vec4 position;
in vec2 texCoord;

out Vertex {
  vec2 texCoord[5];
} vertexOut;

void main() {
  vec2 offsetx;
  vec2 offsety;

  gl_Position = gl_ModelViewProjectionMatrix * position;

  offsetx.x = 2.0 / targetSize.x;
  offsetx.y = 0.0;
  offsety.y = 2.0 / targetSize.y;
  offsety.x = 0.0;

  vertexOut.texCoord[0] = texCoord.xy;
  vertexOut.texCoord[1] = vertexOut.texCoord[0] - offsetx;  //left
  vertexOut.texCoord[2] = vertexOut.texCoord[0] + offsetx;  //right
  vertexOut.texCoord[3] = vertexOut.texCoord[0] - offsety;  //top
  vertexOut.texCoord[4] = vertexOut.texCoord[0] + offsety;  //bottom
}
