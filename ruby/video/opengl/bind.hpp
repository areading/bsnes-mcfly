#define RUBY_GL_FUN(x) __##x

#if defined(DISPLAY_WINDOWS) || defined(DISPLAY_XORG)
PFNGLCREATEPROGRAMPROC __glCreateProgram = nullptr;
PFNGLDELETEPROGRAMPROC __glDeleteProgram = nullptr;
PFNGLUSEPROGRAMPROC __glUseProgram = nullptr;
PFNGLCREATESHADERPROC __glCreateShader = nullptr;
PFNGLDELETESHADERPROC __glDeleteShader = nullptr;
PFNGLSHADERSOURCEPROC __glShaderSource = nullptr;
PFNGLCOMPILESHADERPROC __glCompileShader = nullptr;
PFNGLGETSHADERIVPROC __glGetShaderiv = nullptr;
PFNGLGETSHADERINFOLOGPROC __glGetShaderInfoLog = nullptr;
PFNGLATTACHSHADERPROC __glAttachShader = nullptr;
PFNGLDETACHSHADERPROC __glDetachShader = nullptr;
PFNGLLINKPROGRAMPROC __glLinkProgram = nullptr;
PFNGLVALIDATEPROGRAMPROC __glValidateProgram = nullptr;
PFNGLGETPROGRAMIVPROC __glGetProgramiv = nullptr;
PFNGLGETPROGRAMINFOLOGPROC __glGetProgramInfoLog = nullptr;
PFNGLGENVERTEXARRAYSPROC __glGenVertexArrays = nullptr;
PFNGLDELETEVERTEXARRAYSPROC __glDeleteVertexArrays = nullptr;
PFNGLBINDVERTEXARRAYPROC __glBindVertexArray = nullptr;
PFNGLGENBUFFERSPROC __glGenBuffers = nullptr;
PFNGLDELETEBUFFERSPROC __glDeleteBuffers = nullptr;
PFNGLBINDBUFFERPROC __glBindBuffer = nullptr;
PFNGLBUFFERDATAPROC __glBufferData = nullptr;
PFNGLGETATTRIBLOCATIONPROC __glGetAttribLocation = nullptr;
PFNGLVERTEXATTRIBPOINTERPROC __glVertexAttribPointer = nullptr;
PFNGLENABLEVERTEXATTRIBARRAYPROC __glEnableVertexAttribArray = nullptr;
PFNGLDISABLEVERTEXATTRIBARRAYPROC __glDisableVertexAttribArray = nullptr;
PFNGLBINDFRAGDATALOCATIONPROC __glBindFragDataLocation = nullptr;
PFNGLGETUNIFORMLOCATIONPROC __glGetUniformLocation = nullptr;
PFNGLGETUNIFORMIVPROC __glGetUniformiv = nullptr;
PFNGLUNIFORM1IPROC __glUniform1i = nullptr;
PFNGLUNIFORM1FPROC __glUniform1f = nullptr;
PFNGLUNIFORM2FPROC __glUniform2f = nullptr;
PFNGLUNIFORM2FVPROC __glUniform2fv = nullptr;
PFNGLUNIFORM4FPROC __glUniform4f = nullptr;
PFNGLUNIFORM4FVPROC __glUniform4fv = nullptr;
PFNGLUNIFORMMATRIX4FVPROC __glUniformMatrix4fv = nullptr;
PFNGLGENFRAMEBUFFERSPROC __glGenFramebuffers = nullptr;
PFNGLDELETEFRAMEBUFFERSPROC __glDeleteFramebuffers = nullptr;
PFNGLBINDFRAMEBUFFERPROC __glBindFramebuffer = nullptr;
PFNGLFRAMEBUFFERTEXTURE2DPROC __glFramebufferTexture2D = nullptr;
PFNGLFENCESYNCPROC __glFenceSync = nullptr;
PFNGLISSYNCPROC __glIsSync = nullptr;
PFNGLDELETESYNCPROC __glDeleteSync = nullptr;
PFNGLCLIENTWAITSYNCPROC __glClientWaitSync = nullptr;
PFNGLWAITSYNCPROC __glWaitSync = nullptr;
#define glCreateProgram RUBY_GL_FUN(glCreateProgram)
#define glDeleteProgram RUBY_GL_FUN(glDeleteProgram)
#define glUseProgram RUBY_GL_FUN(glUseProgram)
#define glCreateShader RUBY_GL_FUN(glCreateShader)
#define glDeleteShader RUBY_GL_FUN(glDeleteShader)
#define glShaderSource RUBY_GL_FUN(glShaderSource)
#define glCompileShader RUBY_GL_FUN(glCompileShader)
#define glGetShaderiv RUBY_GL_FUN(glGetShaderiv)
#define glGetShaderInfoLog RUBY_GL_FUN(glGetShaderInfoLog)
#define glAttachShader RUBY_GL_FUN(glAttachShader)
#define glDetachShader RUBY_GL_FUN(glDetachShader)
#define glLinkProgram RUBY_GL_FUN(glLinkProgram)
#define glValidateProgram RUBY_GL_FUN(glValidateProgram)
#define glGetProgramiv RUBY_GL_FUN(glGetProgramiv)
#define glGetProgramInfoLog RUBY_GL_FUN(glGetProgramInfoLog)
#define glGenVertexArrays RUBY_GL_FUN(glGenVertexArrays)
#define glDeleteVertexArrays RUBY_GL_FUN(glDeleteVertexArrays)
#define glBindVertexArray RUBY_GL_FUN(glBindVertexArray)
#define glGenBuffers RUBY_GL_FUN(glGenBuffers)
#define glDeleteBuffers RUBY_GL_FUN(glDeleteBuffers)
#define glBindBuffer RUBY_GL_FUN(glBindBuffer)
#define glBufferData RUBY_GL_FUN(glBufferData)
#define glGetAttribLocation RUBY_GL_FUN(glGetAttribLocation)
#define glVertexAttribPointer RUBY_GL_FUN(glVertexAttribPointer)
#define glEnableVertexAttribArray RUBY_GL_FUN(glEnableVertexAttribArray)
#define glDisableVertexAttribArray RUBY_GL_FUN(glDisableVertexAttribArray)
#define glBindFragDataLocation RUBY_GL_FUN(glBindFragDataLocation)
#define glGetUniformLocation RUBY_GL_FUN(glGetUniformLocation)
#define glGetUniformiv RUBY_GL_FUN(glGetUniformiv)
#define glUniform1i RUBY_GL_FUN(glUniform1i)
#define glUniform1f RUBY_GL_FUN(glUniform1f)
#define glUniform2f RUBY_GL_FUN(glUniform2f)
#define glUniform2fv RUBY_GL_FUN(glUniform2fv)
#define glUniform4f RUBY_GL_FUN(glUniform4f)
#define glUniform4fv RUBY_GL_FUN(glUniform4fv)
#define glUniformMatrix4fv RUBY_GL_FUN(glUniformMatrix4fv)
#define glGenFramebuffers RUBY_GL_FUN(glGenFramebuffers)
#define glDeleteFramebuffers RUBY_GL_FUN(glDeleteFramebuffers)
#define glBindFramebuffer RUBY_GL_FUN(glBindFramebuffer)
#define glFramebufferTexture2D RUBY_GL_FUN(glFramebufferTexture2D)
#define glFenceSync RUBY_GL_FUN(glFenceSync)
#define glIsSync RUBY_GL_FUN(glIsSync)
#define glDeleteSync RUBY_GL_FUN(glDeleteSync)
#define glClientWaitSync RUBY_GL_FUN(glClientWaitSync)
#define glWaitSync RUBY_GL_FUN(glWaitSync)
#endif
#if defined(DISPLAY_WINDOWS)
PFNGLACTIVETEXTUREPROC __glActiveTexture = nullptr;
#define glActiveTexture RUBY_GL_FUN(glActiveTexture)
#endif


static bool OpenGLBind() {
  #define bind(prototype, function) \
    __##function = (prototype)glGetProcAddress(#function); \
    if(__##function == nullptr) return false

  #if defined(DISPLAY_WINDOWS) || defined(DISPLAY_XORG)
  bind(PFNGLCREATEPROGRAMPROC, glCreateProgram);
  bind(PFNGLDELETEPROGRAMPROC, glDeleteProgram);
  bind(PFNGLUSEPROGRAMPROC, glUseProgram);
  bind(PFNGLCREATESHADERPROC, glCreateShader);
  bind(PFNGLDELETESHADERPROC, glDeleteShader);
  bind(PFNGLSHADERSOURCEPROC, glShaderSource);
  bind(PFNGLCOMPILESHADERPROC, glCompileShader);
  bind(PFNGLGETSHADERIVPROC, glGetShaderiv);
  bind(PFNGLGETSHADERINFOLOGPROC, glGetShaderInfoLog);
  bind(PFNGLATTACHSHADERPROC, glAttachShader);
  bind(PFNGLDETACHSHADERPROC, glDetachShader);
  bind(PFNGLLINKPROGRAMPROC, glLinkProgram);
  bind(PFNGLVALIDATEPROGRAMPROC, glValidateProgram);
  bind(PFNGLGETPROGRAMIVPROC, glGetProgramiv);
  bind(PFNGLGETPROGRAMINFOLOGPROC, glGetProgramInfoLog);
  bind(PFNGLGENVERTEXARRAYSPROC, glGenVertexArrays);
  bind(PFNGLDELETEVERTEXARRAYSPROC, glDeleteVertexArrays);
  bind(PFNGLBINDVERTEXARRAYPROC, glBindVertexArray);
  bind(PFNGLGENBUFFERSPROC, glGenBuffers);
  bind(PFNGLDELETEBUFFERSPROC, glDeleteBuffers);
  bind(PFNGLBINDBUFFERPROC, glBindBuffer);
  bind(PFNGLBUFFERDATAPROC, glBufferData);
  bind(PFNGLGETATTRIBLOCATIONPROC, glGetAttribLocation);
  bind(PFNGLVERTEXATTRIBPOINTERPROC, glVertexAttribPointer);
  bind(PFNGLENABLEVERTEXATTRIBARRAYPROC, glEnableVertexAttribArray);
  bind(PFNGLDISABLEVERTEXATTRIBARRAYPROC, glDisableVertexAttribArray);
  bind(PFNGLBINDFRAGDATALOCATIONPROC, glBindFragDataLocation);
  bind(PFNGLGETUNIFORMLOCATIONPROC, glGetUniformLocation);
  bind(PFNGLGETUNIFORMIVPROC, glGetUniformiv);
  bind(PFNGLUNIFORM1IPROC, glUniform1i);
  bind(PFNGLUNIFORM1FPROC, glUniform1f);
  bind(PFNGLUNIFORM2FPROC, glUniform2f);
  bind(PFNGLUNIFORM2FVPROC, glUniform2fv);
  bind(PFNGLUNIFORM4FPROC, glUniform4f);
  bind(PFNGLUNIFORM4FVPROC, glUniform4fv);
  bind(PFNGLUNIFORMMATRIX4FVPROC, glUniformMatrix4fv);
  bind(PFNGLGENFRAMEBUFFERSPROC, glGenFramebuffers);
  bind(PFNGLDELETEFRAMEBUFFERSPROC, glDeleteFramebuffers);
  bind(PFNGLBINDFRAMEBUFFERPROC, glBindFramebuffer);
  bind(PFNGLFRAMEBUFFERTEXTURE2DPROC, glFramebufferTexture2D);
  bind(PFNGLFENCESYNCPROC, glFenceSync);
  bind(PFNGLISSYNCPROC, glIsSync);
  bind(PFNGLDELETESYNCPROC, glDeleteSync);
  bind(PFNGLCLIENTWAITSYNCPROC, glClientWaitSync);
  bind(PFNGLWAITSYNCPROC, glWaitSync);
  #endif
  #if defined(DISPLAY_WINDOWS)
  bind(PFNGLACTIVETEXTUREPROC, glActiveTexture);
  #endif

  #undef bind

  return true;
}
